<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth ;
use Closure;
use Redirect ;
class SinhVienLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $user = Auth::user() ;
            if($user->level == 0){
                return $next($request);
            }else{
                return Redirect::route('login_sinhvien') ;
            }

        }else{
            return Redirect::route('login_sinhvien') ;
        }
        //return $next($request);
    }
}
