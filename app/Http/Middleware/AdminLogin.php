<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth ;
use Closure;
use Redirect ;
class AdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       // return $next($request);
        if(Auth::check()){
            $user = Auth::user() ;
            if($user->level == 1){
                return $next($request);
            }else{
                return Redirect::route('login_admin_get') ;
            }

        }else{
            return Redirect::route('login_admin_get') ;
        }
    }
}
