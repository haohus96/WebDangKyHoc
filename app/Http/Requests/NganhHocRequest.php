<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NganhHocRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
  
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()

    {
        $rule = [   'ma_nganh' =>'required|unique:ChuyenNganh,ma_nganh|min:3|max:20',
                    'ten_nganh'=>'required|min:3|max:50'
                ] ;
        if(isset($this->ma_nganhOld) && $this->ma_nganh == $this->ma_nganhOld){
            unset($rule['ma_nganh']) ;
            $rule['ma_nganh'] = 'required|min:3|max:20' ;
        }
        return $rule ;
    }
    public function messages()
    {
        $message = [
                        'ma_nganh.required'=>'Bạn chưa điền mã ngành',
                        'ma_nganh.unique'=>'Mã ngành đã tồn tại',
                        'ma_nganh.min'=>'Mã ngành có ít nhất 3 ký tự',
                        'ma_nganh.max'=>'Mã ngành có nhiều nhất 20 ký tự',
                        'ten_nganh.required'=>'Bạn chưa điền tên ngành',
                        'ten_nganh.min'=>'Tên ngành có ít nhất 3 ký tự',
                        'ten_nganh.max'=>'Tên ngành có nhiều nhất 50 ký tự',
                    ] ;
        return $message ;
    }
}
