<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lang ;
use App\NamHoc ;
use Exception ;
use DB ;
class NamHocController extends Controller
{
    //
    public function index()

    {
        $namhoc = NamHoc::orderBy('nam','DESC')->orderBy('hoc_ky')->get() ;

        return view('admin.namhoc.index')->with(['namhoc' => $namhoc]) ;
    }
    public function create()
    {

        $data = Lang::get('general.hocky') ;
        $hocky[$data[0]] = $data[0] ;
        $hocky[$data[1]] = $data[1] ;
        $year_now = date('Y') ;
        // $namhoc[($year_now-1) . '-'. $year_now] = ($year_now-1) . '-'. $year_now ;
        for($i=0;$i<=5;$i++){
            $namhoc[($year_now+$i-1) . '-'. ($year_now+$i)] = ($year_now+$i-1) . '-'. ($year_now +$i);
        }
        
        $namhoc_list = DB::table('NamHoc')->select(DB::raw('nam,count(*) as so_namhoc'))->groupBy('nam')->get() ;
        foreach($namhoc_list as $key => $value){
            if($value->so_namhoc == 2){
                unset($namhoc[$value->nam]) ;
            }
        }
        if(count($namhoc)>0){
            $hocky_list = DB::table('NamHoc')->where('nam',array_values($namhoc)[0])->get();
            foreach($hocky_list as $key => $value){
                unset($hocky[$value->hoc_ky]) ;
            }
        }
        return view('admin/namhoc/create')->with(['namhoc'=>$namhoc,'hocky'=>$hocky]) ;
    }
    public function store(Request $request)
    {
        //var_dump($_POST) ;die ;
        try{
            $namhoc = new NamHoc() ;
            $namhoc->nam = $request->namhoc ;
            $namhoc->hoc_ky = $request->hocky ;
            $namhoc->save() ;
            //var_dump($namhoc) ; die ;
            return redirect('admin/namhoc/create')->with(['thongbao'=>'Thêm thành công']) ;
        }catch(Exception $e){
            return redirect('admin/namhoc/create')->with(['thongbao'=>'Không thêm được']) ;
        }
        
    }
    public function edit()
    {
    	
    }
}
