<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Lang ;
use MyFunction ;
class AjaxController extends Controller
{
    //
    public function getHockyByNamhoc(Request $request){
    	$data = Lang::get('general.hocky') ;
    	$hocky = array() ;
    	foreach($data as $key=>$value){
    		$hocky[$value] = $value ;
    		
    	}
    	$namhoc = $request->namhoc ;
    	$namhoc_list = DB::table('NamHoc')->where('nam',$namhoc)->select('hoc_ky')->get();
    	foreach($namhoc_list as $key => $value){
    		unset($hocky[$value->hoc_ky]) ;
    	}
    	foreach ($hocky as $key => $value) {
    		echo '<option value="' . $key .'">'.$value.'</option>' ;
    	}
    }
    public function getTenmhByMamh(Request $request){
       // $lop = Lang::get('general.lop') ;
        $lop = MyFunction::getLop() ;
    	$ma_mh = $request->ma_mh ;
        $id_namhoc = $request->id_namhoc ;
        $lop_exists = DB::table('LopHoc')->where([['ma_mh','=',$ma_mh],['nam_hoc','=',$id_namhoc]])->select('lop');
        $lichhoc_list = DB::table('LichHoc')
            ->whereNotIn('id',function($query) use($id_namhoc){
                    $query->select(DB::raw('id_lichhoc'))
                            ->from('LopHoc_LichHoc')
                            ->join('LopHoc','LopHoc_LichHoc.id_LopHoc','LopHoc.id')
                            ->where('LopHoc.nam_hoc',$id_namhoc) ;
                            
        })->orderBy('ngay_hoc')->orderBy('tiet_hoc')->get();
        if(isset($request->id_lophoc)){
            $lop_exists->where([['id','!=',$request->id_lophoc]]) ;


            $id_lophoc = $request->id_lophoc ;
            $lichhoc_list = DB::table('LichHoc')
                ->whereNotIn('id',function($query) use($id_namhoc,$id_lophoc){
                        $query->select(DB::raw('id_lichhoc'))
                                ->from('LopHoc_LichHoc')
                                ->join('LopHoc','LopHoc_LichHoc.id_LopHoc','LopHoc.id')
                                ->where(array(
                                    ['LopHoc.nam_hoc','=',$id_namhoc],
                                    ['LopHoc.id','!=',$id_lophoc]
                                ));
                })->orderBy('ngay_hoc')->orderBy('tiet_hoc')->get();
        }

        foreach($lop_exists->get() as $key => $value){
            unset($lop[$value->lop]) ;
            //echo $value->lop ;
        }
        sort($lop) ;
    	// echo $ma_mh ; die ;
    	
    	//echo $monhoc->ten_mh ;
        $lichhoc = array() ;

        foreach($lichhoc_list as $value){
            $lichhoc[$value->id] = $value->ngay_hoc.','.$value->tiet_hoc .','.$value->giang_duong;
        }

        $monhoc = DB::table('MonHoc')->where('ma_mh',$ma_mh)->first() ;
        return [    'ten_mh' =>  $monhoc->ten_mh ,
                    'lop'    =>     $lop,
                    'lichhoc'=> $lichhoc,
        ];
        // return response()->json([    'ten_mh' =>  $monhoc->ten_mh ,
        //             'lop'    =>     $lop,
        //             'lichhoc'=> $lichhoc,
        // ]);
    }
   // public function getTensvByMasv()
}
