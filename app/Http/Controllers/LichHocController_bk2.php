<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB ;
use App\LichHoc;
use Lang;
use Session ;
use Exception ;
class LichHocController extends Controller
{
    //
    public function index(){
        $thu = Lang::get('general.thu') ;
        $lichhoc = LichHoc::all() ;

    	return view('admin.lichhoc.index')->with(['lichhoc'=>$lichhoc]) ;
    }
    public function create(){
        $thu = Lang::get('general.thu') ;
        
    	return view('admin.lichhoc.create')->with(['thu'=>$thu]) ;
    }
    public function store(Request $request){
        $rules = [  'tiethoc' => 'required',
                    'giangduong'=>'required'
        ];
        $messages = [   'tiethoc.required' =>'Bạn chưa điền tiết học',
                        'giangduong.required'=>'Bạn chưa điền phòng học'
        ];
        $this->validate($request,$rules,$messages) ;
        //echo "thành công rồi haha" ;

        $ngay_hoc = $request->thu;
        $tiet_hoc = $request->tiethoc;
        $giang_duong = $request->giangduong ;
        $lichhoc_exists = LichHoc::where(['ngay_hoc' => $ngay_hoc,
            'tiet_hoc'=>$tiet_hoc,'giang_duong'=>$giang_duong])->first() ;
        if(!empty($lichhoc_exists)){
            return redirect('lichhoc/create')->with(['thongbao'=>'Lịch học đã tồn tại']) ;
        }
        $tiethoc_exists = LichHoc::where(['ngay_hoc' => $ngay_hoc,
            'giang_duong'=>$giang_duong])->get() ;
        $tiethoc_error = null ;
        if(!empty($tiethoc_exists)){
            //$list_tiethoc = DB::table('LichHoc')->get() ;
            if(strpos($tiet_hoc, '-') > 0 ){
                //echo "có dâu '-' " ; die ;
                $arr_tiethoc = explode('-',$tiet_hoc) ;
                $c = $arr_tiethoc[0] ;
                $d = $arr_tiethoc[1] ;
                foreach($tiethoc_exists as $key => $value){
                    // a, b and c,d 
                    // TH1 c<=a and d >= a => trung
                    // TH2 c<=b and d>=b => trung
                    if(strpos($value->tiet_hoc,'-') > 0){
                        $arr_value = explode('-',$value->tiet_hoc) ;
                        $a = $arr_value[0] ;
                        $b = $arr_value[1] ;
                        if(( $c <= $a && $d >= $a )||( $c <= $b && $d >= $b )){
                            $tiethoc_error = "Lỗi trùng tiết học" ;
                            break ;
                        }
                    }else{
                        $aa = $value->tiet_hoc ;
                        if($aa >= $c && $aa <= $d){
                            $tiethoc_error = "Lỗi trùng tiết học" ;
                            break ;
                        }
                    }
                    
                }
               // print_r($arr_tiethoc) ; die ;
            }else{
                //echo " không có dâu '-' " ; die ;
                foreach($list_tiethoc as $key => $value){
                    
                    if(strpos($value->tiet_hoc,'-') > 0){
                        $arr_value = explode('-',$value->tiet_hoc) ;
                        $a = $arr_value[0] ;
                        $b = $arr_value[1] ;
                        if($tiet_hoc >= $a && $tiet_hoc <= $b){
                            $tiethoc_error = "Lỗi trùng tiết học" ;
                            break ;
                        }
                    }else{
                       
                        if($tiet_hoc == $value->tiet_hoc){
                            $tiethoc_error = "Lỗi trùng tiết học" ;
                            break ;
                        }
                    }
                    
                }
            } 
        }
        if(!empty($tiethoc_error)){
            return redirect('admin/lichhoc/create')->with(['thongbao'=>$tiethoc_error]) ;
        }
        
        $data = array(  'ngay_hoc'  =>  $request->thu,
                        'tiet_hoc'  =>  $request->tiethoc,
                        'giang_duong'   =>  $request->giangduong
        );
        $lichhoc = LichHoc::firstOrCreate($data) ;
        // echo '<pre>';
        // var_dump($lichhoc) ;
        // echo '</pre>';
        return redirect('admin/lichhoc/create')->with(['thongbao'=>'Thêm thành công']) ;
    }
    public function edit($id){
        $thu = Lang::get('general.thu') ;
        $lichhoc = LichHoc::find($id) ;
        //print_r($lichhoc) ; die ;
       // echo $id ; die ;
    	return view('admin.lichhoc.edit')->with(['lichhoc'=>$lichhoc,'thu'=>$thu]) ;
    }
    public function update(Request $request,$id){
       $rules = [  'tiethoc' => 'required',
                    'giangduong'=>'required'
        ];
        $messages = [   'tiethoc.required' =>'Bạn chưa điền tiết học',
                        'giangduong.required'=>'Bạn chưa điền phòng học'
        ];
        $this->validate($request,$rules,$messages) ;
        //echo "thành công rồi haha" ;

        $ngay_hoc = $request->thu;
        $tiet_hoc = $request->tiethoc;
        $giang_duong = $request->giangduong ;
        $lichhoc_exists = LichHoc::where(['ngay_hoc' => $ngay_hoc,
                        'tiet_hoc'=>$tiet_hoc,'giang_duong'=>$giang_duong,
                        ['id','!=',$id]
                        ])->first() ;
        if(!empty($lichhoc_exists)){
            return redirect('lichhoc/edit/'.$id)->with(['thongbao'=>'Lịch học đã tồn tại']) ;
        }
        $tiethoc_exists = LichHoc::where(['ngay_hoc' => $ngay_hoc,
                        'giang_duong'=>$giang_duong,
                        ['id','!=',$id]
                        ])->get() ;
        $tiethoc_error = null ;
        if(!empty($tiethoc_exists)){
            
            if(strpos($tiet_hoc, '-') > 0 ){
                //echo "có dâu '-' " ; die ;
                $arr_tiethoc = explode('-',$tiet_hoc) ;
                $c = $arr_tiethoc[0] ;
                $d = $arr_tiethoc[1] ;
                foreach($tiethoc_exists as $key => $value){
                    // a, b and c,d 
                    // TH1 c<=a and d >= a => trung
                    // TH2 c<=b and d>=b => trung
                    if(strpos($value->tiet_hoc,'-') > 0){
                        $arr_value = explode('-',$value->tiet_hoc) ;
                        $a = $arr_value[0] ;
                        $b = $arr_value[1] ;
                        if(( $c <= $a && $d >= $a )||( $c <= $b && $d >= $b )){
                            $tiethoc_error = "Lỗi trùng tiết học" ;
                            break ;
                        }
                    }else{
                        $aa = $value->tiet_hoc ;
                        if($aa >= $c && $aa <= $d){
                            $tiethoc_error = "Lỗi trùng tiết học" ;
                            break ;
                        }
                    }
                    
                }
               // print_r($arr_tiethoc) ; die ;
            }else{
                //echo " không có dâu '-' " ; die ;
                foreach($list_tiethoc as $key => $value){
                    
                    if(strpos($value->tiet_hoc,'-') > 0){
                        $arr_value = explode('-',$value->tiet_hoc) ;
                        $a = $arr_value[0] ;
                        $b = $arr_value[1] ;
                        if($tiet_hoc >= $a && $tiet_hoc <= $b){
                            $tiethoc_error = "Lỗi trùng tiết học" ;
                            break ;
                        }
                    }else{
                       
                        if($tiet_hoc == $value->tiet_hoc){
                            $tiethoc_error = "Lỗi trùng tiết học" ;
                            break ;
                        }
                    }
                    
                }
            } 
        }
        if(!empty($tiethoc_error)){
            return redirect('lichhoc/edit/'.$id)->with(['thongbao'=>$tiethoc_error]) ;
        }
        
        $data = array(  'ngay_hoc'  =>  $request->thu,
                        'tiet_hoc'  =>  $request->tiethoc,
                        'giang_duong'   =>  $request->giangduong
        );
        LichHoc::where('id',$id)->update($data) ;
        
        return redirect('admin/lichhoc/edit/'.$id)->with(['thongbao'=>'Sửa thành công']) ;

    }
    public function getModal($id){
        $model = 'lichhoc' ;
        $confirm_route = $error = null ;
        try{
            $confirm_route = route('lichhoc_delete',$id) ;
            return view('admin/layout/modal_confirmation')->with(['model'=>$model,
                    'error'=>$error,'confirm_route'=>$confirm_route
                ]) ;
        }catch(Exception $e){
            $error = "lỗi rồi haha" ;
            return view('admin/layout/modal_confirmation')->with(['model'=>$model,
                    'error'=>$error,'confirm_route'=>$confirm_route
                ]) ;
        }
    }
    public function delete($id)
    {
        
        try{
            LichHoc::where('id',$id)->delete() ;
            return  redirect('admin/lichhoc')->with(['thongbao'=>'Xóa thành công']) ;
        }catch(Exception $e){
            return  redirect('admin/lichhoc')->with(['thongbao'=>'Không xóa được ']) ;
        }
    }
}
