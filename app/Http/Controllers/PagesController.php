<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth ;
use Illuminate\Http\Request;
use App\LopHoc;
use App\MonHoc;
use App\SinhVien ;
use App\NamHoc;
use App\LichHoc ;
use App\ChuyenNganh ;
use DB ;
use Redirect ;
use Exception ;
use Validator; 
use Illuminate\Support\Facades\Input;
use Hash;
use App\User ;
use MyFunction ;
use Lang ;
class PagesController extends Controller
{
    //
    public function index(Request $request)
    {
    	if(isset($request->arr_idLopHoc)){
    		//echo $request->id ; die ;
    		//return view('pages.dangkyhoc')->with(['id'=>$request->id]) ;
    		//echo '<pre>' ;
    		//print_r($request->arr_idLopHoc) ;
    		//echo '<pre>' ;
    		return $request->arr_idLopHoc ;
    	}else{
    		//return view('pages.dangkyhoc') ;
    		echo "ko có gì haha" ;
    	}

    }
    // lấy ra danh sách lớp học cho sinh viên đăng ký
    public function getLopHoc(){
    	// $loai_hp = DB::table('LoaiHocPhi')->where('id',3)->first() ;
    	// $loai_diem = $loai_hp->loai_diem ;
    	// echo $loai_diem ;
    	// die  ;
    	$user = Auth::user() ;
        $username = $user->username ; 
        $sinhvien = SinhVien::where('masv',$username)->first() ;
        $ma_nganh = $sinhvien->chuyennganh ;
        

        $lophoc = LopHoc::join('namhoc','nam_hoc','namhoc.id')
            ->join('MonHoc','LopHoc.ma_mh','MonHoc.ma_mh')
            ->select('LopHoc.*','namhoc.nam','namhoc.hoc_ky','MonHoc.ten_mh','MonHoc.so_tinchi',
                    'MonHoc.hoc_phi')
            ->where('LopHoc.trang_thai',1)
            ->whereIn('LopHoc.ma_mh',function($query) use ($ma_nganh){
                $query->select(DB::raw('Nganhhoc_Monhoc.ma_mh'))
                        ->from('Nganhhoc_Monhoc')
                        ->where('Nganhhoc_Monhoc.ma_nganh',$ma_nganh)
                        ->orWhere('Nganhhoc_Monhoc.ma_nganh',1) ;
            })
            ->get() ;
       	$lichhoc_svdk = DB::table('LichHoc')
                ->join('LopHoc_LichHoc','Lichhoc.id','LopHoc_LichHoc.id_lichhoc')
                ->whereIn('LopHoc_LichHoc.id_lophoc',function($query) use($username){
                    $query->select(DB::raw('DsSinhvienDK.id_lophoc'))
                            ->from('DsSinhvienDK')
                            ->join('LopHoc','DsSinhvienDK.id_lophoc','LopHoc.id')
                            ->where('DsSinhvienDK.ma_sv','=',$username)
                            ->where('LopHoc.trang_thai',1)
                             ;
                })->select('LichHoc.*')->get() ;
        $lophoc_svdk = LopHoc::join('MonHoc','LopHoc.ma_mh','MonHoc.ma_mh')
            ->join('DsSinhVienDK','LopHoc.id','DsSinhVienDK.id_lophoc')
            ->select('LopHoc.*','MonHoc.ten_mh','MonHoc.so_tinchi',
                    'MonHoc.hoc_phi','DsSinhVienDK.trang_thai')
            ->where(array(
                ['LopHoc.trang_thai',1],
                ['DsSinhVienDK.ma_sv',$username]
            ))
            ->get() ;
        $data_lophoc = array() ;
        $data_lophocsvdk = array() ;  
        $mamh_selected = array() ;
        $mamh_diemcao = array() ;
        $diem_sv = DB::table('Diem')->where('ma_sv',$username)->get();
        foreach($diem_sv as $dsv){
            $diemsv_diemchu =  $dsv->diem_chu ;
            $trangthaihoc = DB::table('LoaiHocPhi')->where("loai_diem","like","%$diemsv_diemchu%")->get() ;
            if(count($trangthaihoc) == 0){
                array_push($mamh_diemcao,$dsv->ma_mh) ;
                //echo "vào đây" ;
            }
           // var_dump($trangthaihoc) ;
        }
        //var_dump($mamh_diemcao) ; 
       // die ;
        // lấy ra danh sách môn học đã đăng ký
        foreach($lophoc_svdk as $value){
            array_push($mamh_selected, $value->ma_mh) ;
            $data_lhsvdk = array() ;
            
            $data_lhsvdk['id_lophoc'] = $value->id ;
            $data_lhsvdk['ma_mh'] = $value->ma_mh ;
            $data_lhsvdk['lop'] = $value->lop ;
            $data_lhsvdk['so_luong'] = $value->so_luong ;
            $data_lhsvdk['soluong_dk'] = $value->soluong_dk ;
            $data_lhsvdk['hoc_ky'] = $value->hoc_ky ;
            $data_lhsvdk['nam'] = $value->nam ;
            $data_lhsvdk['ten_mh'] = $value->ten_mh ;
            $data_lhsvdk['so_tinchi'] = $value->so_tinchi ;
            $data_lhsvdk['hoc_phi'] = $value->hoc_phi ;
            $data_lhsvdk['trang_thai'] = $value->trang_thai ;
            $diem = DB::table('Diem')
                ->where(array(
                    ['ma_sv','=',$username],
                    ['ma_mh','=',$value->ma_mh]
                ))->first() ;
            if(!empty($diem)){
                $diem_chu = $diem->diem_chu ;
                $loaihocphi = DB::table('LoaiHocPhi')
                    ->where('loai_diem','like',"%$diem_chu%")
                    ->first() ;
                if(!empty($loaihocphi)){
                    //$data['trang_thai']  = $loaihocphi->ten ;
                    $data_lhsvdk['hoc_phi'] = $value->hoc_phi * $loaihocphi->heso_hocphi ;
                }
                
            }
            $id_lh = $value->id ;
            $lichhoc_val = DB::table('LichHoc')
                ->whereIn('LichHoc.id',function($query) use($id_lh){
                    $query->select(DB::raw('LopHoc_LichHoc.id_lichhoc'))
                            ->from('LopHoc_LichHoc')
                            ->where('LopHoc_LichHoc.id_lophoc',$id_lh) ;    
                })->select('LichHoc.*')->get() ;
           
            $data_lhsvdk['ngay_hoc'] = '' ;
            $data_lhsvdk['tiet_hoc'] = '' ;
            $data_lhsvdk['giang_duong'] = '' ;
            foreach($lichhoc_val as $lh){
                $data_lhsvdk['ngay_hoc'] = $data_lhsvdk['ngay_hoc'].','.$lh->ngay_hoc ;
                $data_lhsvdk['tiet_hoc'] = $data_lhsvdk['tiet_hoc'] .','.$lh->tiet_hoc ;
                $data_lhsvdk['giang_duong'] = $data_lhsvdk['giang_duong'].','.$lh->giang_duong;
            }
            
            $data_lhsvdk['ngay_hoc'] = substr($data_lhsvdk['ngay_hoc'], 1) ;
            $data_lhsvdk['tiet_hoc'] = substr($data_lhsvdk['tiet_hoc'], 1) ;
            $data_lhsvdk['giang_duong'] = substr($data_lhsvdk['giang_duong'], 1) ;

            array_push($data_lophocsvdk,$data_lhsvdk) ;
        }


        // lấy ra danh sách lớp học
        foreach($lophoc as $value)
        {
            $data = array() ;
            $data['id_lophoc'] = $value->id ;
            $data['ma_mh'] = $value->ma_mh ;
            $data['lop'] = $value->lop ;
            $data['so_luong'] = $value->so_luong ;
            $data['soluong_dk'] = $value->soluong_dk ;
            $data['hoc_ky'] = $value->hoc_ky ;
            $data['nam'] = $value->nam ;
            $data['ten_mh'] = $value->ten_mh ;
            $data['so_tinchi'] = $value->so_tinchi ;
            $data['hoc_phi'] = $value->hoc_phi ;

            $data['tinh_trang'] = '' ;
            $data['active'] = 1;
            if($value->so_luong == $value->soluong_dk){
                $data['tinh_trang'] = "Môn học đã đủ số lượng đăng ký" ;
                $data['active'] = 0;
            }
            // Kiểm tra xem với mỗi môn học sinh viên đang truy cập vào có trung lịch học không 
             // phần quan trong nhất của trang web
            //khá khó  haha 
            $id_lophoc = $value->id ;
            
            $lichhoc_value = DB::table('LichHoc')
                ->whereIn('LichHoc.id',function($query) use($id_lophoc){
                    $query->select(DB::raw('LopHoc_LichHoc.id_lichhoc'))
                            ->from('LopHoc_LichHoc')
                            ->where('LopHoc_LichHoc.id_lophoc',$id_lophoc) ;    
                })->select('LichHoc.*')->get() ;
            $error_exists = '' ;
            $data['ngay_hoc'] = '' ;
            $data['tiet_hoc'] = '' ;
            $data['giang_duong'] = '' ;
            foreach($lichhoc_value as $key1 => $lh_value){
            	$data['ngay_hoc'] = $data['ngay_hoc'] . ',' . $lh_value->ngay_hoc ;
            	$data['tiet_hoc'] = $data['tiet_hoc'] . ',' . $lh_value->tiet_hoc ;
            	$data['giang_duong'] = $data['giang_duong'] . ',' . $lh_value->giang_duong ;
                foreach ($lichhoc_svdk as $key2 => $lhsvdk_value) {
                    if($lh_value->ngay_hoc == $lhsvdk_value->ngay_hoc){
                        $str1 = $lh_value->tiet_hoc ;
                        $str2 = $lhsvdk_value->tiet_hoc ;
                        $result = MyFunction::compareString($str1,$str2) ;
                        if(!empty($result)){
                            $error_exists = 1 ;
                            break ;
                       }
                    }
                }
            }
            if(in_array($value->ma_mh, $mamh_selected)){
                $error_exists = 1 ;
                $data['active'] = 0; 
            }
            if(in_array($value->ma_mh, $mamh_diemcao)){
               // $error_exists = 1 ;
                $data['active'] = 0; 
                $data['tinh_trang'] = "Bạn đã học môn này và điểm cao " ;
            }else{
                
            }

            if(!empty($error_exists)){
                if(empty($data['tinh_trang'])){
                    $data['tinh_trang'] = "Môn học bị trùng lịch học" ;
                }else{
                    $data['tinh_trang'] = $data['tinh_trang']. " và trùng lịch học " ;
                }
                $data['active'] = 0; 
            }

           	$data['ngay_hoc'] = substr($data['ngay_hoc'], 1) ;
           	$data['tiet_hoc'] = substr($data['tiet_hoc'], 1) ;
           	$data['giang_duong'] = substr($data['giang_duong'], 1) ;
            array_push($data_lophoc,$data) ;
        }
      
       
        return view('pages.dangkyhoc',[
        		'data_lophoc'=>$data_lophoc,
        		'data_lophocsvdk' => $data_lophocsvdk 
        ]) ;
    }
    public function getLopHoc2(Request $request){
    	$user = Auth::user() ;

    	$arr_idLopHoc = [] ;
    	//$request->arr_idLopHoc ;
    	if(isset($request->arr_idLopHoc)){
    		$arr_idLopHoc = $request->arr_idLopHoc ;
    	}
        $username = $user->username ; 
        $sinhvien = SinhVien::where('masv',$username)->first() ;
        $ma_nganh = $sinhvien->chuyennganh ;
            
        $lophoc = LopHoc::join('namhoc','nam_hoc','namhoc.id')
            ->join('MonHoc','LopHoc.ma_mh','MonHoc.ma_mh')
            ->select('LopHoc.*','namhoc.nam','namhoc.hoc_ky','MonHoc.ten_mh','MonHoc.so_tinchi',
                    'MonHoc.hoc_phi')
            ->where('LopHoc.trang_thai',1)
            ->whereIn('LopHoc.ma_mh',function($query) use ($ma_nganh){
                $query->select(DB::raw('Nganhhoc_Monhoc.ma_mh'))
                        ->from('Nganhhoc_Monhoc')
                        ->where('Nganhhoc_Monhoc.ma_nganh',$ma_nganh)
                        ->orWhere('Nganhhoc_Monhoc.ma_nganh',1) ;
            })
            ->get() ;
       	$lichhoc_svdk = DB::table('LichHoc')
                ->join('LopHoc_LichHoc','Lichhoc.id','LopHoc_LichHoc.id_lichhoc')
                ->whereIn('LopHoc_LichHoc.id_lophoc',function($query) use($username){
                    $query->select(DB::raw('DsSinhvienDK.id_lophoc'))
                            ->from('DsSinhvienDK')
                            ->join('LopHoc','DsSinhvienDK.id_lophoc','LopHoc.id')
                            ->where('DsSinhvienDK.ma_sv','=',$username)
                            ->where('LopHoc.trang_thai',1)
                             ;
                })
                ->orWhere(function($query) use($arr_idLopHoc){
                	$query->whereIn('Lichhoc.id',function($query2) use($arr_idLopHoc){
                		$query2->select(DB::raw('LopHoc_LichHoc2.id_lichhoc '))
                				->from('LopHoc_LichHoc as LopHoc_LichHoc2')
                				->whereIn('LopHoc_LichHoc2.id_lophoc',$arr_idLopHoc) ;
                	});
                })
                ->select('LichHoc.*')->get() ;
        
        $lophoc_svdk = LopHoc::leftJoin('DsSinhVienDK','LopHoc.id','DsSinhVienDK.id_lophoc')
            ->where(array(
                ['LopHoc.trang_thai',1],
                ['DsSinhVienDK.ma_sv',$username]
            ))->orWhere(function($query3) use($arr_idLopHoc){
                $query3->whereIn('LopHoc.id',$arr_idLopHoc) ;
            })
            ->select('LopHoc.*')->get() ;
        $data_lophoc = array() ;
        $mamh_selected = array() ;
        $mamh_diemcao = array() ;
        $diem_sv = DB::table('Diem')->where('ma_sv',$username)->get();
        foreach($diem_sv as $dsv){
            $diemsv_diemchu =  $dsv->diem_chu ;
            $trangthaihoc = DB::table('LoaiHocPhi')->where("loai_diem","like","%$diemsv_diemchu%")->get() ;
            if(count($trangthaihoc) == 0){
                array_push($mamh_diemcao,$dsv->ma_mh) ;
                //echo "vào đây" ;
            }
           // var_dump($trangthaihoc) ;
        }

        foreach($lophoc_svdk as $value){
            array_push($mamh_selected, $value->ma_mh) ;
        }

        foreach($lophoc as $value)
        {
            $data = array() ;
            $data['id_lophoc'] = $value->id ;
            $data['ma_mh'] = $value->ma_mh ;
            $data['lop'] = $value->lop ;
            $data['so_luong'] = $value->so_luong ;
            $data['soluong_dk'] = $value->soluong_dk ;
            $data['hoc_ky'] = $value->hoc_ky ;
            $data['nam'] = $value->nam ;
            $data['ten_mh'] = $value->ten_mh ;
            $data['so_tinchi'] = $value->so_tinchi ;
            $data['hoc_phi'] = $value->hoc_phi ;
            $data['tinh_trang'] = '' ;
            $data['active'] = 1;
            $data['checkbox'] ='<input type="checkbox" value="'.$value->id.'">' ; 
            // Kiểm tra xem với mỗi môn học sinh viên đang truy cập vào có trung lịch học không 
             // phần quan trong nhất của trang web
            //khá khó  haha 
            $id_lophoc = $value->id ;
            
            $lichhoc_value = DB::table('LichHoc')
                ->whereIn('LichHoc.id',function($query) use($id_lophoc){
                    $query->select(DB::raw('LopHoc_LichHoc.id_lichhoc'))
                            ->from('LopHoc_LichHoc')
                            ->where('LopHoc_LichHoc.id_lophoc',$id_lophoc) ;    
                })->select('LichHoc.*')->get() ;
            $error_exists = '' ;
            $check_selected = '' ;
            $data['ngay_hoc'] = '' ;
            $data['tiet_hoc'] = '' ;
            $data['giang_duong'] = '' ;
            foreach($lichhoc_value as $key1 => $lh_value){
            	$data['ngay_hoc'] = $data['ngay_hoc'] . ',' . $lh_value->ngay_hoc ;
            	$data['tiet_hoc'] = $data['tiet_hoc'] . ',' . $lh_value->tiet_hoc ;
            	$data['giang_duong'] = $data['giang_duong'] . ',' . $lh_value->giang_duong ;
                foreach ($lichhoc_svdk as $key2 => $lhsvdk_value) {
                    if($lh_value->ngay_hoc == $lhsvdk_value->ngay_hoc){
                        $str1 = $lh_value->tiet_hoc ;
                        $str2 = $lhsvdk_value->tiet_hoc ;
                        $result = MyFunction::compareString($str1,$str2) ;
                        if(!empty($result)){
                            
                            $error_exists = 1 ;
                            break ;
                       }
                    }
                }
            }
           
           	$data['ngay_hoc'] = substr($data['ngay_hoc'], 1) ;
           	$data['tiet_hoc'] = substr($data['tiet_hoc'], 1) ;
           	$data['giang_duong'] = substr($data['giang_duong'], 1) ;
            
            if($value->so_luong == $value->soluong_dk){
                $data['active'] = 0 ;
                $check_selected = 1;
                $data['tinh_trang'] = "Môn học đã đủ số lượng đăng ký" ;
                
                $data['checkbox'] ='<input type="checkbox" value="'.$value->id.'" disabled>' ;
            }


           	if(in_array($value->id , $arr_idLopHoc)){
                if($value->so_luong > $value->soluong_dk){
                    $data['checkbox'] ='<input type="checkbox" value="'.$value->id.'" checked>' ;
                    $data['tinh_trang'] = 'Bạn đã chọn môn học này' ;
                    $data['soluong_dk']  = $data['soluong_dk'] + 1;
                    $check_selected = '' ;
                }
           		
           	}else{
           		
                if(in_array($value->ma_mh, $mamh_selected)){
                    $data['active'] = 0 ;
                    $error_exists = 1 ;
                   
                    $check_selected = 1;
                    $data['checkbox'] ='<input type="checkbox" value="'.$value->id.'" disabled>' ;
                }
                if(in_array($value->ma_mh, $mamh_diemcao)){
                   // $error_exists = 1 ;
                    $data['active'] = 0 ;
                    $check_selected = 1;
                    $data['tinh_trang'] = "Bạn đã học môn này và điểm cao " ;
                    $data['checkbox'] ='<input type="checkbox" value="'.$value->id.'" disabled>' ;
                }else{
                    
                }

           		if(!empty($error_exists)){
                    $data['active'] = 0 ;
                	if(empty($data['tinh_trang'])){
                        $data['tinh_trang'] = "Môn học bị trùng lịch học" ;
                    }else{
                        $data['tinh_trang'] = $data['tinh_trang']. " và trùng lịch học " ;
                    }
                   
                    $data['checkbox'] ='<input type="checkbox" value="'.$value->id.'" disabled>' ;
                    $check_selected = 1;
                }
           	}
            if(!empty($check_selected)){

            }
           	
            array_push($data_lophoc,$data) ;
        }
        return $data_lophoc ;
    }
    public function getLoginSinhVien(){

    	
    	if(Auth::check()){
            if(Auth::user()->level == 0){
                return Redirect::route('dangkymonhoc') ;
            }else{
                return view('pages.login_sinhvien') ;
            }
        }else{
            return view('pages.login_sinhvien') ;
        }
    }
    public function postLoginSinhVien(Request $request){
    	//return view('pages.login_sinhvien') ;
    	$rules = [	
        			'username' => 'required',
        			
	                'password'=>'required|min:6|max:32',
            	];

        $messages =  [	
		                'username.required'=>'Bạn chưa nhập tên đăng nhập',
		                'password.required'=>'Bạn chưa nhập mật khẩu',
		                'password.min'=>'mật khẩu phải có ít nhất 6 ký tự',
		                'password.max'=>'mật khẩu phải có nhiều nhất 32 ký tự',
		               
		               
	            	];
	            	//|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/
	    $this->validate($request,$rules,$messages) ;
	    $remember_me = ($request->remember_me == 'on') ? true : false ;
	    if(Auth::attempt(
	    				['username'=>$request->username,
	    				'password'=>$request->password,
	    				'level'=>0],
	    				$remember_me))
	    {
	    	
	    	return Redirect::route('dangkymonhoc') ;
	    }else{
	    	return Redirect::route('login_sinhvien')->with(['thongbao'=>'Đăng nhập thất bại']) ;
	    }
    }
    public function storeDanhSachDangKy(Request $request){
    	
    	
    	try{
    		$id_lophoc = $request->id_lophoc ;
	    	$ma_sv = Auth::user()->username ;
	    	$data_dsdk = array() ;
            $i = 0 ;
	    	foreach ($id_lophoc as $value){
	    		$data['id_lophoc'] = $value ;
	    		$data['ma_sv'] = $ma_sv ;
	    		$data['trang_thai'] = 'Học lần đầu' ;
	    		$lophoc = DB::table('LopHoc')
	    			->where('id',$value)->first();
                if($lophoc->so_luong > $lophoc->soluong_dk){

                    $diem = DB::table('Diem')
                        ->where(array(
                            ['ma_sv','=',$ma_sv],
                            ['ma_mh','=',$lophoc->ma_mh]
                        ))->first() ;
                    if(!empty($diem)){
                        $diem_chu = $diem->diem_chu ;
                        // echo $diem->diem_chu ;
                        // die ;
                        $loaihocphi = DB::table('LoaiHocPhi')
                            ->where('loai_diem','like',"%$diem_chu%")
                            ->first() ;
                           // echo "tới đây" ;
                        if(!empty($loaihocphi)){
                            $data['trang_thai']  = $loaihocphi->ten ;
                            //echo $loaihocphi->ten ;
                            // $data_lhsvdk['hoc_phi'] = $value->hoc_phi * $loaihocphi->heso_hocphi ;
                        }
                       // die ;
                    }

                    array_push($data_dsdk, $data) ;
                    DB::table('LopHoc')
                        ->where('id',$value)
                        ->update(['soluong_dk'=>($lophoc->soluong_dk +1) ]) ;
                    $i++ ;
                }
	    		
	    	}
            if(count($data_dsdk)!=0){
                DB::table('DSSinhVienDK')->insert($data_dsdk) ;
            }
	    	return $i ;
    	}catch(Exception $e){
    		return 'error' ;
    	}
    }
    public function getModalHuyMon($id){
        $model = 'pages/HuyMon' ;
        $confirm_route = $error = null ;
        try{
            $confirm_route = route('pages_huymon',['id' => $id]) ;
            return view('layouts.modal_confirmation',
                    ['error'=>$error,'model'=>$model,'confirm_route'=>$confirm_route]) ;
        }catch(Exception $e){
            $error = "Lỗi rồi" ;
            return view('layouts.modal_confirmation',
                    ['error'=>$error,'model'=>$model,'confirm_route'=>$confirm_route]) ;
        }
    }
    public function huyMonHoc($id){
       // echo "vào đay" ;
        try{
            $username = Auth::user()->username ;
            DB::table('DsSinhvienDK')->where(array(
                            ['ma_sv','=',$username],
                            ['id_lophoc','=',$id],
                        ))->delete() ;
            $lophoc = DB::table('LopHoc')->where('id',$id)->first() ;

            LopHoc::where('id',$id)->update(['soluong_dk'=>($lophoc->soluong_dk)-1]) ;
            return Redirect::back()->with('thongbao','Bạn đã hủy thành công 1 môn học') ;
        }catch(Exception $e){
            return Redirect::back()->with('thongbao','Lỗi rồi') ; ;
        }
    }
    public function getDangXuat(){
        Auth::logout() ;
        return Redirect::route('login_sinhvien') ;
    }
    public function getLichHocBySinhVien(){
        $username = Auth::user()->username ;
        $namhoc = NamHoc::join('LopHoc','NamHoc.id','LopHoc.nam_hoc')
                        ->select('NamHoc.*')->get() ;
        $arr_Nam = array() ;
        foreach($namhoc as $key => $value){
            $arr_valNam = explode('-', $value->nam) ;
            for($i = 0 ;$i<count($arr_valNam);$i++){
                array_push($arr_Nam,intval($arr_valNam[$i])) ;
            }
        }
        $maxNam = max($arr_Nam) ;
        $data_nam = array() ;

        foreach($namhoc as $key => $value){
            $arr_valNam = explode('-', $value->nam) ;
            if(in_array($maxNam,$arr_valNam)){
                $nh['id'] = $value->id ;
                $nh['hoc_ky'] = $value->hoc_ky ;
                $nh['nam'] = $value->nam ;
                array_push($data_nam,$nh) ;

            }
        }
        $year_newbest ; 
        if(count($data_nam)==1){
            $year_newbest = $data_nam ;
        }else{
            foreach($data_nam as $data){
                if(strpos($data['hoc_ky'], "2")){
                    $year_newbest = $data ;
                }
            }
        }
        //  echo '<pre>' ;
        // var_dump($year_newbest) ;
        // echo '</pre>' ;
        // die ;
        $lophoc_svdk = LopHoc::join('MonHoc','LopHoc.ma_mh','MonHoc.ma_mh')
            ->join('DsSinhVienDK','LopHoc.id','DsSinhVienDK.id_lophoc')
            ->select('LopHoc.*','MonHoc.ten_mh','MonHoc.so_tinchi',
                    'MonHoc.hoc_phi','DsSinhVienDK.trang_thai')
            ->where(array(
                ['LopHoc.nam_hoc',$year_newbest['id']],
                ['DsSinhVienDK.ma_sv',$username]
            ))
            ->get() ;
        $data_lophocsvdk = array() ; 
        $data_test = array() ;
        $thu = Lang::get('general.thu') ;
        for($i = 1;$i<=10;$i++){
            foreach($thu as $j => $val){
              //  $data_test[$i][$j] = '' ;
            }
        }
        foreach($lophoc_svdk as $value){
            
            $data_lhsvdk = array() ;
            
            $data_lhsvdk['id_lophoc'] = $value->id ;
            $data_lhsvdk['ma_mh'] = $value->ma_mh ;
            $data_lhsvdk['lop'] = $value->lop ;
            $data_lhsvdk['so_luong'] = $value->so_luong ;
            $data_lhsvdk['soluong_dk'] = $value->soluong_dk ;
          //  $data_lhsvdk['hoc_ky'] = $value->hoc_ky ;
          //  $data_lhsvdk['nam'] = $value->nam ;
            $data_lhsvdk['ten_mh'] = $value->ten_mh ;
            $data_lhsvdk['so_tinchi'] = $value->so_tinchi ;
            $data_lhsvdk['hoc_phi'] = $value->hoc_phi ;
            $data_lhsvdk['trang_thai'] = $value->trang_thai ;
            $diem = DB::table('Diem')
                ->where(array(
                    ['ma_sv','=',$username],
                    ['ma_mh','=',$value->ma_mh]
                ))->first() ;
            if(!empty($diem)){
                $diem_chu = $diem->diem_chu ;
                $loaihocphi = DB::table('LoaiHocPhi')
                    ->where('loai_diem','like',"%$diem_chu%")
                    ->first() ;
                if(!empty($loaihocphi)){
                    //$data['trang_thai']  = $loaihocphi->ten ;
                    $data_lhsvdk['hoc_phi'] = $value->hoc_phi * $loaihocphi->heso_hocphi ;
                }
                
            }
            $id_lh = $value->id ;
            $lichhoc_val = DB::table('LichHoc')
                ->whereIn('LichHoc.id',function($query) use($id_lh){
                    $query->select(DB::raw('LopHoc_LichHoc.id_lichhoc'))
                            ->from('LopHoc_LichHoc')
                            ->where('LopHoc_LichHoc.id_lophoc',$id_lh) ;    
                })->select('LichHoc.*')->get() ;
           
            $data_lhsvdk['ngay_hoc'] = '' ;
            $data_lhsvdk['tiet_hoc'] = '' ;
            $data_lhsvdk['giang_duong'] = '' ;
            foreach($lichhoc_val as $lh){
                $data_lhsvdk['ngay_hoc'] = $data_lhsvdk['ngay_hoc'].','.$lh->ngay_hoc ;
                $data_lhsvdk['tiet_hoc'] = $data_lhsvdk['tiet_hoc'] .','.$lh->tiet_hoc ;
                $data_lhsvdk['giang_duong'] = $data_lhsvdk['giang_duong'].','.$lh->giang_duong;
                // $arr_str = explode('-', $lh->tiet_hoc) ;
                // for($i = intval($arr_str[0]);$i <= intval($arr_str[count($arr_str) -1]); $i++){
                //     $data_test[$i][$lh->ngay_hoc] = $value->ma_mh.'('.$lh->giang_duong.')' ;
                // }
                $str_ngayhoc = $lh->ngay_hoc ;
                $data_test[$lh->tiet_hoc.'_'.(intval($str_ngayhoc[1])-1)] = $value->ma_mh.'('.$lh->giang_duong.')' ;
            }
            
            $data_lhsvdk['ngay_hoc'] = substr($data_lhsvdk['ngay_hoc'], 1) ;
            $data_lhsvdk['tiet_hoc'] = substr($data_lhsvdk['tiet_hoc'], 1) ;
            $data_lhsvdk['giang_duong'] = substr($data_lhsvdk['giang_duong'], 1) ;

            array_push($data_lophocsvdk,$data_lhsvdk) ;
        }
       // var_dump($data_test); die ;
        return view('pages.lichhoc')->with([
            'data_lophocsvdk'=>$data_lophocsvdk,
            'namhoc'=>$year_newbest,
            'data_test' => $data_test,
        ]) ;
    }
    public function get_changePassword()
    {
        return view('pages.doimatkhau') ;

    }
    public function post_changePassword(Request $request)

    {
        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator)
        {

            return Hash::check($value, current($parameters));

        });

        //'old_password' => 'required|old_password:' . Auth::user()->password
        $rules = [  'passwordOld' => 'required|old_password:'.Auth::user()->password,
                    'password'=>'required|min:6|max:32',
                    'passwordAgain'=>'required|same:password',
                   
                    
                ];

        $messages =  [  'passwordOld.old_password' => 'bạn nhập sai mật khẩu',
                        'password.required'=>'Bạn chưa nhập mật khẩu',
                        'password.min'=>'mật khẩu phải có ít nhất 6 ký tự',
                        'password.max'=>'mật khẩu phải có nhiều nhất 32 ký tự',
                        'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                        'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp',
                    ];
        $this->validate($request,$rules,$messages) ;
        $username = Auth::user()->username ;
       // $user = User::where('username',$username) ;

        $data['password'] = bcrypt($request->password) ;
        User::where('username',$username)->update($data) ;
        return Redirect::back()->with(['thongbao'=>'Bạn đã đổi mật khẩu thành công']) ;
    }
    public function DanhSachDiem(){
        if(Auth::check()){
            $user = Auth::user() ;
        }
        $diem = DB::table('Diem')
            ->join('MonHoc','Diem.ma_mh','MonHoc.ma_mh')
            ->where('Diem.ma_sv',$user->username)
            ->select('Diem.*','MonHoc.ten_mh','MonHoc.so_tinchi')
            ->get() ;
        $namhoc = NamHoc::whereIn('id',function($query){
                $query->select(DB::raw('Diem.nam_hoc'))
                        ->from('Diem') ;
            })->get() ;
        $arr_diem = array() ;
        $tong_tinchi = 0;
        $diem_tb = 0 ;
        $tong_tinchitl = 0 ;
        foreach ($diem as $key => $value) {
          
            array_push($arr_diem,(array)$value) ;
            $tong_tinchi += $value->so_tinchi ;
            $diem_tb += ($value->diem_heso2 * $value->so_tinchi) ;
            if($value->diem_heso1 >= 4){
                $tong_tinchitl += $value->so_tinchi ;
            }
        }
        if($tong_tinchi !=0 ){
            $diem_tb = $diem_tb/$tong_tinchi ;
        }
       
        $data_diem = array() ;
        foreach($namhoc as $value){
            
            $data = array() ;
            foreach($arr_diem as $val_diem){
                
                if($value->id == $val_diem['nam_hoc']){
                    array_push($data,$val_diem) ;
                }
            }
            $data_diem[$value->hoc_ky.' Năm học '.$value->nam] = $data ;
        }
       // var_dump($data_diem) ;
       // die ;
        return view('pages.ketquahoc')->with([
            'diemsv'=>$data_diem,
            'tong_tinchi' =>$tong_tinchi,
            'tong_tinchitl' =>$tong_tinchitl,
            'diem_tb' =>$diem_tb
        ]) ;
    }
}
