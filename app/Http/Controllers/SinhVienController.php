<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChuyenNganh ;
use App\SinhVien ;
use App\NamHoc ;
use App\MonHoc ;
use Carbon\Carbon ;
use Exception ;
use Session;
use DB ;
use Lang ;
use Redirect;
class SinhVienController extends Controller
{
    //
    public function index(){
        $sinhvien = SinhVien::all() ;

    	return view('admin.sinhvien.index')->with(['sinhvien' => $sinhvien]) ;

    }
    public function create(){
        $chuyennganh = ChuyenNganh::where('ma_nganh','!=',1)->get() ;
        $data = array() ;
        foreach($chuyennganh as $key => $value)
        {
            $data[$value->ma_nganh] = $value->ten_nganh ;
        }
        $gioitinh = Lang::get('general.gioitinh') ;
    	return view('admin.sinhvien.create')->with([
                                'chuyennganh'=>$data,
                                'gioitinh'  =>$gioitinh
                            ]) ;
    }
    public function store(Request $request)
    {
        $this->validate($request,
                        [   'ma_sv'=>'required|unique:SinhVien,masv|digits_between:8,10|',
                            'hoten' =>'required|min:3',
                            'gioitinh'=>'required'
                        ],
                        [
                            'ma_sv.required' => 'Bạn chưa điền mã sinh viên',
                            'ma_sv.unique'   => ' Mã sinh viên đã tồn tại',
                           
                            'ma_sv.digits_between' => 'Mã sinh viên nằm trong khoảng 8 đến 10 số',
                          
                            'hoten.required' =>'Bạn chưa điền tên sinh viên',
                            'hoten.min'      => 'Tên sinh viên có ít nhất 3 ký tự',
                            'gioitinh.required'=>'Bạn chưa điền giới tính'
                        ]
                    );
       
        $sinhvien = new SinhVien() ;
        $sinhvien->masv = $request->ma_sv ;
        $sinhvien->hoten = $request->hoten ;
        $sinhvien->ngaysinh = Carbon::parse($request->ngaysinh)->format('Y-m-d') ;
        $sinhvien->noisinh = $request->noisinh ;
        $sinhvien->email = $request->email ;
        $sinhvien->chuyennganh = $request->chuyennganh ;
        $sinhvien->gioitinh = $request->gioitinh ;
        $sinhvien->save() ;
        return redirect('sinhvien/create')->with(['thongbao'=>'Thêm thành công']) ;
    }
    public function edit($ma_sv){
        $chuyennganh = ChuyenNganh::where('ma_nganh','!=',1)->get() ;
        $sinhvien = SinhVien::where('masv',$ma_sv)->firstOrFail() ;
        //echo $sinhvien->hoten ; die ;

        $data = array() ;
        foreach($chuyennganh as $key => $value)
        {
            $data[$value->ma_nganh] = $value->ten_nganh ;
        }
        return view('admin.sinhvien.edit')->with(['chuyennganh'=>$data,'sinhvien'=>$sinhvien]) ;
    	
    }
    public function update(Request $request , $ma_sv)
    {
        $rules = [  'ma_sv'=>'required|between:8,10',
                    'hoten' =>'required|min:3'
                 ] ;
        $messages = [
                        'ma_sv.required' => 'Bạn chưa điền mã sinh viên',
                    
                        'ma_sv.between'      => 'Mã sinh viên nằm trong khoảng 8 đến 10 số',
                      
                        'hoten.required' =>'Bạn chưa điền tên sinh viên',
                        'hoten.min'      => 'Tên sinh viên có ít nhất 3 ký tự',
                    ] ;
        if($request->ma_sv != $ma_sv)
        {
            $rules['ma_sv'] = $rules['ma_sv'].'|unique:SinhVien,masv' ;
            $messages['ma_sv.unique'] = ' Mã sinh viên đã tồn tại' ;
        }
        $this->validate($request,$rules,$messages);
       
       
        $data['masv'] = $request->ma_sv ;
        $data['hoten'] = $request->hoten ;
        $data['ngaysinh'] = Carbon::parse($request->ngaysinh)->format('Y-m-d') ;
        $data['noisinh'] = $request->noisinh ;
        $data['email'] = $request->email ;
        $data['chuyennganh'] = $request->chuyennganh ; 
        SinhVien::where('masv',$ma_sv)->update($data) ;
        return redirect('sinhvien/edit/'.$request->ma_sv)->with(['thongbao'=>'Sửa thành công']) ;
    }
    public function getModal($ma_sv)
    {
        $model = 'sinhvien' ;
        $confirm_route = $error = null ;
        try{
            $confirm_route = route('sinhvien_delete',['ma_sv' => $ma_sv]) ;
            return view('admin.layout.modal_confirmation',['model' => $model ,'error'=>$error,'confirm_route' => $confirm_route]) ;
        }catch(Exception $e){
            $error = " Lỗi rồi " ;
            return view('admin.layout.modal_confirmation',['model' => $model ,'error'=>$error,'confirm_route' => $confirm_route]) ;
        }
    }
    public function delete($ma_sv){
        // echo "vào đây haha\n" ;
        // echo $ma_sv ;
        // die;
        try{
            SinhVien::where('masv',$ma_sv)->delete() ;
            return redirect('sinhvien')->with(['thongbao'=>'Xóa thành công']) ;
        }catch(Exception $e){
            return redirect('sinhvien')->with(['thongbao'=>'Không xóa được']) ;
        }
    }
    public function getDiemBySinhVien($ma_sv){
        $diemsv = DB::table('Diem')->join('sinhvien','Diem.ma_sv','sinhvien.masv')
                                ->join('MonHoc','Diem.ma_mh','MonHoc.ma_mh')
                                ->join('NamHoc','Diem.nam_hoc','NamHoc.id')
                                ->select('Diem.*','sinhvien.hoten as ten_sv','MonHoc.ten_mh',
                                        'MonHoc.so_tinchi',
                                        'NamHoc.nam', 'NamHoc.hoc_ky')
                                ->where('Diem.ma_sv',$ma_sv)
                                ->orderBy('NamHoc.nam','DESC')
                                ->orderBy('NamHoc.hoc_ky','DESC')
                                ->get() ;
        $sinhvien = SinhVien::where('masv',$ma_sv)->first();
        return view('admin.sinhvien.view_diem')->with(['diemsv'=>$diemsv,'sinhvien'=>$sinhvien]) ;
    }
    public function createDiem($ma_sv){
        $sinhvien = SinhVien::where('masv',$ma_sv)->first();
        $namhoc_list = NamHoc::orderBy('nam','DESC')->orderBy('hoc_ky','DESC')->get() ;
        $namhoc = array() ;
        foreach($namhoc_list as $key =>$value){
            $namhoc[$value->id] = $value->hoc_ky.' Năm học '.$value->nam ;
        }
        $monhoc_list = MonHoc::all() ;
        $diem_hechu = Lang::get('general.diem_hechu') ;
        return view('admin.sinhvien.createDiemBySinhvien')->with([
                                                'namhoc'=>$namhoc,
                                                'sinhvien'=>$sinhvien,
                                                'monhoc'=>$monhoc_list,
                                                'diem_hechu'=>$diem_hechu
        ]) ;
    }
    public function storeDiem(Request $request,$ma_sv){
        $rules = [  'diem_heso1' => 'required|numeric|min:0|max:10',
                    'diem_heso2' => 'required|numeric|min:0|max:4'
        ];
        $messages = [   'diem_heso1.required' => 'Bạn chưa điền điểm hệ số 10',
                        'diem_heso1.numeric' => 'Điểm hệ số 10 là số thực',
                        'diem_heso1.min' => 'Điểm hệ số 10 nhỏ nhất là 0',
                        'diem_heso1.max' => 'Điểm hệ số 10 lớn nhất là 10',
                        'diem_heso2.required' => 'Bạn chưa điền điểm hệ số 4',
                        'diem_heso2.numeric' => 'Điểm hệ số 10 là số thực',
                        'diem_heso2.min' => 'Điểm hệ số 10 nhỏ nhất là 0',
                        'diem_heso2.max' => 'Điểm hệ số 10 lớn nhất là 4',
        ];
        $this->validate($request,$rules,$messages) ;
        $data = [   'ma_sv' =>$ma_sv,
                    'ma_mh' =>$request->ma_mh,
                    'diem_heso1' =>$request->diem_heso1,
                    'diem_heso2' =>$request->diem_heso2,
                    'diem_chu' =>$request->diem_hechu,
                    'nam_hoc' =>$request->namhoc,
        ];
        $diemsv = DB::table('Diem')->where(array(
                                    ['ma_sv','=',$ma_sv],
                                    ['ma_mh','=',$request->ma_mh]
                                ))->first() ;
        //var_dump($diemsv) ; die ;
        if(empty($diemsv)){
            DB::table('Diem')->insert($data) ;
            return Redirect::route('sinhvien_getDiemBySinhVien',$ma_sv)->with(['thongbao'=>'thêm thành công']) ;
        }else{
            DB::table('Diem')->where('id',$diemsv->id)->update($data) ;
            return Redirect::route('sinhvien_getDiemBySinhVien',$ma_sv)->with(['thongbao'=>'Diểm đã được update được']) ;
        }
    }
}
