<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth ;
use Illuminate\Http\Request;
use Fpdf ;
use PDF ;
use DB;
use App\LopHoc ;
use App\SinhVien ;
use Carbon\Carbon ;
use filePDF ;
use App ;
use SnapPDF ;
use Redirect ;
class FpdfController extends Controller
{
    //

    function exportPDF($id_namhoc){
    	//$ma_sv = 14000214 ;
    	$ma_sv = '' ;
    	if(Auth::check()){
    		$ma_sv = Auth::user()->username ;
    	}

    	//$id_namhoc = 21 ;
    	$sinhvien = SinhVien::where('masv',$ma_sv)->first() ;
    	$year_now = date('Y') ;
    	$month_now = date('m') ;
    	$day_now = date('d') ;

    	//echo $day_now ; 
    	//die ;

    	$lophoc_svdk = LopHoc::join('MonHoc','LopHoc.ma_mh','MonHoc.ma_mh')
            ->join('DsSinhVienDK','LopHoc.id','DsSinhVienDK.id_lophoc')
            ->select('LopHoc.*','MonHoc.ten_mh','MonHoc.so_tinchi',
                    'MonHoc.hoc_phi','DsSinhVienDK.trang_thai')
            ->where(array(
                ['LopHoc.nam_hoc',$id_namhoc],
                ['DsSinhVienDK.ma_sv',$ma_sv]
            ))
            ->get() ;
        //var_dump($lophoc_svdk) ; die ;
        if(count($lophoc_svdk) == 0){
        	echo "vào đây" ;
        	return Redirect::back()->with(['thongbao'=>'Không có dữ liệu để in']) ;
        }
        $data_lophocsvdk = array() ; 
        foreach($lophoc_svdk as $key => $value){
            
            $data_lhsvdk = array() ;
            
            $data_lhsvdk['id_lophoc'] = $key + 1 ;
            $data_lhsvdk['ma_mh'] = $value->ma_mh ;
            
           // $data_lhsvdk['so_luong'] = $value->so_luong ;
           // $data_lhsvdk['soluong_dk'] = $value->soluong_dk ;
          	
          	$data_lhsvdk['ten_mh'] = $value->ten_mh ;
          	
           	$data_lhsvdk['so_tinchi'] = $value->so_tinchi ;
           
	        $data_lhsvdk['trang_thai'] = $value->trang_thai ;
            //$data_lhsvdk['trang_thai'] = 'Học lần đầu' ;
            $data_lhsvdk['hoc_phi'] = $value->hoc_phi ;
           

            $diem = DB::table('Diem')
                ->where(array(
                    ['ma_sv','=',$ma_sv],
                    ['ma_mh','=',$value->ma_mh]
                ))->first() ;
            if(!empty($diem)){
                $diem_chu = $diem->diem_chu ;
                $loaihocphi = DB::table('LoaiHocPhi')
                    ->where('loai_diem','like',"%$diem_chu%")
                    ->first() ;
                if(!empty($loaihocphi)){
                    //$data['trang_thai']  = $loaihocphi->ten ;
                    $data_lhsvdk['hoc_phi'] = $value->hoc_phi * $loaihocphi->heso_hocphi ;
                }
                
            }
            $data_lhsvdk['lop'] =  $value->ma_mh .' '.$value->lop ;
            $id_lh = $value->id ;
            $lichhoc_val = DB::table('LichHoc')
                ->whereIn('LichHoc.id',function($query) use($id_lh){
                    $query->select(DB::raw('LopHoc_LichHoc.id_lichhoc'))
                            ->from('LopHoc_LichHoc')
                            ->where('LopHoc_LichHoc.id_lophoc',$id_lh) ;    
                })->select('LichHoc.*')->get() ;
           
            $data_lhsvdk['ngay_hoc'] = '' ;
            $data_lhsvdk['tiet_hoc'] = '' ;
            $data_lhsvdk['giang_duong'] = '' ;
            foreach($lichhoc_val as $lh){
                $data_lhsvdk['ngay_hoc'] = $data_lhsvdk['ngay_hoc'].','.$lh->ngay_hoc ;
                $data_lhsvdk['tiet_hoc'] = $data_lhsvdk['tiet_hoc'] .','.$lh->tiet_hoc ;
                $data_lhsvdk['giang_duong'] = $data_lhsvdk['giang_duong'].','.$lh->giang_duong;
            }
            
            $data_lhsvdk['ngay_hoc'] = substr($data_lhsvdk['ngay_hoc'], 1) ;
            $data_lhsvdk['tiet_hoc'] = substr($data_lhsvdk['tiet_hoc'], 1) ;
            $data_lhsvdk['giang_duong'] = substr($data_lhsvdk['giang_duong'], 1) ;

            array_push($data_lophocsvdk,$data_lhsvdk) ;
        }
       // var_dump($data_lophocsvdk) ; die ;
    	// function Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M')
    	 // function SetFont($family, $style='', $size=null, $fontfile='', $subset='default', $out=true)
    	$pdf = new PDF("L", "mm", "A4", true, 'UTF-8', false);
    	$pdf::SetTitle('Xem lịch học');
    	$pdf::SetAutoPageBreak(true, 25);
    	$pdf::SetMargins(10, 12, 10);
    	//$pdf::AliasNbPages();
	  	$pdf::AddPage();

	  	// $pdf::writeHTMLCell(0, 10, '', '', '<meta charset="utf-8">hào phạm', 0, 1, 0, true, '', true);
	  	
	  	$pdf::SetFont('freeserif', 'L', 12);
	  	//$pdf::Ln(10) ;
	  	$pdf::cell(20,8,"","C");
		$pdf::cell(40,8, 'ĐẠI HỌC QUỐC GIA HÀ NỘI',0,'','C');
		$pdf::cell(50,8,"","C");
		$pdf::SetFont('freeserif', 'B', 12);
		$pdf::cell(60,8, 'CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM',0,'','C');
		$pdf::Ln() ;
		$pdf::SetFont('freeserif', 'B', 12);
		$pdf::cell(15,8,"","C");
		$pdf::cell(50,8, 'TRƯỜNG ĐẠI HỌC KHOA HỌC TỰ NHIÊN',0,'','C');
		$pdf::cell(50,8,"","C");
		$pdf::cell(50,8, 'Độc lập-Tự do-Hạnh phúc',0,'','C');
		//$pdf::Ln() ;
		//$pdf::SetMargins(100,0,0,0) ;
		$pdf::SetFont('freeserif', 'B', 13);
		//$pdf::cell(30,20,"","C");
		$pdf::Ln(15) ;
		$pdf::cell(0,8, 'KẾT QUẢ ĐĂNG KÝ MÔN HỌC - HỌC KỲ 2 NĂM HỌC 2017-2018',0,'','C');
		$pdf::Ln() ;
		$pdf::SetFont('freeserif', '', 12);
		$pdf::cell(0,8, 'Ngày '.$day_now.' tháng '.$month_now.' năm '.$year_now,0,'','C');
		$pdf::Ln(15) ;
		$pdf::SetFont('freeserif', '', 13);
		//$pdf::cell(15,8,"","C");
		$pdf::cell(20,8, 'Họ và tên:',0,'','C');
		$pdf::cell(5,8,"","C");
		$pdf::SetFont('freeserif', 'B', 13);
		$pdf::cell(30,8, $sinhvien->hoten,0,'','C');
		$pdf::cell(30,8,"","C");
		$pdf::SetFont('freeserif', '', 13);
		$pdf::cell(20,8, 'Ngày sinh: ',0,'','C');
		$pdf::cell(5,8,"","C");
		$pdf::SetFont('freeserif', 'B', 13);
		$pdf::cell(10,8,Carbon::parse($sinhvien->ngaysinh)->format('d/m/Y'),0,'','C');
		$pdf::SetFont('freeserif', '', 13);
		$pdf::cell(20,8,"","C");
		$pdf::cell(20,8, 'Mã sinh viên: ',0,'','C');
		$pdf::cell(5,8,"","C");
		$pdf::SetFont('freeserif', 'B', 13);
		$pdf::cell(15,8, $sinhvien->masv,0,'','C');
		$pdf::Ln() ;
		$pdf::SetFont('freeserif', '', 13);
		//$pdf::cell(15,8,"","C");
		$pdf::cell(20,8, '  Ngành học:',0,'','C');
		$pdf::cell(5,8,"","C");
		$pdf::SetFont('freeserif', 'B', 13);
		$pdf::cell(80,8, $sinhvien->getChuyenNganh->ten_nganh,0,'','C');
		$pdf::Ln(20) ;
		// $pdf->MultiCell(55, 5, '[LEFT] '.$txt, 1, 'L', 1, 0, '', '', true);
		// $pdf->MultiCell(55, 5, '[RIGHT] '.$txt, 1, 'R', 0, 1, '', '', true);

		// function MultiCell($w, $h, $txt, $border=0, $align='J', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='T', $fitcell=false) 
		$pdf::SetFont('freeserif', 'B', 12);
		$width = 20 ;
		$height = 8 ;
		$height1 = 8 ;
		$thead = array( 'STT',
						'Mã môn học',
						'Môn học',
						'Số tín chỉ',
						'Trạng thái',
						'Học phí',
						'Lớp môn học',
						'Thứ',
						'Tiết',
						'Giảng đường'
		) ;
		$arr_width = array( $width-8,
							$width,
							$width+20,
							$width-5,
							$width,
							$width-5,
							$width,
							$width-5,
							$width-8,
							$width+2
		) ;
		$row_last = array(	'',
							'',
							'Tổng',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
		) ;
		$arr_height = array() ;

		foreach($thead as $key => $value)
		{
			$val_height = ceil($pdf::getStringHeight($arr_width[$key],$value)) ;
			array_push($arr_height,$val_height) ;
			//echo $val_height.'<br>' ;
		}
		
		if($height < max($arr_height)) {
			$height = max($arr_height) ;
		}
		
		foreach($thead as $key => $value){
			$pdf::MultiCell($arr_width[$key],$height,$value, 1, 'C', 0, 0, '', '', true,0,false,true,$height,'M',false);
		}
		
		$pdf::Ln() ;
		$pdf::SetFont('freeserif', '', 11);
		//$arr = array() ; 
		$pagehei = $pdf::getPageHeight();
		$height = $height1 ;
		$tong_tinchi = 0 ;
		$tong_hocphi = 0 ;
		foreach($data_lophocsvdk as $key =>$data){
			$tong_hocphi += $data['hoc_phi'] ;
			$tong_tinchi += $data['so_tinchi'] ;
			$arr = array() ;
			$i = 0 ;
			foreach($data as $key2 => $value){
				$val_height = ceil($pdf::getStringHeight($arr_width[$i],$value)) ;
				array_push($arr,$val_height) ;
			//	echo $val_height.'<br>' ;
				$i++ ;
			}
			//die ;
			if($height < max($arr)) $height = max($arr) ;
			$x = $pdf::GetX();
            $y = $pdf::GetY();                                            
            //$slen    = $pdf::GetStringWidth($array2[$key]);
            $hei = $pagehei - $y -25 ;
            if($hei < $height){
            	//echo $x .'  '. $y. '  '.$pagehei ; die ;
            	$pdf::AddPage() ;
            }

            $j = 0 ;
            foreach($data as $data_val){
            	$pdf::MultiCell($arr_width[$j],$height,$data_val, 1, 'C', 0, 0, '', '', true,0,false,true,$height,'M',false);
            	$j ++ ;
            }
			
			$pdf::Ln() ;
			$height = $height1 ;
		}
		$row_last[3] = $tong_tinchi ;
		$row_last[5] = $tong_hocphi ;
		$arr2 = array() ;
		foreach($row_last as $key => $value){
			
			$val_height = ceil($pdf::getStringHeight($arr_width[$key],$value)) ;
			array_push($arr2,$val_height) ;
		}
		if($height < max($arr2)) $height = max($arr2) ;
		$x = $pdf::GetX();
        $y = $pdf::GetY();                                            
        $hei = $pagehei - $y -25 ;
        if($hei < $height){
        	
        	$pdf::AddPage() ;
        }
		foreach($row_last as $key => $value){
			$pdf::MultiCell($arr_width[$key],$height,$value, 1, 'C', 0, 0, '', '', true,0,false,true,$height,'M',false);
		}
		$height = $height1 ;
		$pdf::Ln(10) ;
		$pdf::SetFont('freeserif', 'B', 11);
		$pdf::cell(120,8, 'Tổng số học phí đã làm tròn: '.number_format($tong_hocphi,2,',','.').' (đồng)',0,'','L');
		
		$pdf::Output('xem lịch học.pdf', 'I');
    }

    function exportPDF_LopHoc($id_lophoc){
    	//$id_lophoc = 3 ;
    	$lophoc = LopHoc::join('NamHoc','LopHoc.nam_hoc','NamHoc.id')
    			->where('LopHoc.id',$id_lophoc)
    			->select('LopHoc.*','NamHoc.hoc_ky','NamHoc.nam')
    			->first() ;

        $sinhvien = SinhVien::join('DsSinhVienDK','SinhVien.masv','DsSinhVienDK.ma_sv')
                    ->where('DsSinhVienDK.id_lophoc',$id_lophoc)
                    ->get();
        $data_sinhvien = array() ;
        if(count($sinhvien) ==0){
        	return Redirect::back()->with(['thongbao'=>'Không có dữ liệu để in']) ;
        }
        foreach($sinhvien as $key=>$value){
        	$data = array() ; 
        	array_push($data,$key+1) ;
        	array_push($data,$value->masv) ;
        	array_push($data,$value->hoten) ;
        	array_push($data,Carbon::parse($value->ngaysinh)->format('d/m/Y')) ;
        	array_push($data,$value->getChuyenNganh->ten_nganh) ;
        	
        	if(!is_null($value->trang_thai)){
        		array_push($data,$value->trang_thai) ;
        	}else{
        		array_push($data,'') ;
        	}
        	
        	array_push($data_sinhvien,$data) ;
        }
        //var_dump($data_sinhvien) ; die ;
    	$year_now = date('Y') ;
    	$month_now = date('m') ;
    	$day_now = date('d') ;
    	$pdf = new PDF("L", "mm", "A4", true, 'UTF-8', false);
    	$pdf::SetTitle('Xem danh sách sinh viên');
    	$pdf::SetAutoPageBreak(true, 25);
    	$pdf::SetMargins(10, 12, 10);
    	//$pdf::AliasNbPages();
	  	$pdf::AddPage();

	  	// $pdf::writeHTMLCell(0, 10, '', '', '<meta charset="utf-8">hào phạm', 0, 1, 0, true, '', true);
	  	
	  	$pdf::SetFont('freeserif', 'L', 12);
	  	//$pdf::Ln(10) ;
	  	$pdf::cell(20,8,"","C");
		$pdf::cell(40,8, 'ĐẠI HỌC QUỐC GIA HÀ NỘI',0,'','C');
		$pdf::cell(50,8,"","C");
		$pdf::SetFont('freeserif', 'B', 12);
		$pdf::cell(60,8, 'CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM',0,'','C');
		$pdf::Ln() ;
		$pdf::SetFont('freeserif', 'B', 12);
		$pdf::cell(15,8,"","C");
		$pdf::cell(50,8, 'TRƯỜNG ĐẠI HỌC KHOA HỌC TỰ NHIÊN',0,'','C');
		$pdf::cell(50,8,"","C");
		$pdf::cell(50,8, 'Độc lập-Tự do-Hạnh phúc',0,'','C');
		//$pdf::Ln() ;
		//$pdf::SetMargins(100,0,0,0) ;
		$pdf::SetFont('freeserif', 'B', 13);
		//$pdf::cell(30,20,"","C");
		$pdf::Ln(15) ;
		$pdf::cell(0,8, 'DANH SÁCH SINH VIÊN - '.$lophoc->hoc_ky.' NĂM HỌC '.$lophoc->nam,0,'','C');
		$pdf::Ln() ;
		$pdf::SetFont('freeserif', '', 12);
		$pdf::cell(0,8, 'Ngày '.$day_now.' tháng '.$month_now.' năm '.$year_now,0,'','C');
		$pdf::Ln(15) ;
		$pdf::SetFont('freeserif', '', 12);
		//$pdf::cell(15,8,"","C");
		$pdf::cell(25,8, 'Mã môn học:',0,'','C');
		$pdf::cell(10,8,"","C");
		$pdf::SetFont('freeserif', 'B', 12);
		$pdf::cell(15,8, $lophoc->ma_mh,0,'','C');
		$pdf::cell(30,8,"","C");
		$pdf::SetFont('freeserif', '', 12);
		$pdf::cell(20,8, 'Môn học: ',0,'','C');
		$pdf::SetFont('freeserif', 'B', 12);
		$pdf::cell(100,8,$lophoc->getMonHoc->ten_mh,"C");
		$pdf::SetFont('freeserif', 'B', 12);
		//$pdf::cell(10,8,'',0,'','C');
		
		$pdf::Ln() ;
		$pdf::SetFont('freeserif', '', 12);
		$pdf::cell(27,8, 'Lớp môn học: ',0,'','C');
		$pdf::cell(5,8,"","C");
		$pdf::SetFont('freeserif', 'B', 12);
		$pdf::cell(20,8, $lophoc->ma_mh.' '.$lophoc->lop,0,'','C');
		$pdf::Ln(20) ;
		// $pdf->MultiCell(55, 5, '[LEFT] '.$txt, 1, 'L', 1, 0, '', '', true);
		// $pdf->MultiCell(55, 5, '[RIGHT] '.$txt, 1, 'R', 0, 1, '', '', true);

		// function MultiCell($w, $h, $txt, $border=0, $align='J', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='T', $fitcell=false) 
		$pdf::SetFont('freeserif', 'B', 12);
		$width = 30 ;
		$height = 8 ;
		$height1 = 8 ;
		$thead = array( 'STT',
						'Mã sinh viên',
						'Họ và tên',
						'Ngày sinh',
						'Ngành học',
						'Trạng thái',
						
		) ;
		$arr_width = array( $width-20,
							$width,
							$width+10,
							$width-5,
							$width+25,
							$width,
							
		) ;
		$row_last = array(	'',
							'',
							'Tổng',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
		) ;
		$arr_height = array() ;

		foreach($thead as $key => $value)
		{
			$val_height = ceil($pdf::getStringHeight($arr_width[$key],$value)) ;
			array_push($arr_height,$val_height) ;
			//echo $val_height.'<br>' ;
		}
		
		if($height < max($arr_height)) {
			$height = max($arr_height) ;
		}
		
		foreach($thead as $key => $value){
			$pdf::MultiCell($arr_width[$key],$height,$value, 1, 'C', 0, 0, '', '', true,0,false,true,$height,'M',false);
		}
		
		$pdf::SetFont('freeserif', '', 11);
		$pdf::Ln() ;
		$pagehei = $pdf::getPageHeight();
		$height = $height1 ;
		//echo $pdf::getStringHeight($width,'') ; die ;
		foreach($data_sinhvien as $key => $value){

			$arr = array() ;
			foreach($value as $key1 => $data1){
				$val_height = ceil($pdf::getStringHeight($arr_width[$key1],$data1)) ;
				array_push($arr,$val_height) ;
			}
			if($height1 < max($arr)) $height = max($arr) ;
			$x = $pdf::GetX() ;
			$y = $pdf::GetY() ;
			$hei = $pagehei - $y - 25 ;
			if($hei < $height) {
				$pdf::AddPage() ;
			}

			foreach($value as $key2 => $data2){
				$pdf::MultiCell($arr_width[$key2],$height,$data2, 1, 'C', 0, 0, '', '', true,0,false,true,$height,'M',false);

			}
			$pdf::Ln() ;
			$height = $height1 ;
		}
		$pdf::Output('xem lịch học.pdf', 'I');
    }
    public function pdfView(){
    	//$pdf = new filePDF() ;
    	$pdf = SnapPDF::loadView('filePDF/test') ;
    	//$pdf->setOptions(['encoding' =>'utf-8','defaultFont' => 'fixed']);
    	//return $pdf->download('test.pdf') ;
  //   	$pdf = App::make('dompdf.wrapper');
		// $pdf->loadHTML('<h1>đại học</h1>');
		return $pdf->download('test_pdf.pdf');

    }
}
