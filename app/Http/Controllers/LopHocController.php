<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lang ;
use App\MonHoc ;
use App\NamHoc ;
use App\LopHoc ;
use App\LichHoc ;
use DB;
use Exception ;
use Redirect ;
use App\SinhVien ;
use MyFunction ;
class LopHocController extends Controller
{
    //
    public function index(){
        $lophoc = LopHoc::join('namhoc','nam_hoc','namhoc.id')
       
        ->select('LopHoc.*',
            'namhoc.nam','namhoc.hoc_ky')
        ->orderBy('namhoc.nam','DESC')
        ->orderBy('namhoc.hoc_ky','DESC')->get() ;
       
        foreach($lophoc as $key =>$value){
            $id_lophoc = $value->id ;
            $lichhoc = LichHoc::whereIn('LichHoc.id',function($query) use($id_lophoc){
                $query->select(DB::raw('LopHoc_LichHoc.id_lichhoc'))
                        ->from('LopHoc_LichHoc')
                        ->where('LopHoc_LichHoc.id_lophoc',$id_lophoc) ;
            })
            ->select('LichHoc.*')
            ->get() ;
             
            $ngay_hoc = '' ;
            $tiet_hoc = '' ;
            $giang_duong = '' ;
            foreach($lichhoc as $data){
                $ngay_hoc = $ngay_hoc. ',' .$data->ngay_hoc ;
                $tiet_hoc = $tiet_hoc. ',' .$data->tiet_hoc ;
                $giang_duong = $giang_duong. ',' .$data->giang_duong ;
            }
            $ngay_hoc = substr($ngay_hoc,1) ;
            $tiet_hoc = substr($tiet_hoc,1) ;
            $giang_duong = substr($giang_duong,1) ;
            $value->ngay_hoc = $ngay_hoc ;
            $value->tiet_hoc = $tiet_hoc ;
            $value->giang_duong = $giang_duong ;
        }
       
        $year_now = date('Y') ;
        $namhoc = DB::table('NamHoc')
                ->orderBy('nam','DESC')
               // ->orderBy('hoc_ky1','DESC')
                ->get() ;
        $namhoc2 = DB::table('NamHoc')
                ->orderBy('nam','DESC')
               // ->orderBy('hoc_ky1','DESC')
                ->get() ;
       
        foreach($namhoc as $key => $value) {
            $arr_nam = explode('-',$value->nam) ;
            for($i = 0 ; $i<count($arr_nam) ; $i++){
                $arr_nam[$i] = intval($arr_nam[$i]) ; 
            }
            if($arr_nam[0] < $year_now && $arr_nam[1] < $year_now)
                unset($namhoc[$key]) ;
        }
        //var_dump($namhoc2) ; die ;
    	return view('admin.lophoc.index')->with([
                                    'lophoc'=>$lophoc,
                                    'namhoc'=>$namhoc,
                                    'namhoc2'=>$namhoc2,
        ]) ;
    }
    public function create() 
    {
        $year_now = date('Y') ;
      
        $lop = MyFunction::getLop() ;
    	$monhoc_list = MonHoc::all() ;
    
        $namhoc_list = NamHoc::orderBy('nam')
                ->orderBy('hoc_ky')
                ->get() ;
        foreach ($namhoc_list as $key => $value){
            $arr_nam = explode('-', $value->nam) ;
            for($i = 0 ; $i< count($arr_nam) ; $i++){
                $arr_nam[$i] = intval($arr_nam[$i]) ;
            }
          
            if($arr_nam[0] < $year_now && $arr_nam[1] < $year_now)
                unset($namhoc_list[$key]) ;
        }
    

    	$monhoc = array() ;
    	$namhoc = array() ;
    	$lichhoc = array() ;
    	foreach ($namhoc_list as $value) {
    		$namhoc[$value->id] = $value->hoc_ky.' Năm học '.$value->nam ;
    	}
    
    	

    	$first_arrNamhoc = array_keys($namhoc)[0] ;
    	$first_arrMonhoc = $monhoc_list[0]->ma_mh ;
    	$lop_exists = DB::table('LopHoc')->where(array(['ma_mh','=',$first_arrMonhoc]
    		,['nam_hoc','=',$first_arrNamhoc]))->select('lop')->get();
    	foreach($lop_exists as $key => $value){
    		unset($lop[$value->lop]) ;
    	}
    	$lichhoc_list = DB::table('LichHoc')
    		->whereNotIn('id',function($query) use($first_arrNamhoc){
    				$query->select(DB::raw('id_lichhoc'))
    						->from('LopHoc_LichHoc')
    						->join('LopHoc','LopHoc_LichHoc.id_LopHoc','LopHoc.id')
    						->where('LopHoc.nam_hoc',$first_arrNamhoc) ;
    						
    	})->orderBy('ngay_hoc')->orderBy('tiet_hoc')->get() ;

    	foreach($lichhoc_list as $value){
    		$lichhoc[$value->id] = $value->ngay_hoc.','.$value->tiet_hoc .','.$value->giang_duong;
    	}
    	return view('admin.lophoc.create')->with([
                                            'lop'=>$lop,
                                            'monhoc'=>$monhoc_list,
                                            'namhoc'=>$namhoc,
                                            'lichhoc'=>$lichhoc
                                        ]) ;
    }
    public function store(Request $request){
    	$rules = [	'soluong' => 'required|digits_between:2,3',
    				'lichhoc'	=>'required',
    	];
    	$messages = [	'soluong.required' => 'Bạn chưa điền số lượng mở lớp',

    					//'soluong.integer' =>	'Số lượng mở lớp là 1 số nguyên',
    					'soluong.digits_between'=>'Số lượng mở lớp là 1 số nguyên có 2 đến 3 chữ số',
    					'lichhoc.required'	=>'Bạn chưa chọn lịch học'
    	];
    	$this->validate($request,$rules,$messages) ;
    	 // insert into table LopHoc
    	$lophoc = new LopHoc() ;
    	$lophoc->ma_mh = $request->ma_mh ;
    	$lophoc->lop = $request->lop ;
    	$lophoc->so_luong = $request->soluong ;
    	$lophoc->nam_hoc = $request->namhoc ;
    	$lophoc->save() ;

         // insert into table LopHoc_LichHoc 
    	$id_lophoc = $lophoc->id ;
    	$data = array() ;
    	$lichhoc = $request->lichhoc ;
    	foreach($lichhoc as $value){
    		$lophoc_lichhoc = [	'id_lichhoc' => $value,
    							'id_lophoc'	=>	$id_lophoc
    		];
    		array_push($data,$lophoc_lichhoc) ;

    	}
         
    	DB::table('LopHoc_LichHoc')->insert($data) ;
        return redirect('admin/lophoc/create')->with(['thongbao'=>'Thêm thành công']) ;
    }
    public function edit($id)
    {
      //  $lop = Lang::get('general.lop') ;
        $lop = MyFunction::getLop() ;
        $lophoc = LopHoc::where('id',$id)->first() ;
       // var_dump($lophoc) ;die ;
        $lophoc_lichhoc = DB::table('LopHoc_LichHoc')->where('id_lophoc',$id)->get() ;
        $id_lichhoc = [];
        foreach($lophoc_lichhoc as $key => $value){
            array_push($id_lichhoc,$value->id_lichhoc) ;
        }
      
        $monhoc_list = MonHoc::all() ;
        $namhoc_list = NamHoc::orderBy('nam')->orderBy('hoc_ky')->get() ;
       
        $monhoc = array() ;
        $namhoc = array() ;
        $lichhoc = array() ;
        foreach ($namhoc_list as $value) {
            $namhoc[$value->id] = $value->hoc_ky.' Năm học '.$value->nam ;
        }
   
        $first_arrNamhoc = array_keys($namhoc)[0] ;
        $first_arrMonhoc = $monhoc_list[0]->ma_mh ;
        $lop_exists = DB::table('LopHoc')->where(array(
                                            ['ma_mh','=',$lophoc->ma_mh],
                                            ['nam_hoc','=',$lophoc->nam_hoc],
                                            ['id','!=',$id],
                                        ))->select('lop')->get();
        foreach($lop_exists as $key => $value){
            unset($lop[$value->lop]) ;
        }

        $lichhoc_list = DB::table('LichHoc')
    		->whereNotIn('id',function($query) use($first_arrNamhoc,$id){
    				$query->select(DB::raw('id_lichhoc'))
    						->from('LopHoc_LichHoc')
    						->join('LopHoc','LopHoc_LichHoc.id_LopHoc','LopHoc.id')
    						->where([['LopHoc.nam_hoc','=',$first_arrNamhoc],
    									['LopHoc.id','!=',$id]
    								]) ;
    						
    	})->orderBy('ngay_hoc')->orderBy('tiet_hoc')->get() ;

    	foreach($lichhoc_list as $value){
    		$lichhoc[$value->id] = $value->ngay_hoc.','.$value->tiet_hoc .','.$value->giang_duong;
    	}
        return view('admin.lophoc.edit')->with(['lop'=>$lop,
                                        'monhoc'=>$monhoc_list,
                                        'namhoc'=>$namhoc,
                                        'lichhoc'=>$lichhoc,
                                        'lophoc'=>$lophoc,
                                        'id_lichhoc'=>$id_lichhoc,
                                    ]) ;
    }
    public function update(Request $request,$id){
        $rules = [  'soluong' => 'required|digits_between:2,3',
                    'lichhoc'   =>'required',
        ];
        $messages = [   'soluong.required' => 'Bạn chưa điền số lượng mở lớp',

                        //'soluong.integer' =>  'Số lượng mở lớp là 1 số nguyên',
                        'soluong.digits_between'=>'Số lượng mở lớp là 1 số nguyên có 2 đến 3 chữ số',
                        'lichhoc.required'  =>'Bạn chưa chọn lịch học'
        ];
        $this->validate($request,$rules,$messages) ;
         // update  table LopHoc
        $data1['ma_mh'] = $request->ma_mh ;
        $data1['lop'] = $request->lop ;
        $data1['so_luong'] = $request->soluong ;
        $data1['nam_hoc'] = $request->namhoc ;
        LopHoc::where('id',$id)->update($data1) ;

        // insert into table LopHoc_LichHoc 
        $id_lophoc = $id ;
        $data2 = array() ;
        $lichhoc = $request->lichhoc ;
        foreach($lichhoc as $value){
            $lophoc_lichhoc = [ 'id_lichhoc' => $value,
                                'id_lophoc' =>  $id_lophoc
            ];
            array_push($data2,$lophoc_lichhoc) ;

        }
        DB::table('LopHoc_LichHoc')->where('id_lophoc',$id_lophoc)->delete() ;
        DB::table('LopHoc_LichHoc')->insert($data2) ;
        return redirect('admin/lophoc/edit/'.$id)->with(['thongbao'=>'Sửa thành công ']) ;
    }
    public function getModal($id)
    {
        $model = 'lophoc' ;
        $confirm_route = $error = null ;
        try{
            $confirm_route = route('lophoc_delete',['id'=>$id]) ;
            return view('admin.layout.modal_confirmation')->with([
                        'model'=>$model,
                        'error'=>$error,
                        'confirm_route'=>$confirm_route
            ]);

        }catch(Exception $e){
            $error = " lỗi rồi" ;
            return view('admin.layout.modal_confirmation')->with([
                        'model'=>$model,
                        'error'=>$error,
                        'confirm_route'=>$confirm_route
            ]);

        }

    }
    public function delete($id)
    {
        try{
            
            DB::table('LopHoc_LichHoc')->where('id_lophoc',$id)->delete() ;
            LopHoc::where('id',$id)->delete() ;
            return redirect('lophoc')->with(['thongbao'=>'Xoa thanh cong']) ;
        }catch(Exception $e){
             return redirect('admin/lophoc')->with(['thongbao'=>'khong xoa duoc']) ;
        }
    }
    public function dongdkHoc(){
        $data['trang_thai'] = 0;
        LopHoc::where('trang_thai',1)->update($data) ;
        return Redirect::back() ;
    }
    public function modkHoc(Request $request){
       if(isset($request->id_nam) && !empty($request->id_nam )){
            $lophoc = LopHoc::all() ;
            $arr['trang_thai'] = 0 ;
           // DB::table('LopHoc')->update($arr) ;
            
            foreach($lophoc as $key => $value){
                $value->update($arr) ;
            }
           $data['trang_thai'] = 1;
           LopHoc::where('nam_hoc',$request->id_nam)->update($data) ;
       }
    }
    public function searchLopHocByNamHoc(Request $request){
        $lophoc = LopHoc::join('namhoc','nam_hoc','namhoc.id')
        ->select('LopHoc.*',
            'namhoc.nam','namhoc.hoc_ky')
        ->orderBy('namhoc.nam','DESC')
        ->orderBy('namhoc.hoc_ky','DESC')->get() ;
        if(isset($request->nam_hoc) && !empty($request->nam_hoc)){
            $nam_hoc = $request->nam_hoc ;
            $lophoc = LopHoc::join('namhoc','nam_hoc','namhoc.id')
            ->select('LopHoc.*',
                'namhoc.nam','namhoc.hoc_ky')
            ->where('LopHoc.nam_hoc',$request->nam_hoc)
            ->orderBy('namhoc.nam','DESC')
            ->orderBy('namhoc.hoc_ky','DESC')->get() ;
        }
          
        foreach($lophoc as $key =>$lh){

            $id_lophoc = $lh->id ;
            $lichhoc = LichHoc::whereIn('LichHoc.id',function($query) use($id_lophoc){
                $query->select(DB::raw('LopHoc_LichHoc.id_lichhoc'))
                        ->from('LopHoc_LichHoc')
                        ->where('LopHoc_LichHoc.id_lophoc',$id_lophoc) ;
            })
            ->select('LichHoc.*')
            ->get() ;
             
            $ngay_hoc = '' ;
            $tiet_hoc = '' ;
            $giang_duong = '' ;
            foreach($lichhoc as $data){
                $ngay_hoc = $ngay_hoc. ',' .$data->ngay_hoc ;
                $tiet_hoc = $tiet_hoc. ',' .$data->tiet_hoc ;
                $giang_duong = $giang_duong. ',' .$data->giang_duong ;
            }
            $ngay_hoc = substr($ngay_hoc,1) ;
            $tiet_hoc = substr($tiet_hoc,1) ;
            $giang_duong = substr($giang_duong,1) ;
            $lh->ngay_hoc = $ngay_hoc ;
            $lh->tiet_hoc = $tiet_hoc ;
            $lh->giang_duong = $giang_duong ;


            $lh->ten_mh = $lh->getMonHoc->ten_mh ;
            $lh->so_tinchi = $lh->getMonHoc->so_tinchi ;
            $lh->option =  '<a href="'.route('lophoc_confirmDelete',$lh->id).'" title="Xóa" 
                            data-toggle="modal" data-target="#confirm_delete"> 
                                <i class="fa fa-trash-o  fa-fw"></i>
                            </a>
                            <a title="Sửa" href="'.route('lophoc_edit_get',$lh->id).'"> <i class="fa fa-pencil fa-fw"></i> </a>
                            <a title="Xem danh dách sinh viên đã đăng ký học" href="'.route('lophoc_ds_sinhvienDK',$lh->id).'"> 
                                        <i class="fa fa-delicious fa-fw"> 
                                        </i>
                                    </a>
                            <a title="Xuất danh dách sinh viên đã đăng ký học" href="'.route('lophoc_export_dsSinhVien',$lh->id).'"> 
                                <i class="fa fa fa-file-pdf-o fa-fw">
                                    
                                </i> 
                            </a>' ;
        }
        return $lophoc ;
    }
    public function ds_sinhvienDK($id){
        $lophoc = LopHoc::join('NamHoc','LopHoc.nam_hoc','NamHoc.id')
                ->where('LopHoc.id',$id)
                ->select('LopHoc.*','NamHoc.hoc_ky','NamHoc.nam')
                ->first() ;
        $sinhvien = SinhVien::join('DsSinhVienDK','SinhVien.masv','DsSinhVienDK.ma_sv')
                    ->where('DsSinhVienDK.id_lophoc',$id)->get();
        return view('admin.lophoc.ds_sinhvienDK')->with([
                        'sinhvien'=>$sinhvien,
                        'lophoc' => $lophoc,
        ]) ;
    }
}
