<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth ;
use Illuminate\Http\Request;
use App\SinhVien ;
use App\User ;
use Redirect ;
use DB ;
use Exception ;
class UsersController extends Controller
{
    //
    public function index()
    {
        $users = User::all() ;
    	return view('admin.users.index')->with(['users'=>$users]) ;
    }
    public function create()

    {
    	//$sinhvien = SinhVien::all() ;
    	return view('admin.users.create') ;
    	//->with(['sinhvien'=>$sinhvien]) ;
    }
    public function store(Request $request)
    {
    	
        $rules = [	'name'=>'required|min:3',
        			'username' => 'required|unique:users,username|min:3',
	                
	                'password'=>'required|min:6|max:32',
	                'passwordAgain'=>'required|same:password',
	                'email'=>'required|email|unique:users,email',
	                
            	];

        $messages =  [	'name.required'=>'Bạn chưa nhập tên người dùng',
		                'name.min'=>'tên người dùng có ít nhất 3 ký tự',
		                 'username.required'=>'Bạn chưa nhập tên đăng nhập',
		                'username.min'=>'tên người dùng có ít nhất 3 ký tự',
		                'username.unique'=>'Tên đăng nhập đã tồn tại',
		               
		                'password.required'=>'Bạn chưa nhập mật khẩu',
		                'password.min'=>'mật khẩu phải có ít nhất 6 ký tự',
		                'password.max'=>'mật khẩu phải có nhiều nhất 32 ký tự',
		                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
		                'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp',
		                'email.required'=>'Bạn chưa nhập địa chỉ email',
		                'email.email'=>'Tên email không đúng định dạng',
		                'email.unique'=>'Địa chỉ email đã tồn tại',
		               
	            	];
	    $this->validate($request,$rules,$messages) ;
        $user = new User() ;
        $user->name = $request->name ;
        $user->username = $request->username ;
        $user->email = $request->email ;
        $user->level = 1;
        //$request->quyen ;
        $user->password = bcrypt($request->password) ;
        $user->save() ;
        return Redirect::route('user_create_get')->with(['thongbao'=>'Thêm thành công']) ;
    }
    public function createSinhVien(){
    	$sinhvien = SinhVien::whereNotIn('masv',function($query){
    				$query->select(DB::raw('username'))->from('Users') ;

    	})->get() ;
       
    	return view('admin.users.create_sinhvien')->with(['sinhvien'=>$sinhvien]) ;
    }
    public function storeSinhVien(Request $request){
    	//echo $request->name ;
    	try{
    		$sinhvien = SinhVien::where('masv',$request->username)->first() ;
	    	$user = new User() ;
	        $user->name = $sinhvien->hoten ;
	        $user->username = $sinhvien->masv ;
	        $user->email = $sinhvien->email ;
	        $user->level = 0;
	        //$request->quyen ;
	        $user->password = bcrypt($sinhvien->masv) ;
	        $user->save() ;
	        return Redirect::route('user_create_sv_get')->with(['thongbao'=>'Thêm thành công']) ;
    	}catch(Exception $e){
    		return Redirect::route('user_create_sv_get')->with(['thongbao'=>'Không thêm được do tài khoản có thể bị trùng']) ;
    	}
    	
    }
    public function getLoginAdmin()
    {
        if(Auth::check()){
            if(Auth::user()->level == 1){
                return Redirect::route('lophoc_list') ;
            }else{
                return view('admin.dangnhap.login') ;
            }
        }else{
            return view('admin.dangnhap.login') ;
        }
    }
    public function postLoginAdmin(Request $request)
    {
        $rules = [  
                    'username' => 'required',
                    
                    'password'=>'required|min:6|max:32',
                ];

        $messages =  [  
                        'username.required'=>'Bạn chưa nhập tên đăng nhập',
                        'password.required'=>'Bạn chưa nhập mật khẩu',
                        'password.min'=>'mật khẩu phải có ít nhất 6 ký tự',
                        'password.max'=>'mật khẩu phải có nhiều nhất 32 ký tự',
                       
                       
                    ];
                    //|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/
        $this->validate($request,$rules,$messages) ;
        $remember_me = ($request->remember_me == 'on') ? true : false ;
        if(Auth::attempt(
                        ['username'=>$request->username,
                        'password'=>$request->password,
                        'level'=>1],
                        $remember_me))
        {
            
            return Redirect::route('lophoc_list') ;
        }else{
            return Redirect::route('login_admin_get')->with(['thongbao'=>'Đăng nhập thất bại']) ;
        }
    }
    public function logoutAdmin(){
        Auth::logout() ;
        return Redirect::route('login_admin_get') ;
    }
    public function createAllUserStudent(){
        try{
            $sinhvien = SinhVien::whereNotIn('masv',function($query){
            $query->select(DB::raw('username'))
                    ->from('users') ;
            })->get() ;
            foreach($sinhvien as $sv){
                $user = new User() ;
                $user->username = $sv->masv ;
                $user->name = $sv->hoten ;
                $user->email = $sv->email ;
                $user->password = bcrypt($sv->masv) ;
                $user->save() ;
            }
            return Redirect::back()->with('thongbao','Thêm thành công') ;
        }catch(Exception $e){
            return Redirect::back()->with('thongbao','Lỗi rồi không thêm được') ;
        }
        
    }
    public function edit($id){
        $user = User::find($id) ;
        return view('admin.users.edit')->with(['user'=>$user]) ;
    }
    public function update(Request $request,$id){
        // 'password'=>'required|min:6|max:32',
        //             'passwordAgain'=>'required|same:password',
         // 'password.required'=>'Bạn chưa nhập mật khẩu',
         //                'password.min'=>'mật khẩu phải có ít nhất 6 ký tự',
         //                'password.max'=>'mật khẩu phải có nhiều nhất 32 ký tự',
         //                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
         //                'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp',
        $rules = [  'name'=>'required|min:3',
                    'username' => 'required|min:3',
                    'email'=>'required|email|',
                ];

        $messages =  [  'name.required'=>'Bạn chưa nhập tên người dùng',
                        'name.min'=>'tên người dùng có ít nhất 3 ký tự',
                        'username.required'=>'Bạn chưa nhập tên đăng nhập',
                        'username.min'=>'tên người dùng có ít nhất 3 ký tự',
                        'username.unique'=>'Tên đăng nhập đã tồn tại',
                        'email.required'=>'Bạn chưa nhập địa chỉ email',
                        'email.email'=>'Tên email không đúng định dạng',
                        'email.unique'=>'Địa chỉ email đã tồn tại',
                       
                    ];
        $user = User::find($id) ;
        if($user->username != $request->username){
            unset($rules['username']) ;
            $rules['username'] = 'required|unique:users,username|min:3' ;
        }
        if($user->email != $request->email){
            unset($rules['email']) ;
            $rules['mail'] =  'required|email|unique:users,email' ;
        }
        if($request->changePassword == "on"){
            $rules['password'] = 'required|min:6|max:32' ;
            $rules['passwordAgain'] = 'required|same:password' ;
            $messages['password.required'] = 'Bạn chưa nhập mật khẩu' ;
            $messages['password.min'] = 'mật khẩu phải có ít nhất 6 ký tự' ;
            $messages['password.max'] = 'mật khẩu phải có nhiều nhất 32 ký tự' ;
            $messages['passwordAgain.required'] = 'Bạn chưa nhập lại mật khẩu';
            $messages['passwordAgain.same'] = 'Mật khẩu nhập lại chưa khớp' ;
        }
        $this->validate($request,$rules,$messages) ;
       // $data['username'] = 
        $user->name = $request->name ;
        $user->username = $request->username ;
        $user->email = $request->email ;
        if($request->changePassword == "on"){
            $user->password = bcrypt($request->password) ;
        }
        $user->save() ;
        return Redirect::route('user_list')->with(['thongbao'=>'Sửa thành công']) ;
    }
    public function getModal($id){
        $model = 'users' ;
        $error = $confirm_route = null ;
        try{
            $confirm_route = route('user_delete',$id) ;
            return view('admin/layout/modal_confirmation',
                ['model'=>$model,'error'=>$error,'confirm_route'=>$confirm_route]) ;

        }catch(Exception $e){
            $error = "Lỗi rồi" ;
            return view('admin/layout/modal_confirmation',
                ['model'=>$model,'error'=>$error,'confirm_route'=>$confirm_route]) ;
        }
    }
    public function delete($id){
        try{
            User::where('id',$id)->delete() ;
            return Redirect::route('user_list')->with(['thongbao'=>'Xóa thành công']) ;
        }catch(Exception $e){
            return Redirect::route('user_list')->with(['thongbao'=>'Không xóa được']) ;
        }
    }
}
