<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MonHoc ;
use App\ChuyenNganh ;
use Lang ;
use Exception ;
use DB ;
use  Validator ;
class MonHocController extends Controller
{
    //
    public function index()
    {
        $monhoc = MonHoc::all() ;
    	return view('admin.monhoc.index')->with(['monhoc'=>$monhoc]) ;

    }
    public function create(){
        $chuyennganh = ChuyenNganh::where('ma_nganh','!=',1)->get() ;
        $data = Lang::get('general.nganh') ;
      //  var_dump($data) ; die;
        foreach ($chuyennganh as $key => $value) {
            $data[$value->ma_nganh] = $value->ten_nganh ;
        }
    	return view('admin.monhoc.create')->with(['chuyennganh' => $data]) ;
    }
    public function store(Request $request)
    {
         //'ma_mh' =>'required|unique:MonHoc,ma_mh|min:3',
                   // 
        $rules = [  'ma_mh' =>'required|unique:MonHoc,ma_mh|min:3',
                    'ten_mh' =>'required|min:3',
                    'sotinchi' => 'digits: 1',
                    'lephi' => 'nullable|numeric',
                    'chuyennganh' =>'required'
        ];
        $messages = [   'ma_mh.required'   => 'Bạn chưa điền mã môn học',
                        'ma_mh.unique'    => 'Mã môn học bị trùng',
                        'ma_mh.min'       => 'Mã môn học có ít nhất 3 ký tự',
                        'ten_mh.required' => 'Bạn chưa điền tên môn học',
                        'ten_mh.min'      => 'Tên môn học phải có ít nhất 3 ký tự',
                        'sotinchi.digits' =>'Số tín chỉ là một số nguyên có 1 chữ số',
                        'lephi.numeric'    => 'Lệ phí phải là một số thực',  
                        'chuyennganh.required'=>'Bạn chưa chọn ngành học'   

        ];
        $this->validate($request,$rules,$messages) ;
        $arr_monhoc = $request->only(['ma_mh','ten_mh','so_tinchi','hoc_phi']) ;
       // var_dump($test) ; die ;
      // // 
        $monhoc = new MonHoc($arr_monhoc) ;
      //   $monhoc->ma_mh = $request->ma_mh ;
      //   $monhoc->ten_mh = $request->ten_mh ;
      //   $monhoc->so_tinchi = $request->sotinchi ;
      //   $monhoc->hoc_phi = $request->lephi ;
      //   //$monhoc->chuyen_nganh = $request->chuyennganh ;
        $monhoc->save() ;
        //MonHoc::insert($arr_monhoc) ;
        $nganhhoc = $request->chuyennganh ;
        $data = [] ;
        foreach($nganhhoc as $value){
            $nganhhoc_monhoc['ma_mh'] = $request->ma_mh ;
            $nganhhoc_monhoc['ma_nganh'] = $value ;
            array_push($data, $nganhhoc_monhoc) ;
        }
        DB::table('NganhHoc_MonHoc')->insert($data) ;
        return redirect('admin/monhoc/create')->with(['thongbao'=>'Thêm thành công']) ;
    }
    public function edit($ma_mh){
        $monhoc = MonHoc::where('ma_mh',$ma_mh)->firstOrFail() ;
        $chuyennganh = ChuyenNganh::where('ma_nganh','!=',1)->get() ;
        $data = Lang::get('general.nganh') ;
        foreach ($chuyennganh as $key => $value) {
            $data[$value->ma_nganh] = $value->ten_nganh ;
        }
        $nganhhoc_monhoc = DB::table('NganhHoc_MonHoc')->where('ma_mh',$ma_mh)->get() ;
        $data_ma_nganh = array() ;
        foreach($nganhhoc_monhoc as $key => $value ){
            array_push($data_ma_nganh,$value->ma_nganh) ;
        }
    	return view('admin.monhoc.edit')->with([
                                        'monhoc'=>$monhoc,'chuyennganh'=>$data,
                                        'data_ma_nganh'=>$data_ma_nganh
        ]) ;
    }
    public function update(Request $request,$ma_mh)
    {

        $rules = [  'ma_mh' =>'required|min:3',
                    'ten_mh' =>'required|min:3',
                    'sotinchi' => 'digits: 1',
                    'lephi' => 'nullable|numeric',
                    'chuyennganh' =>'required',
        ];
        $messages = [   'ma_mh.required'   => 'Bạn chưa điền mã môn học',
                        'ma_mh.min'       => 'Mã môn học có ít nhất 3 ký tự',
                        'ten_mh.required' => 'Bạn chưa điền tên môn học',
                        'ten_mh.min'      => 'Tên môn học phải có ít nhất 3 ký tự',
                        'sotinchi.digits' =>'Số tín chỉ là một số nguyên có 1 chữ số',
                        'lephi.numeric'    => 'Học phí phải là một số thực', 
                        'chuyennganh.required'=>'Bạn chưa chọn ngành học'     
        ];
        if(isset($request->ma_mh) && ($request->ma_mh != $ma_mh)){
            $rules['ma_mh'] = $rules['ma_mh'].'|unique:MonHoc,ma_mh' ;
            $messages['ma_mh.unique'] = 'Mã môn học bị trùng' ;
        }
        $this->validate($request,$rules,$messages) ;
        
        $data['ma_mh'] = $request->ma_mh ;
        $data['ten_mh'] = $request->ten_mh ;
        $data['so_tinchi'] = $request->sotinchi ;
        $data['hoc_phi'] = $request->lephi ;
        //$data['chuyen_nganh'] = $request->chuyennganh ;

        MonHoc::where('ma_mh',$ma_mh)->update($data) ;
        $nganhhoc = $request->chuyennganh ;
        $data = [] ;
        foreach($nganhhoc as $value){
            $nganhhoc_monhoc['ma_mh'] = $request->ma_mh ;
            $nganhhoc_monhoc['ma_nganh'] = $value ;
            array_push($data, $nganhhoc_monhoc) ;
        }
        DB::table('NganhHoc_MonHoc')->where('ma_mh',$ma_mh)->delete() ;
        DB::table('NganhHoc_MonHoc')->insert($data) ;
        return redirect('admin/monhoc/edit/'.$request->ma_mh)->with(['thongbao'=>'Sửa thành công']) ;
    }
    public function getModal($ma_mh)
    {
        $model = 'monhoc' ;
        $confirm_route = $error = null ;
        try{
            $confirm_route = route('monhoc_delete',['ma_mh' =>$ma_mh]) ;
            return view('admin/layout/modal_confirmation',
                ['model'=>$model,'error'=>$error,'confirm_route'=>$confirm_route]) ;

        }catch(Exception $e){
            $error = "Lỗi rồi " ;
            return view('admin/layout/modal_confirmation',
                ['model'=>$model,'error'=>$error,'confirm_route'=>$confirm_route]) ;
        }
    }
    public function delete($ma_mh){
        try{
            
            DB::table('NganhHoc_MonHoc')->where('ma_mh',$ma_mh)->delete() ;
            MonHoc::where('ma_mh',$ma_mh)->delete() ;
            return redirect('admin/monhoc')->with(['thongbao'=>'Xóa thành công ']) ;

        }catch(Exception $e){
            return redirect('admin/monhoc')->with(['thongbao'=>'Không xóa được']) ;
        }
    }
    public function updateHocPhi(Request $request){

        // $this->Validate($request,
        //         [ 'hoc_phi' =>'required|numeric'
        //         ],
        //         [
        //             'hoc_phi.required' => 'Bạn chưa nhập học phí',
        //             'hoc_phi.numeric'  => 'Học phí phải là một số'
        //         ]
        // ) ;
        // $valida =  Validator::make($request->only('hoc_phi'),
        //             [ 'hoc_phi' =>'required|numeric'
        //             ],
        //             [
        //                 'hoc_phi.required' => 'Bạn chưa nhập học phí',
        //                 'hoc_phi.numeric'  => 'Học phí phải là một số'
        //             ]
        //     ) ;
        // if($valida->fails()){
        //     var_dump($valida)  ;
        // }
        if(!is_numeric($request->hoc_phi)){
            return "Học phí phải là một số thực. Vui lòng nhập lại" ;
        }else{
            $hoc_phi = $request->hoc_phi ;
            $monhoc = MonHoc::all() ;
            foreach ($monhoc as $value) {
                $data['hoc_phi'] = $hoc_phi * $value->so_tinchi ;
                $ma_mh  = $value->ma_mh ;
                $value->where('ma_mh',$ma_mh)->update($data) ;
            }
            return "bạn đã cập nhập học phí thành công" ;
        }
       
    }
}
