<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB ;
use App\ChuyenNganh ;
use Exception ;
use App\Http\Requests\NganhHocRequest;
class NganhHocController extends Controller
{
    //
    public function index()

    {
        $chuyennganh = ChuyenNganh::where('ma_nganh','!=',1)->get() ;
    	//return view('admin.chuyennganh.index')->with(['chuyennganh'=>$chuyennganh]) ;
       
        return view('admin.chuyennganh.index') ;
    }
    public function data()

    {
        $chuyennganh = ChuyenNganh::where('ma_nganh','!=',1)->get() ;
        foreach($chuyennganh as $value){
            $actions = '&nbsp;&nbsp;<a href="' .route('chuyennganh_confirmDelete',$value->ma_nganh).'" title="xóa" data-toggle="modal" data-target="#confirm_delete">
                                        <i class="fa fa-trash-o  fa-fw"></i>
                                    </a>
                <a href="'.route('chuyennganh_edit_get',$value->ma_nganh).'" title="sửa"><i class="fa fa-pencil fa-fw"></i> </a>' ;
            $value->actions = $actions ;
        }
        return $chuyennganh ;
        //return response()->json($chuyennganh) ;
    }
    public function create(){
    	return view('admin.chuyennganh.create') ;
    }
    public function store(NganhHocRequest $request)
    {
    	// $this->validate($request,
	    // 				[	'ma_nganh' =>'required|unique:ChuyenNganh,ma_nganh|min:3|max:20',
	    // 					'ten_nganh'=>'required|min:3|max:50'
	    // 				],
	    // 				[
	    // 					'ma_nganh.required'=>'Bạn chưa điền mã ngành',
	    // 					'ma_nganh.unique'=>'Mã ngành đã tồn tại',
	    // 					'ma_nganh.min'=>'Mã ngành có ít nhất 3 ký tự',
	    // 					'ma_nganh.max'=>'Mã ngành có nhiều nhất 20 ký tự',
	    // 					'ten_nganh.required'=>'Bạn chưa điền tên ngành',
	    // 					'ten_nganh.min'=>'Tên ngành có ít nhất 3 ký tự',
	    // 					'ten_nganh.max'=>'Tên ngành có nhiều nhất 50 ký tự',
	    // 				]
	    // 				);
    	// $data = [	'ma_nganh' => $request->ma_nganh,
    	// 			'ten_nganh'=> $request->ten_nganh
    	// 		];
    	//DB::table('chuyennganh')->insert($data) ;
        
        $chuyennganh =  new ChuyenNganh() ;
        $chuyennganh->ma_nganh = $request->ma_nganh ;
        $chuyennganh->ten_nganh = $request->ten_nganh ;
        $chuyennganh->save() ;
    	return redirect('admin/chuyennganh/create')->with(['thongbao'=>'Thêm thành công']) ;
    }
    public function edit($ma_nganh){
    	//$ma_nganh = $request->ma_nganh ;
    	$nganhhoc = DB::table('chuyennganh')->where('ma_nganh',$ma_nganh)->first() ;
    	//var_dump($nganhhoc) ; die ;
    	return view('admin.chuyennganh.edit')->with(['nganhhoc'=>$nganhhoc]) ;
    }
    public function update(Request $request,$ma_nganh)
    {
        //$chuyennganh = ChuyenNganh::where('ma_nganh','=',$ma_nganh)->firstOrFail() ;
        //  echo '<pre>' ;
        // var_dump($chuyennganh) ;
        //   echo '</pre>' ;
        //   die ;
        $rules = [  'ma_nganh' =>'required|min:3|max:20',
                    'ten_nganh'=>'required|min:3|max:50'
                ] ;
        $messages = [
                        'ma_nganh.required'=>'Bạn chưa điền mã ngành',
                        'ma_nganh.min'=>'Mã ngành có ít nhất 3 ký tự',
                        'ma_nganh.max'=>'Mã ngành có nhiều nhất 20 ký tự',
                        'ten_nganh.required'=>'Bạn chưa điền tên ngành',
                        'ten_nganh.min'=>'Tên ngành có ít nhất 3 ký tự',
                        'ten_nganh.max'=>'Tên ngành có nhiều nhất 50 ký tự',
                    ] ;
        if($request->ma_nganh != $ma_nganh)
        {
            $rules['ma_nganh'] = $rules['ma_nganh'].'|unique:ChuyenNganh,ma_nganh' ;
            $messages['ma_nganh.unique'] = 'Mã ngành đã tồn tại' ;
            
        }
        // echo '<pre>' ;
        // print_r($rules) ;
        // echo '</pre>' ;
        // die ;
    	$this->validate($request,$rules,$messages) ;
    	$data = [	'ma_nganh' => $request->ma_nganh,
    				'ten_nganh'=> $request->ten_nganh
    			];
    	//DB::table('chuyennganh')->insert($data) ;
        ChuyenNganh::where('ma_nganh','=',$request->ma_nganh)->update($data) ;
        // echo '<pre>' ;
        // print_r($a) ;
        // echo '</pre>' ;
        // die ;
    	return redirect('admin/chuyennganh/edit/'.$ma_nganh)->with(['thongbao'=>'Sửa thành công']) ;
    }
    public function getModal($ma_nganh)
    {
        $model = 'chuyennganh' ;
        $confirm_route = $error = null ;
        try{
            $confirm_route = route('chuyennganh_delete',['ma_nganh' => $ma_nganh]) ;
            return view('admin.layout.modal_confirmation',
                    ['error'=>$error,'model'=>$model,'confirm_route'=>$confirm_route]) ;
        }catch(Exception $e){
            $error = "Lỗi rồi" ;
            return view('admin.layout.modal_confirmation',
                    ['error'=>$error,'model'=>$model,'confirm_route'=>$confirm_route]) ;
        }
    }
    public function delete($ma_nganh)
    {
        try{
            ChuyenNganh::where('ma_nganh',$ma_nganh)->delete() ;
            return redirect('admin/chuyennganh')->with(['thongbao' =>'Xóa thành công']) ;
        }catch(Exception $e){
            return redirect('admin/chuyennganh')->with(['thongbao' =>'Không thể xóa']) ;
        }
    }
}
