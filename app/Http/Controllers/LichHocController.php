<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB ;
use App\LichHoc;
use Lang;
use Session ;
use Exception ;
use MyFunction ;
class LichHocController extends Controller
{
    //
    public function index(){
        $thu = Lang::get('general.thu') ;
        $lichhoc = LichHoc::all() ;

    	return view('admin.lichhoc.index')->with(['lichhoc'=>$lichhoc]) ;
    }
    public function create(){
        $thu = Lang::get('general.thu') ;
        
    	return view('admin.lichhoc.create')->with(['thu'=>$thu]) ;
    }
    public function store(Request $request){
        $rules = [  'tiethoc' => 'required',
                    'giangduong'=>'required'
        ];
        $messages = [   'tiethoc.required' =>'Bạn chưa điền tiết học',
                        'giangduong.required'=>'Bạn chưa điền phòng học'
        ];
        $this->validate($request,$rules,$messages) ;
        //echo "thành công rồi haha" ;

        $ngay_hoc = $request->thu;
        $tiet_hoc = $request->tiethoc;
        $giang_duong = $request->giangduong ;
        $lichhoc_exists = LichHoc::where(['ngay_hoc' => $ngay_hoc,
            'tiet_hoc'=>$tiet_hoc,'giang_duong'=>$giang_duong])->first() ;
        if(!empty($lichhoc_exists)){
            return redirect('admin/lichhoc/create')->with(['thongbao'=>'Lịch học đã tồn tại']) ;
        }
        $tiethoc_exists = LichHoc::where(['ngay_hoc' => $ngay_hoc,
            'giang_duong'=>$giang_duong])->get() ;
        $tiethoc_error = null ;
        $not_invalid = '' ;
        $pattern = '/^([1-9][-]([1-9]|10))|([1-9]|10)$/';
       // $subject = $request->tiethoc ;
        if (preg_match($pattern, $tiet_hoc) == 0 ){
            $not_invalid = 'Lỗi tiết học không hợp lệ' ;
        }else{
            $arr_tiethoc = explode('-', $tiet_hoc) ;
            if(count($arr_tiethoc) == 2){
                if($arr_tiethoc[0]>=$arr_tiethoc[1]) 
                    $not_invalid = 'Lỗi tiết học không hợp lệ' ;
            }
        }
        if(!empty($not_invalid)){
             return redirect('admin/lichhoc/create')->with(['thongbao'=>$not_invalid]) ;
        }
        if(!empty($tiethoc_exists)){
            //$list_tiethoc = DB::table('LichHoc')->get() ;
            foreach($tiethoc_exists as $key =>$value){
                $str1 = $request->tiethoc ;
                $str2 = $value->tiet_hoc ;
                $error_exists = MyFunction::compareString($str1,$str2) ;
                if(!empty($error_exists)){
                    $tiethoc_error = 'Lỗi trùng giờ học' ;
                    break ;
                }
            }
            
        }
        if(!empty($tiethoc_error)){
            return redirect('admin/lichhoc/create')->with(['thongbao'=>$tiethoc_error]) ;
        }
        
        $data = array(  'ngay_hoc'  =>  $request->thu,
                        'tiet_hoc'  =>  $request->tiethoc,
                        'giang_duong'   =>  $request->giangduong
        );
        $lichhoc = LichHoc::firstOrCreate($data) ;
        // echo '<pre>';
        // var_dump($lichhoc) ;
        // echo '</pre>';
        return redirect('admin/lichhoc/create')->with(['thongbao'=>'Thêm thành công']) ;
    }
    public function edit($id){
        $thu = Lang::get('general.thu') ;
        $lichhoc = LichHoc::find($id) ;
        //print_r($lichhoc) ; die ;
       // echo $id ; die ;
    	return view('admin.lichhoc.edit')->with(['lichhoc'=>$lichhoc,'thu'=>$thu]) ;
    }
    public function update(Request $request,$id){
       $rules = [  'tiethoc' => 'required',
                    'giangduong'=>'required'
        ];
        $messages = [   'tiethoc.required' =>'Bạn chưa điền tiết học',
                        'giangduong.required'=>'Bạn chưa điền phòng học'
        ];
        $this->validate($request,$rules,$messages) ;
        //echo "thành công rồi haha" ;

        $ngay_hoc = $request->thu;
        $tiet_hoc = $request->tiethoc;
        $giang_duong = $request->giangduong ;
        $lichhoc_exists = LichHoc::where(['ngay_hoc' => $ngay_hoc,
                        'tiet_hoc'=>$tiet_hoc,'giang_duong'=>$giang_duong,
                        ['id','!=',$id]
                        ])->first() ;
        if(!empty($lichhoc_exists)){
            return redirect('admin/lichhoc/edit/'.$id)->with(['thongbao'=>'Lịch học đã tồn tại']) ;
        }
        $tiethoc_exists = LichHoc::where(['ngay_hoc' => $ngay_hoc,
                        'giang_duong'=>$giang_duong,
                        ['id','!=',$id]
                        ])->get() ;
        $tiethoc_error = null ;
        $not_invalid = '' ;
        $pattern = '/^([1-9][-]([1-9]|10))|([1-9])$/';
       // $subject = $request->tiethoc ;
        if (preg_match($pattern, $tiet_hoc)==0){
             $not_invalid = 'Lỗi tiết học không hợp lệ' ;
        }else{
            $arr_tiethoc = explode('-', $tiet_hoc) ;
            if(count($arr_tiethoc) == 2){
                if($arr_tiethoc[0]>=$arr_tiethoc[1]) 
                    $not_invalid = 'Lỗi tiết học không hợp lệ' ;
            }
        }
        if(!empty($not_invalid)){
            return redirect('admin/lichhoc/edit/'.$id)->with(['thongbao'=>$not_invalid]) ;
        }
        if(!empty($tiethoc_exists)){
            foreach($tiethoc_exists as $key =>$value){
                $str1 = $request->tiethoc ;
                $str2 = $value->tiet_hoc ;
                $error_exists = MyFunction::compareString($str1,$str2) ;
                if(!empty($error_exists)){
                    $tiethoc_error = 'Lỗi trùng giờ học' ;
                    break ;
                }
            }
          
        }
        if(!empty($tiethoc_error)){
            return redirect('admin/lichhoc/edit/'.$id)->with(['thongbao'=>$tiethoc_error]) ;
        }
        
        $data = array(  'ngay_hoc'  =>  $request->thu,
                        'tiet_hoc'  =>  $request->tiethoc,
                        'giang_duong'   =>  $request->giangduong
        );
        LichHoc::where('id',$id)->update($data) ;
        
        return redirect('admin/lichhoc/edit/'.$id)->with(['thongbao'=>'Sửa thành công']) ;

    }
    public function getModal($id){
        $model = 'lichhoc' ;
        $confirm_route = $error = null ;
        try{
            $confirm_route = route('lichhoc_delete',$id) ;
            return view('admin/layout/modal_confirmation')->with(['model'=>$model,
                    'error'=>$error,'confirm_route'=>$confirm_route
                ]) ;
        }catch(Exception $e){
            $error = "lỗi rồi haha" ;
            return view('admin/layout/modal_confirmation')->with(['model'=>$model,
                    'error'=>$error,'confirm_route'=>$confirm_route
                ]) ;
        }
    }
    public function delete($id)
    {
        
        try{
            LichHoc::where('id',$id)->delete() ;
            return  redirect('admin/lichhoc')->with(['thongbao'=>'Xóa thành công']) ;
        }catch(Exception $e){
            return  redirect('admin/lichhoc')->with(['thongbao'=>'Không xóa được ']) ;
        }
    }
}
