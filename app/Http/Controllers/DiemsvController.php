<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SinhVien ;
use App\MonHoc ;
use App\NamHoc ;
use Lang ;
use DB ;
use Redirect ;
use Exception ;
use URL;
use View;
use Datatables;
use Validator;
class DiemsvController extends Controller
{
    //
    public function index()
    {
    	$diemsv = DB::table('Diem')->join('sinhvien','Diem.ma_sv','sinhvien.masv')
    							->join('MonHoc','Diem.ma_mh','MonHoc.ma_mh')
    							->join('NamHoc','Diem.nam_hoc','NamHoc.id')
    							->select('Diem.*','sinhvien.hoten as ten_sv','MonHoc.ten_mh',
    										'NamHoc.hoc_ky','NamHoc.nam')
    							->orderBy('Diem.ma_sv')
    							->orderBy('NamHoc.nam','DESC')
    							->orderBy('NamHoc.hoc_ky','DESC')
    							->get() ;
    	// echo '<pre>' ;

    	// print_r($diemsv) ;
    	// echo '</pre>' ;
    	// die ;

    	return view('admin.diemsinhvien.index')->with(['diemsv'=>$diemsv]) ;
    }
    public function create(){
    	$namhoc_list = NamHoc::orderBy('nam','DESC')->orderBy('hoc_ky','DESC')->get() ;
    	$namhoc = array() ;
    	foreach($namhoc_list as $key =>$value){
    		$namhoc[$value->id] = $value->hoc_ky.' Năm học '.$value->nam ;
    	}
    	$monhoc_list = MonHoc::all() ;
    	$sinhvien_list = SinhVien::all() ;
    	$diem_hechu = Lang::get('general.diem_hechu') ;
    	return view('admin.diemsinhvien.create')->with([
    											'namhoc'=>$namhoc,
    											'sinhvien'=>$sinhvien_list,
    											'monhoc'=>$monhoc_list,
    											'diem_hechu'=>$diem_hechu
    	]) ;
    }
    public function store(Request $request)
    {

    	$rules = [	'diem_heso1' => 'required|numeric|min:0|max:10',
    				'diem_heso2' => 'required|numeric|min:0|max:4'
    	];
    	$messages = [ 	'diem_heso1.required' => 'Bạn chưa điền điểm hệ số 10',
    					'diem_heso1.numeric' => 'Điểm hệ số 10 là số thực',
    					'diem_heso1.min' => 'Điểm hệ số 10 nhỏ nhất là 0',
    					'diem_heso1.max' => 'Điểm hệ số 10 lớn nhất là 10',
    					'diem_heso2.required' => 'Bạn chưa điền điểm hệ số 4',
    					'diem_heso2.numeric' => 'Điểm hệ số 10 là số thực',
    					'diem_heso2.min' => 'Điểm hệ số 10 nhỏ nhất là 0',
    					'diem_heso2.max' => 'Điểm hệ số 10 lớn nhất là 4',
    	];
    	$this->validate($request,$rules,$messages) ;
    	$data = [ 	'ma_sv' =>$request->ma_sv,
    				'ma_mh' =>$request->ma_mh,
    				'diem_heso1' =>$request->diem_heso1,
    				'diem_heso2' =>$request->diem_heso2,
    				'diem_chu' =>$request->diem_hechu,
    				'nam_hoc' =>$request->namhoc,
    	];
    	$diemsv = DB::table('Diem')->where(array(
    								['ma_sv','=',$request->ma_sv],
    								['ma_mh','=',$request->ma_mh]
    							))->first() ;
    	//var_dump($diemsv) ; die ;
    	if(empty($diemsv)){
    		DB::table('Diem')->insert($data) ;
    		return Redirect::route('diemsv_create_get')->with(['thongbao'=>'thêm thành công']) ;
    	}else{
    		DB::table('Diem')->where('id',$diemsv->id)->update($data) ;
    		return Redirect::route('diemsv_create_get')->with(['thongbao'=>'Diểm đã được update được']) ;
    	}
    	
    }
    public function edit(Request $request,$id)
    {
    	$diemsv = DB::table('Diem')->join('sinhvien','Diem.ma_sv','sinhvien.masv')
    							->join('MonHoc','Diem.ma_mh','MonHoc.ma_mh')
    							->select('Diem.*','sinhvien.hoten as ten_sv','MonHoc.ten_mh')
    							->where('Diem.id','=',$id)
    							->first() ;
    	$namhoc_list = NamHoc::orderBy('nam','DESC')->orderBy('hoc_ky','DESC')->get() ;
    	$namhoc = array() ;
    	foreach($namhoc_list as $key =>$value){
    		$namhoc[$value->id] = $value->hoc_ky.' Năm học '.$value->nam ;
    	}
    	$monhoc_list = MonHoc::all() ;
    	$sinhvien_list = SinhVien::all() ;
    	$diem_hechu = Lang::get('general.diem_hechu') ;
    	return view('admin.diemsinhvien.edit')->with([
    											'namhoc'=>$namhoc,
    											//'sinhvien'=>$sinhvien_list,
    											'monhoc'=>$monhoc_list,
    											'diem_hechu'=>$diem_hechu,
    											'diemsv' =>$diemsv,
    	]) ;
    }
    public function update(Request $request,$id){
    	$rules = [	'diem_heso1' => 'required|numeric|min:0|max:10',
    				'diem_heso2' => 'required|numeric|min:0|max:4'
    	];
    	$messages = [ 	'diem_heso1.required' => 'Bạn chưa điền điểm hệ số 10',
    					'diem_heso1.numeric' => 'Điểm hệ số 10 là số thực',
    					'diem_heso1.min' => 'Điểm hệ số 10 nhỏ nhất là 0',
    					'diem_heso1.max' => 'Điểm hệ số 10 lớn nhất là 10',
    					'diem_heso2.required' => 'Bạn chưa điền điểm hệ số 4',
    					'diem_heso2.numeric' => 'Điểm hệ số 10 là số thực',
    					'diem_heso2.min' => 'Điểm hệ số 10 nhỏ nhất là 0',
    					'diem_heso2.max' => 'Điểm hệ số 10 lớn nhất là 4',
    	];
    	$this->validate($request,$rules,$messages) ;
    	$data = [ 	
    				'ma_mh' =>$request->ma_mh,
    				'diem_heso1' =>$request->diem_heso1,
    				'diem_heso2' =>$request->diem_heso2,
    				'diem_chu' =>$request->diem_hechu,
    				'nam_hoc' =>$request->namhoc,
    	];
    	// $diemsv = DB::table('Diem')->where(array(
    	// 							['ma_sv','=',$request->ma_sv],
    	// 							['ma_mh','=',$request->ma_mh]
    	// 						))->first() ;
    	//var_dump($diemsv) ; die ;
    	// if(empty($diemsv)){
    	// 	DB::table('Diem')->insert($data) ;
    	// 	return Redirect::route('diemsv_create_get')->with(['thongbao'=>'thêm thành công']) ;
    	// }else{
    		DB::table('Diem')->where('id',$id)->update($data) ;
    		return Redirect::route('diemsv_edit_get',$id)->with(['thongbao'=>' Sửa thành công']) ;
    	//}
    }
    public function getModal($id)
    {
    	$model = 'diemsinhvien' ;
    	$confirm_route = $error = null ;
    	try{
    		$confirm_route = route('diemsv_delete',$id) ;
    		return view('admin.layout.modal_confirmation',compact('model','confirm_route','error')) ;
    	}catch(Exception $e){
    		$error = " lỗi rồi haha" ;
    		return view('admin.layout.modal_confirmation',compact('model','confirm_route','error')) ;
    	}
    }
    public function delete($id){
    	try{
    		DB::table('Diem')->where('id',$id)->delete() ;
    		return Redirect::back()->with(['thongbao'=>'Xóa thành công']);
    		//route('diemsv_list')
    	}catch(Exception $e){
    		return Redirect::back()->with(['thongbao'=>'Không thể xóa được']);
    	}
    }
}
