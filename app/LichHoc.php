<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LichHoc extends Model
{
    //
    protected $table = 'LichHoc' ;
    protected $fillable = ['ngay_hoc','tiet_hoc','giang_duong'] ;
}
