<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ChuyenNganh extends Model
{
    //
    protected $table = 'ChuyenNganh' ;
    protected $fillable = ['ma_nganh','ten_nganh'] ;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
