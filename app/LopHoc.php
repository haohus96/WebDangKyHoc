<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LopHoc extends Model
{
    //
    protected $table = 'LopHoc' ;
   // protected $fillable  = [] ;
    protected $fillable = ['ma_mh','lop','so_luong','soluong_dk','nam_hoc','trang_thai'];
    public function getNamHoc(){
    	return $this->belongsTo('App\NamHoc','nam_hoc','id') ;
    }
    public function getMonHoc(){
    	return $this->belongsTo('App\MonHoc','ma_mh','ma_mh') ;
    }
}
