<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonHoc extends Model
{
    //
    protected $table = 'MonHoc' ;
    protected $fillable = ['ma_mh','ten_mh','so_tinchi','hoc_phi'] ;
}
