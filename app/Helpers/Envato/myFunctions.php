<?php

namespace App\Helpers\Envato;
use Illuminate\Http\Request;
 
class myFunctions {
   
    public static function compareString($str1,$str2){
        
    	$arr_str1 = explode('-',$str1) ;
    	$arr_str2 = explode('-',$str2) ;
    	//$arr_str1 = intval($arr_str1) ;
    	$error_exist = '' ;
    	$x = array() ;
    	$y = array() ;
    	for($i = intval($arr_str1[0]) ; $i <= intval($arr_str1[count($arr_str1)-1]) ; $i++){
    		array_push($x,$i) ;
    	}
    	for($j = intval($arr_str2[0]) ; $j <= intval($arr_str2[count($arr_str2)-1]) ; $j++){
    		array_push($y, $j) ;
    	}
    
    	foreach($x as $a){
    		if(in_array($a, $y)){
    			$error_exist = 1 ;
    			break ;
    		}
    	}
    	
        return $error_exist ;
    }
    public static function getLop(){
	   	$lop = array() ;
		for($i=1;$i<=10;$i++){
			if($i<10){
				$lop['0'.$i] = '0'.$i ;
			}else{
				$lop[''.$i] = ''.$i ;
			}
		}
		return $lop ;
   }

}