<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SinhVien extends Model
{
    //
    protected $table = 'SinhVien' ;
    public function getChuyenNganh()
    {
    	return $this->belongsTo('App\ChuyenNganh','chuyennganh','ma_nganh') ;
    }
}
