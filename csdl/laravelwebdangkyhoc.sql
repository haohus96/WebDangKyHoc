-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2018 at 09:10 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravelwebdangkyhoc`
--

-- --------------------------------------------------------

--
-- Table structure for table `chuyennganh`
--

CREATE TABLE `chuyennganh` (
  `ma_nganh` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ten_nganh` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chuyennganh`
--

INSERT INTO `chuyennganh` (`ma_nganh`, `ten_nganh`, `created_at`, `updated_at`, `deleted_at`) VALUES
('7420101', 'Sinh học', '2018-06-24 08:46:08', '2018-07-10 01:21:08', NULL),
('7420201', 'Công nghệ sinh học', '2018-06-24 08:46:29', '2018-06-24 08:46:29', NULL),
('7420201CLC', 'Công nghệ sinh học CTĐT CLC TT23', '2018-06-24 08:47:56', '2018-06-24 08:47:56', NULL),
('7440102', 'Vật lý học', '2018-06-22 07:40:04', '2018-06-22 07:40:04', NULL),
('7440112', 'Hóa học', '2018-06-24 08:41:07', '2018-06-24 08:41:07', NULL),
('7440112TT', 'Hóa học CTĐT tiên tiến', '2018-06-24 08:41:45', '2018-06-24 08:41:45', NULL),
('7440122', 'Khoa học vật liệu', '2018-06-22 07:41:14', '2018-06-22 07:41:14', NULL),
('7440301', 'Khoa học môi trường', '2018-06-24 08:48:34', '2018-06-24 08:48:34', NULL),
('7460101', 'Toán học', '2018-06-22 07:36:01', '2018-06-22 07:36:01', NULL),
('7460117', 'Toán tin UD', '2018-06-22 07:36:53', '2018-06-25 01:37:25', NULL),
('7460201', 'Toán cơ', '2018-06-25 01:42:36', '2018-06-25 01:42:36', NULL),
('7480105', 'Máy tính và khoa học thông tin', '2018-06-22 07:37:38', '2018-06-22 07:37:38', NULL),
('7480105CLC', 'Máy tính và khoa học thông tin CTDT CLC TT23', '2018-06-22 07:38:25', '2018-06-22 07:38:25', NULL),
('7510401', 'Công nghệ kỹ thuật hóa học', '2018-06-24 08:43:32', '2018-06-24 08:43:32', NULL),
('7510401CLC', 'Công nghệ kỹ thuật hóa học CTĐT CLC TT23', '2018-06-24 08:45:05', '2018-06-24 08:45:05', NULL),
('7510407', 'Công nghệ kỹ thuật hạt nhân', '2018-06-22 07:43:30', '2018-06-22 07:43:30', NULL),
('test 22222', 'test linh 22222222', '2018-07-10 01:21:43', '2018-07-10 07:10:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `diem`
--

CREATE TABLE `diem` (
  `id` int(11) NOT NULL,
  `ma_sv` int(11) NOT NULL,
  `ma_mh` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `diem_heso1` double NOT NULL,
  `diem_heso2` double NOT NULL,
  `diem_chu` varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nam_hoc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diem`
--

INSERT INTO `diem` (`id`, `ma_sv`, `ma_mh`, `diem_heso1`, `diem_heso2`, `diem_chu`, `nam_hoc`) VALUES
(1, 14000214, 'FLF2101', 5, 1.5, 'D+', 22),
(2, 14000214, 'MAT3500', 3.5, 0, 'F', 24);

-- --------------------------------------------------------

--
-- Table structure for table `dssinhviendk`
--

CREATE TABLE `dssinhviendk` (
  `id_lophoc` int(11) NOT NULL,
  `ma_sv` int(11) NOT NULL,
  `trang_thai` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dssinhviendk`
--

INSERT INTO `dssinhviendk` (`id_lophoc`, `ma_sv`, `trang_thai`) VALUES
(8, 14000214, 'Học cải thiện');

-- --------------------------------------------------------

--
-- Table structure for table `lichhoc`
--

CREATE TABLE `lichhoc` (
  `id` int(11) NOT NULL,
  `ngay_hoc` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tiet_hoc` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `giang_duong` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lichhoc`
--

INSERT INTO `lichhoc` (`id`, `ngay_hoc`, `tiet_hoc`, `giang_duong`, `created_at`, `updated_at`) VALUES
(17, 'T4', '6-7', '103-T5', '2018-06-25 02:12:06', '2018-06-25 02:12:06'),
(18, 'T6', '6-8', 'PM (ca 1)', '2018-06-25 02:13:50', '2018-06-25 02:13:50'),
(19, 'T6', '6-8', 'PM (ca 2)', '2018-06-25 02:14:15', '2018-06-25 02:14:15'),
(20, 'T4', '9-10', '101-T5', '2018-06-25 02:16:35', '2018-06-25 02:16:35'),
(21, 'T4', '6-8', '104-T4', '2018-06-25 02:17:25', '2018-06-25 02:17:25'),
(22, 'T5', '1-3', '102-T5', '2018-06-25 02:19:34', '2018-06-25 02:19:34'),
(23, 'T2', '9-10', '207-T5', '2018-06-25 02:20:06', '2018-06-25 02:20:06'),
(24, 'T3', '6-7', '207-T5', '2018-06-25 02:20:19', '2018-06-25 02:20:19'),
(25, 'T2', '9-10', '205-T5', '2018-06-25 02:20:56', '2018-06-25 02:20:56'),
(26, 'T3', '6-7', '205-T5', '2018-06-25 02:21:15', '2018-06-25 02:21:15'),
(27, 'T2', '6-7', '206-T5', '2018-06-25 02:22:10', '2018-06-25 02:22:10'),
(28, 'T5', '4-5', '206-T5', '2018-06-25 02:22:38', '2018-06-25 02:22:38'),
(29, 'T4', '1-3', '102-T5', '2018-06-25 02:23:11', '2018-06-25 02:23:11'),
(30, 'T2', '6-7', '204-T5', '2018-06-25 02:28:18', '2018-06-25 02:28:18'),
(31, 'T5', '4-5', '204-T5', '2018-06-25 02:28:45', '2018-06-25 02:28:45'),
(32, 'T4', '4-5', '108-T5', '2018-06-25 02:29:08', '2018-06-25 02:29:08'),
(33, 'T2', '4-5', '108-T5', '2018-06-25 02:29:58', '2018-06-25 02:29:58'),
(34, 'T2', '1-2', '105-T5', '2018-06-25 02:31:12', '2018-06-25 02:31:12'),
(35, 'T6', '4-5', '101-T5', '2018-06-25 02:31:41', '2018-06-25 02:31:41'),
(36, 'T4', '9-10', '102-T4', '2018-06-25 02:32:15', '2018-06-25 02:32:15'),
(37, 'T6', '1-2', '102-T4', '2018-06-25 02:32:52', '2018-06-25 02:32:52'),
(38, 'T4', '1-2', '102-T4', '2018-06-25 02:33:37', '2018-06-25 02:33:37'),
(39, 'T2', '1-2', '102-T4', '2018-06-25 02:34:00', '2018-06-25 02:34:00'),
(40, 'T2', '4-5', '103-T4', '2018-06-25 02:34:44', '2018-06-25 02:34:44'),
(41, 'T4', '6-7', 'PM', '2018-06-25 02:35:25', '2018-06-25 02:35:25'),
(42, 'T6', '4-5', '103-T4', '2018-06-25 02:35:48', '2018-06-25 02:35:48'),
(43, 'T4', '8-9', 'PM', '2018-06-25 02:36:08', '2018-06-25 02:36:08'),
(44, 'T2', '6-8', '108-T5', '2018-06-25 02:36:33', '2018-06-25 02:36:33'),
(45, 'T6', '9-10', '108T4', '2018-06-25 02:38:25', '2018-06-25 02:46:21'),
(46, 'T6', '9-10', 'PM A', '2018-06-25 02:46:45', '2018-06-25 02:46:45'),
(47, 'T3', '9-10', '102-T5', '2018-06-25 03:39:58', '2018-06-25 03:39:58'),
(48, 'T3', '6-8', '108-T5', '2018-06-25 03:46:07', '2018-06-25 03:46:07'),
(49, 'T4', '4-5', '102-T5', '2018-06-25 03:46:29', '2018-06-25 03:46:29'),
(50, 'T4', '1-3', '108-T5', '2018-06-25 03:51:38', '2018-06-25 03:51:38'),
(51, 'T4', '7-10', 'PM A', '2018-06-25 03:54:59', '2018-06-25 03:54:59'),
(52, 'T5', '9-10', '102T5', '2018-06-25 04:05:47', '2018-06-25 04:05:47'),
(53, 'T6', '9-10', 'PM', '2018-06-25 04:06:04', '2018-06-25 04:06:04'),
(54, 'T6', '6-7', 'PM', '2018-06-25 04:06:33', '2018-06-25 04:06:33'),
(55, 'T2', '1-3', '108-T5', '2018-06-25 09:11:20', '2018-06-25 09:11:20'),
(56, 'T2', '8-10', '103-T4', '2018-06-25 09:17:02', '2018-06-25 09:17:02'),
(57, 'T3', '1-3', '103T5', '2018-06-25 09:21:34', '2018-06-25 09:21:34'),
(58, 'T3', '1-3', '511-T4', '2018-06-26 01:05:56', '2018-06-26 01:05:56'),
(59, 'T6', '1-3', '511-T4', '2018-06-26 01:06:05', '2018-06-26 01:06:05'),
(60, 'T3', '1-3', '514-T4', '2018-06-26 01:08:07', '2018-06-26 01:08:07'),
(61, 'T6', '1-3', '514-T4', '2018-06-26 01:08:21', '2018-06-26 01:08:21'),
(62, 'T2', '6-8', '510-T5', '2018-06-26 01:13:15', '2018-06-26 01:13:15'),
(63, 'T5', '6-8', '510-T5', '2018-06-26 01:13:23', '2018-06-26 01:13:23'),
(64, 'T2', '1-3', '410-T5', '2018-06-26 01:16:05', '2018-06-26 01:16:05'),
(65, 'T4', '1-3', '410-T5', '2018-06-26 01:16:12', '2018-06-26 01:16:12'),
(66, 'T3', '6-8', '406-T5', '2018-06-26 01:17:25', '2018-06-26 01:17:25'),
(67, 'T5', '6-8', '406-T5', '2018-06-26 01:17:37', '2018-06-26 01:17:37'),
(68, 'T3', '9-10', 'Sân 2', '2018-06-26 01:21:16', '2018-06-26 01:21:16'),
(69, 'T4', '1-2', 'Sân 1', '2018-06-26 01:22:32', '2018-06-26 01:22:32'),
(70, 'T5', '4-5', '102-T5', '2018-06-26 01:26:00', '2018-06-26 01:26:00'),
(71, 'T3', '6-10', 'PM', '2018-06-26 01:27:00', '2018-06-26 01:27:00'),
(72, 'T5', '9-10', '102-T5', '2018-06-26 01:53:27', '2018-06-26 01:53:27'),
(77, 'T2', '10', '401T5', '2018-07-05 04:54:17', '2018-07-05 04:54:17'),
(79, 'T7', '1-3', '102T2', '2018-07-06 12:52:31', '2018-07-06 12:52:31'),
(80, 'T4', '1-3', 'Sân 2', '2018-07-12 06:50:56', '2018-07-12 06:50:56'),
(81, 'T6', '3-5', 'Sân 1', '2018-07-12 06:51:11', '2018-07-12 06:51:11');

-- --------------------------------------------------------

--
-- Table structure for table `loaihocphi`
--

CREATE TABLE `loaihocphi` (
  `id` int(11) NOT NULL,
  `ten` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `loai_diem` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `heso_hocphi` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loaihocphi`
--

INSERT INTO `loaihocphi` (`id`, `ten`, `loai_diem`, `heso_hocphi`) VALUES
(1, 'Học lần đầu', '', 1),
(2, 'Học lại', 'F', 1),
(3, 'Học cải thiện', 'D,D+', 1.5);

-- --------------------------------------------------------

--
-- Table structure for table `lophoc`
--

CREATE TABLE `lophoc` (
  `id` int(11) NOT NULL,
  `ma_mh` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lop` varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `so_luong` int(11) NOT NULL,
  `soluong_dk` int(11) NOT NULL DEFAULT '0',
  `nam_hoc` int(11) NOT NULL,
  `trang_thai` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lophoc`
--

INSERT INTO `lophoc` (`id`, `ma_mh`, `lop`, `so_luong`, `soluong_dk`, `nam_hoc`, `trang_thai`, `created_at`, `updated_at`) VALUES
(1, 'MAT3452', '01', 77, 77, 21, 1, '2018-06-25 03:28:38', '2018-07-12 07:00:45'),
(2, 'MAT3452', '02', 77, 8, 21, 1, '2018-06-25 03:30:14', '2018-07-12 07:03:55'),
(3, 'MAT3500', '01', 110, 5, 21, 1, '2018-06-25 03:37:21', '2018-07-12 07:04:04'),
(4, 'MAT3500', '02', 110, 1, 21, 1, '2018-06-25 03:48:11', '2018-07-12 07:00:45'),
(5, 'MAT3514', '01', 110, 1, 21, 1, '2018-06-25 03:58:42', '2018-07-12 07:00:45'),
(6, 'MAT3506', '01', 110, 3, 21, 1, '2018-06-25 09:11:55', '2018-07-12 07:00:45'),
(7, 'MAT3507', '01', 77, 0, 21, 1, '2018-06-25 09:20:10', '2018-07-12 07:00:45'),
(8, 'FLF2101', '01', 35, 1, 21, 1, '2018-06-26 01:07:11', '2018-07-12 07:00:45'),
(9, 'FLF2101', '02', 35, 1, 21, 1, '2018-06-26 01:12:36', '2018-07-12 07:00:45'),
(10, 'FLF2101', '03', 35, 1, 21, 1, '2018-06-26 01:14:07', '2018-07-12 07:00:45'),
(11, 'FLF2102', '01', 35, 1, 21, 1, '2018-06-26 01:16:55', '2018-07-12 07:00:45'),
(12, 'FLF2102', '02', 35, 0, 21, 1, '2018-06-26 01:18:20', '2018-07-12 07:00:45'),
(13, 'PES1005', '01', 50, 0, 21, 1, '2018-06-26 01:22:04', '2018-07-12 07:00:45'),
(14, 'PES1005', '02', 50, 0, 21, 1, '2018-06-26 01:23:11', '2018-07-12 07:00:45'),
(15, 'MAT3520', '01', 50, 0, 21, 1, '2018-06-26 01:27:43', '2018-07-12 07:00:45'),
(16, 'MAT3505', '01', 50, 0, 21, 1, '2018-06-26 01:54:27', '2018-07-12 07:00:45'),
(17, 'MAT3505', '02', 50, 0, 21, 1, '2018-06-26 01:55:32', '2018-07-12 07:00:45'),
(18, 'MAT3452', '01', 30, 0, 22, 0, '2018-07-10 03:09:57', '2018-07-12 06:58:20'),
(19, 'PES1020', '01', 50, 0, 21, 1, '2018-07-12 06:51:54', '2018-07-12 07:00:45'),
(20, 'PES1020', '02', 50, 0, 21, 1, '2018-07-12 06:52:19', '2018-07-12 07:00:45'),
(21, 'MAT2405', '01', 50, 0, 21, 1, '2018-07-12 06:53:53', '2018-07-12 07:00:45'),
(22, 'MAT2314', '01', 50, 0, 21, 1, '2018-07-12 06:54:19', '2018-07-12 07:00:45'),
(23, 'MAT2403', '01', 50, 0, 21, 1, '2018-07-12 06:54:55', '2018-07-12 07:00:45'),
(24, 'MAT3507', '02', 50, 0, 21, 1, '2018-07-12 06:58:57', '2018-07-12 07:00:45'),
(25, 'MAT2407', '01', 50, 0, 21, 1, '2018-07-12 07:00:00', '2018-07-12 07:00:45'),
(26, 'MAT3504', '01', 50, 0, 21, 1, '2018-07-12 07:00:27', '2018-07-12 07:00:45');

-- --------------------------------------------------------

--
-- Table structure for table `lophoc_lichhoc`
--

CREATE TABLE `lophoc_lichhoc` (
  `id_lichhoc` int(11) NOT NULL,
  `id_lophoc` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lophoc_lichhoc`
--

INSERT INTO `lophoc_lichhoc` (`id_lichhoc`, `id_lophoc`, `created_at`, `updated_at`) VALUES
(40, 1, NULL, NULL),
(41, 1, NULL, NULL),
(42, 2, NULL, NULL),
(43, 2, NULL, NULL),
(44, 3, NULL, NULL),
(47, 3, NULL, NULL),
(48, 4, NULL, NULL),
(49, 4, NULL, NULL),
(50, 5, NULL, NULL),
(51, 5, NULL, NULL),
(55, 6, NULL, NULL),
(56, 7, NULL, NULL),
(57, 7, NULL, NULL),
(60, 9, NULL, NULL),
(61, 9, NULL, NULL),
(62, 10, NULL, NULL),
(63, 10, NULL, NULL),
(64, 11, NULL, NULL),
(65, 11, NULL, NULL),
(66, 12, NULL, NULL),
(67, 12, NULL, NULL),
(68, 13, NULL, NULL),
(69, 14, NULL, NULL),
(70, 15, NULL, NULL),
(71, 15, NULL, NULL),
(52, 16, NULL, NULL),
(54, 16, NULL, NULL),
(53, 17, NULL, NULL),
(72, 17, NULL, NULL),
(58, 8, NULL, NULL),
(59, 8, NULL, NULL),
(80, 19, NULL, NULL),
(81, 20, NULL, NULL),
(17, 22, NULL, NULL),
(22, 22, NULL, NULL),
(27, 23, NULL, NULL),
(29, 23, NULL, NULL),
(17, 21, NULL, NULL),
(22, 21, NULL, NULL),
(40, 18, NULL, NULL),
(41, 18, NULL, NULL),
(24, 24, NULL, NULL),
(46, 24, NULL, NULL),
(32, 25, NULL, NULL),
(33, 25, NULL, NULL),
(34, 26, NULL, NULL),
(35, 26, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `monhoc`
--

CREATE TABLE `monhoc` (
  `ma_mh` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ten_mh` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `so_tinchi` int(1) NOT NULL,
  `hoc_phi` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monhoc`
--

INSERT INTO `monhoc` (`ma_mh`, `ten_mh`, `so_tinchi`, `hoc_phi`, `created_at`, `updated_at`) VALUES
('BIO2201', 'Sinh học phân tử N2 (60SH)', 3, 660000, NULL, '2018-07-11 03:31:47'),
('BIO2204', 'Vi sinh vật học', 3, 660000, '2018-07-03 01:35:14', '2018-07-11 03:31:47'),
('BIO3201', 'Động vật học động vật không xương sống', 3, 660000, '2018-07-03 01:36:52', '2018-07-11 03:31:47'),
('FLF2101', 'Tiếng Anh cơ sở 1', 4, 880000, '2018-06-24 14:47:45', '2018-07-11 03:31:47'),
('FLF2102', 'Tiếng Anh cơ sở 2', 5, 1100000, '2018-06-24 14:50:02', '2018-07-11 03:31:48'),
('FLF2103', 'Tiếng Anh cơ sở 3', 5, 1100000, '2018-06-25 01:22:54', '2018-07-11 03:31:48'),
('HIS1002', 'Đường lối cách mạng của Đảng Cộng Sản Việt Nam', 3, 660000, '2018-06-25 01:36:29', '2018-07-11 03:31:48'),
('INT1003', 'Tin học cơ sở 1', 2, 440000, '2018-06-25 02:09:57', '2018-07-11 03:31:48'),
('INT1006', 'Tin học cơ sở 4', 3, 660000, '2018-06-25 01:39:59', '2018-07-11 03:31:48'),
('MAT2301', 'Đại số tuyến tính 2', 4, 880000, '2018-06-25 02:04:10', '2018-07-11 03:31:48'),
('MAT2303', 'Giải tích 2', 5, 1100000, '2018-06-25 02:06:41', '2018-07-11 03:31:48'),
('MAT2314', 'Phương trình vi phân', 4, 880000, '2018-06-25 02:08:23', '2018-07-11 03:31:48'),
('MAT2402', 'Giải tích 2', 5, 1100000, '2018-06-25 01:45:46', '2018-07-11 03:31:48'),
('MAT2403', 'Phương trình vi phân', 3, 660000, '2018-06-25 01:49:34', '2018-07-11 03:31:48'),
('MAT2405', 'Xác suất', 3, 660000, '2018-06-25 01:47:02', '2018-07-11 03:31:48'),
('MAT2407', 'Tối ưu hóa', 3, 660000, '2018-06-25 01:48:42', '2018-07-11 03:31:48'),
('MAT3452', 'Phân tích thống kê nhiều chiều', 3, 660000, '2018-06-25 01:51:22', '2018-07-11 03:31:48'),
('MAT3500', 'Toán rời rạc (Discrete Mathematics)', 4, 880000, '2018-06-25 01:52:49', '2018-07-11 03:31:48'),
('MAT3504', 'Thiết kế và đánh giá thuật toán', 3, 660000, '2018-06-25 01:54:03', '2018-07-11 03:31:48'),
('MAT3505', 'Kiến trúc máy tính', 3, 660000, '2018-06-25 01:54:46', '2018-07-11 03:31:48'),
('MAT3506', 'Mạng máy tính', 3, 660000, '2018-06-25 01:55:27', '2018-07-11 03:31:48'),
('MAT3507', 'Cơ sở dữ liệu', 4, 880000, '2018-06-25 01:55:56', '2018-07-11 03:31:48'),
('MAT3509', 'Ngôn ngữ hình thức và Otomat', 3, 660000, '2018-06-25 01:57:24', '2018-07-11 03:31:48'),
('MAT3510', 'Đồ án phần mềm', 3, 660000, '2018-06-25 01:58:26', '2018-07-11 03:31:48'),
('MAT3514', 'Cấu trúc dữ liệu và thuật toán', 3, 660000, '2018-06-25 01:53:32', '2018-07-11 03:31:48'),
('MAT3520', 'Lập trình C/C++', 3, 660000, '2018-06-25 01:59:14', '2018-07-11 03:31:48'),
('PES1005', 'Thể dục Aerobic 1', 1, 220000, '2018-06-25 01:25:37', '2018-07-11 03:31:48'),
('PES1015', 'Bóng chuyền 1', 1, 220000, '2018-06-25 01:26:08', '2018-07-11 03:31:48'),
('PES1017', 'Bóng chuyền hơi', 1, 220000, '2018-06-25 01:26:33', '2018-07-11 03:31:49'),
('PES1020', 'Bóng rổ 1', 1, 220000, '2018-06-25 01:27:02', '2018-07-11 03:31:49'),
('PES1045', 'Khiêu vũ thể thao 1', 1, 220000, '2018-06-25 01:27:46', '2018-07-11 03:31:49'),
('PES1050', 'Teakwondo 1', 1, 220000, '2018-06-25 01:28:20', '2018-07-11 03:31:49'),
('PHI1004', 'Những nguyên lý cơ bản của chủ nghĩa Mác – Lênin 1', 2, 440000, '2018-06-25 01:29:36', '2018-07-11 03:31:49'),
('PHI1005', 'Những nguyên lý cơ bản của chủ nghĩa Mác – Lênin 2', 3, 660000, '2018-06-25 01:30:02', '2018-07-11 03:31:49'),
('PHY1100', 'Cơ - nhiệt', 3, 660000, '2018-06-25 01:32:12', '2018-07-11 03:31:49'),
('PHY1103', 'Điện - Quang', 3, 660000, '2018-06-25 01:33:58', '2018-07-11 03:31:49'),
('POL1001', 'Tư tưởng Hồ Chí Minh', 2, 440000, '2018-06-25 01:35:18', '2018-07-11 03:31:49');

-- --------------------------------------------------------

--
-- Table structure for table `namhoc`
--

CREATE TABLE `namhoc` (
  `id` int(11) NOT NULL,
  `hoc_ky` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nam` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `namhoc`
--

INSERT INTO `namhoc` (`id`, `hoc_ky`, `nam`, `created_at`, `updated_at`) VALUES
(17, 'Học kỳ 1', '2018-2019', '2018-06-18 09:42:22', '2018-06-18 09:42:22'),
(18, 'Học kỳ 2', '2018-2019', '2018-06-18 09:42:24', '2018-06-18 09:42:24'),
(19, 'Học kỳ 1', '2019-2020', '2018-06-19 01:57:05', '2018-06-19 01:57:05'),
(20, 'Học kỳ 2', '2019-2020', '2018-06-19 01:57:53', '2018-06-19 01:57:53'),
(21, 'Học kỳ 2', '2017-2018', '2018-06-19 02:49:30', '2018-06-19 02:49:30'),
(22, 'Học kỳ 1', '2017-2018', '2018-06-19 02:49:34', '2018-06-19 02:49:34'),
(23, 'Học kỳ 1', '2020-2021', '2018-07-02 12:03:52', '2018-07-02 12:03:52'),
(24, 'Học kỳ 1', '2016-2017', '2018-07-04 17:00:00', '2018-07-04 17:00:00'),
(25, 'Học kỳ 2', '2020-2021', '2018-07-05 02:08:42', '2018-07-05 02:08:42'),
(26, 'Học kỳ 1', '2021-2022', '2018-07-05 02:08:50', '2018-07-05 02:08:50');

-- --------------------------------------------------------

--
-- Table structure for table `nganhhoc_monhoc`
--

CREATE TABLE `nganhhoc_monhoc` (
  `id` int(11) NOT NULL,
  `ma_nganh` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ma_mh` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nganhhoc_monhoc`
--

INSERT INTO `nganhhoc_monhoc` (`id`, `ma_nganh`, `ma_mh`) VALUES
(3, '1', 'FLF2101'),
(2, '1', 'FLF2102'),
(4, '1', 'FLF2103'),
(28, '1', 'HIS1002'),
(54, '1', 'INT1003'),
(5, '1', 'PES1005'),
(6, '1', 'PES1015'),
(7, '1', 'PES1017'),
(8, '1', 'PES1020'),
(9, '1', 'PES1045'),
(10, '1', 'PES1050'),
(11, '1', 'PHI1004'),
(12, '1', 'PHI1005'),
(27, '1', 'POL1001'),
(55, '7420101', 'BIO2201'),
(56, '7420101', 'BIO2204'),
(57, '7420101', 'BIO3201'),
(19, '7420101', 'PHY1103'),
(20, '7420201', 'PHY1103'),
(13, '7440112', 'PHY1100'),
(21, '7440112', 'PHY1103'),
(14, '7440301', 'PHY1100'),
(22, '7440301', 'PHY1103'),
(48, '7460101', 'MAT2301'),
(50, '7460101', 'MAT2303'),
(15, '7460101', 'PHY1100'),
(23, '7460101', 'PHY1103'),
(29, '7460117', 'INT1006'),
(49, '7460117', 'MAT2301'),
(51, '7460117', 'MAT2303'),
(52, '7460117', 'MAT2314'),
(32, '7460117', 'MAT2405'),
(42, '7460117', 'MAT3507'),
(45, '7460117', 'MAT3510'),
(16, '7460117', 'PHY1100'),
(24, '7460117', 'PHY1103'),
(53, '7460201', 'MAT2314'),
(30, '7480105', 'INT1006'),
(31, '7480105', 'MAT2402'),
(35, '7480105', 'MAT2403'),
(33, '7480105', 'MAT2405'),
(34, '7480105', 'MAT2407'),
(36, '7480105', 'MAT3452'),
(37, '7480105', 'MAT3500'),
(39, '7480105', 'MAT3504'),
(40, '7480105', 'MAT3505'),
(41, '7480105', 'MAT3506'),
(43, '7480105', 'MAT3507'),
(44, '7480105', 'MAT3509'),
(46, '7480105', 'MAT3510'),
(38, '7480105', 'MAT3514'),
(47, '7480105', 'MAT3520'),
(17, '7480105', 'PHY1100'),
(25, '7480105', 'PHY1103'),
(18, '7510401', 'PHY1100'),
(26, '7510401', 'PHY1103');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sinhvien`
--

CREATE TABLE `sinhvien` (
  `masv` int(11) NOT NULL,
  `hoten` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ngaysinh` date NOT NULL,
  `noisinh` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gioitinh` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `chuyennganh` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sinhvien`
--

INSERT INTO `sinhvien` (`masv`, `hoten`, `ngaysinh`, `noisinh`, `gioitinh`, `email`, `chuyennganh`, `created_at`, `updated_at`) VALUES
(14000001, 'Nguyễn Hoàng An', '1996-11-21', 'Hưng Yên', 'Nam', 'nguyenhoangan_t59@hus.edu.vn', '7480105', '2018-06-24 10:34:11', '2018-06-24 10:34:11'),
(14000046, 'Trần Nam Anh', '1996-12-30', 'Hà nội', 'Nam', 'trannamanh_t59@hus.edu.vn', '7480105', '2018-06-24 10:23:49', '2018-06-24 10:23:49'),
(14000100, 'Hoàng Văn Cường', '1996-09-17', 'Nam Định', 'Nam', 'hoangvancuong_t59@hus.edu.vn', '7480105', '2018-06-25 01:12:48', '2018-06-25 01:12:48'),
(14000181, 'Phạm Thị Gấm', '1996-03-12', 'Ninh Bình', 'Nữ', 'phamthigam_t59@hus.edu.vn', '7480105', '2018-06-24 10:26:17', '2018-06-24 10:26:17'),
(14000214, 'Phạm Văn Hào', '1996-09-07', 'kim sơn Ninh Bình', '', 'phamvanhao_t59@hus.edu.vn', '7480105', '2018-06-15 08:35:32', '2018-06-24 08:31:36'),
(14000215, 'Nguyễn văn A', '1995-08-16', 'Hà nội', '', 'admin@admin.com', '7440122', '2018-06-20 01:45:27', '2018-06-24 08:31:50'),
(14000227, 'Phạm Hồng Hạnh', '1996-03-29', 'Nghệ An', 'Nữ', 'phamhonghanh_t59@hus.edu.vn', '7480105', '2018-06-24 10:29:48', '2018-06-24 10:29:48'),
(14000985, 'Mai Anh Đức', '1995-10-18', 'Hải Dương', 'Nam', 'maianhduc_t59@hus.edu.vn', '7480105', '2018-06-25 01:16:47', '2018-06-25 01:16:47'),
(14001056, 'Đỗ Xuân Tiến', '1996-09-07', 'Hải Phòng', 'Nam', 'doxuantien_t59@hus.edu.vn', '7480105', '2018-06-25 01:18:12', '2018-06-25 01:18:12'),
(14001184, 'Vũ Mạnh Cường', '1996-01-02', 'Thái Bình', 'Nam', 'vumanhcuong_t59@hus.edu.vn', '7480105', '2018-06-25 01:20:27', '2018-06-25 01:20:27'),
(14002064, 'Trần Anh Đức', '1996-01-20', 'Thanh Hóa', 'Nam', 'trananhduc_t59@hus.edu.vn', '7480105', '2018-06-25 01:22:01', '2018-06-25 01:22:01'),
(14002095, 'Bùi Thu Hằng', '1996-06-06', 'Bắc Giang', 'Nữ', 'buithuhang_t59@hus.edu.vn', '7420101', '2018-06-24 10:31:40', '2018-06-24 10:31:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `email`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, '14000214', 'Phạm Văn Hào', 'phamvanhao_t59@hus.edu.vn', '$2y$10$NOt9LG/lHEw5T5cRP9BVhOhqQiGMEs7di4mCCdEZ71cVX1n/TqtSu', 0, '6H0KMyIRpx9yimhFjj0OYknr8NRBKCRyLn74xXo4i2fw7e7yQDxQGMcvA3TT', '2018-06-20 09:54:22', '2018-06-30 13:22:09'),
(6, '14000215', 'Nguyễn văn A', 'admin@admin.com', '$2y$10$RiWQaaH8SmdfCP.1EB/82uNxaB2cvpZSfY0lK6FuLQ2sMcTqtXCXK', 0, NULL, '2018-06-21 02:25:31', '2018-06-21 02:25:31'),
(7, '14000001', 'Nguyễn Hoàng An', 'nguyenhoangan_t59@hus.edu.vn', '$2y$10$Q/uCTX028pFUhW257otZh.9WEeSVIDPSoHBrX9zEnv4dUscHN9mb.', 0, NULL, '2018-06-26 02:53:41', '2018-06-26 02:53:41'),
(8, 'admin', 'Phạm Văn Hào sửa', 'phamhao96.hus@gmail.com', '$2y$10$ZtTMUwGrE1Npq0afY/hGgeP4NMpyXe1yNpjxyze9EmSrD8Hy9i0Ia', 1, 'KjkKGzmW1wyPTKFl2ieByVmIxREHvnKuDSuKPzLWwEAq65zsFFLZpZixdwxG', '2018-07-10 01:39:15', '2018-07-11 08:15:30'),
(9, 'haohao', 'Bùi Đức Phú', 'haophamhus.jb@gmail.com', '$2y$10$s2oqLl5zk.r/rYD2deyXe.fweb72eM6yTWaDYbValP0MVdLc7RuRK', 1, NULL, '2018-07-10 02:00:15', '2018-07-10 02:00:15'),
(11, '14000100', 'Hoàng Văn Cường', 'hoangvancuong_t59@hus.edu.vn', '$2y$10$IXjqwpu1.FQKZ4GVy2dfGuwKZkJVQGgX59g4nS59Wco9RwZs2DCLu', 0, NULL, '2018-07-11 06:55:18', '2018-07-11 06:55:18'),
(12, '14000181', 'Phạm Thị Gấm', 'phamthigam_t59@hus.edu.vn', '$2y$10$BQKEeBCCdGPWK644vib03.Lu1TclTCncqpigQLWCee14AFV2QaTr.', 0, NULL, '2018-07-11 06:55:18', '2018-07-11 06:55:18'),
(13, '14000227', 'Phạm Hồng Hạnh', 'phamhonghanh_t59@hus.edu.vn', '$2y$10$bmfMXBR0K/TRM2MBvdszRukna65W.NQQX60fz3V1CAhWOf62qNrtm', 0, NULL, '2018-07-11 06:55:18', '2018-07-11 06:55:18'),
(14, '14000985', 'Mai Anh Đức', 'maianhduc_t59@hus.edu.vn', '$2y$10$KcNLMn105tz9XFQQT2cNUugktZGMRgSbmHldcbn.V4bc8L/vMCdTK', 0, NULL, '2018-07-11 06:55:18', '2018-07-11 06:55:18'),
(15, '14001056', 'Đỗ Xuân Tiến', 'doxuantien_t59@hus.edu.vn', '$2y$10$tfTI3b0ku29k0pK351st4.Yp5HFT0ZBqQmyBQhwxBSkdG2gtV8MIS', 0, NULL, '2018-07-11 06:55:18', '2018-07-11 06:55:18'),
(19, '14000046', 'Trần Nam Anh', 'trannamanh_t59@hus.edu.vn', '$2y$10$OR0.NcS8m/8o/XVxqA6L7.Xlsn.qLihTnelpAB0.V6QbQnY1HvdO6', 0, 'NKPNgjwZCJUXOKLKLxvT7sRmtfGcIiVCcDeoV0DWDJcR35ohX8dRO9ee8eMz', '2018-07-11 07:09:49', '2018-07-11 07:09:49'),
(20, '14001184', 'Vũ Mạnh Cường', 'vumanhcuong_t59@hus.edu.vn', '$2y$10$k0zLmFAfCuSqhoU7QbDhm.5mTkaVt.whd/v4VpY4HL6tpMTOwRfam', 0, NULL, '2018-07-11 07:09:49', '2018-07-11 07:09:49'),
(21, '14002064', 'Trần Anh Đức', 'trananhduc_t59@hus.edu.vn', '$2y$10$zRCbqxlf1B4qf18Lr2xRhuCBD0CJdvIpl.QCYc6vCZIQETJ9ZPcT6', 0, NULL, '2018-07-11 07:09:50', '2018-07-11 07:09:50'),
(22, '14002095', 'Bùi Thu Hằng', 'buithuhang_t59@hus.edu.vn', '$2y$10$BiPMXQH4FoCP5ffnEzQ2VuNi/C60jPCCBPIq4kqkIC9n2Eb6Vfrve', 0, NULL, '2018-07-11 07:09:50', '2018-07-11 07:09:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chuyennganh`
--
ALTER TABLE `chuyennganh`
  ADD PRIMARY KEY (`ma_nganh`);

--
-- Indexes for table `diem`
--
ALTER TABLE `diem`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ma_sv` (`ma_sv`,`ma_mh`) USING BTREE,
  ADD KEY `nam_hoc` (`nam_hoc`),
  ADD KEY `ma_mh` (`ma_mh`);

--
-- Indexes for table `dssinhviendk`
--
ALTER TABLE `dssinhviendk`
  ADD KEY `id_lophoc` (`id_lophoc`),
  ADD KEY `ma_sv` (`ma_sv`);

--
-- Indexes for table `lichhoc`
--
ALTER TABLE `lichhoc`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ngay_hoc` (`ngay_hoc`,`tiet_hoc`,`giang_duong`) USING BTREE;

--
-- Indexes for table `loaihocphi`
--
ALTER TABLE `loaihocphi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lophoc`
--
ALTER TABLE `lophoc`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ma_mh` (`ma_mh`,`lop`,`nam_hoc`) USING BTREE,
  ADD KEY `nam_hoc` (`nam_hoc`);

--
-- Indexes for table `lophoc_lichhoc`
--
ALTER TABLE `lophoc_lichhoc`
  ADD KEY `id_lophoc` (`id_lophoc`),
  ADD KEY `id_lichhoc` (`id_lichhoc`) USING BTREE;

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monhoc`
--
ALTER TABLE `monhoc`
  ADD PRIMARY KEY (`ma_mh`);

--
-- Indexes for table `namhoc`
--
ALTER TABLE `namhoc`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hoc_ky` (`hoc_ky`,`nam`);

--
-- Indexes for table `nganhhoc_monhoc`
--
ALTER TABLE `nganhhoc_monhoc`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ma_nganh` (`ma_nganh`,`ma_mh`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD PRIMARY KEY (`masv`),
  ADD KEY `chuyennganh` (`chuyennganh`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `diem`
--
ALTER TABLE `diem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lichhoc`
--
ALTER TABLE `lichhoc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `loaihocphi`
--
ALTER TABLE `loaihocphi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lophoc`
--
ALTER TABLE `lophoc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `namhoc`
--
ALTER TABLE `namhoc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `nganhhoc_monhoc`
--
ALTER TABLE `nganhhoc_monhoc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `diem`
--
ALTER TABLE `diem`
  ADD CONSTRAINT `diem_ibfk_1` FOREIGN KEY (`ma_sv`) REFERENCES `sinhvien` (`masv`),
  ADD CONSTRAINT `diem_ibfk_2` FOREIGN KEY (`ma_mh`) REFERENCES `monhoc` (`ma_mh`),
  ADD CONSTRAINT `diem_ibfk_3` FOREIGN KEY (`nam_hoc`) REFERENCES `namhoc` (`id`);

--
-- Constraints for table `dssinhviendk`
--
ALTER TABLE `dssinhviendk`
  ADD CONSTRAINT `dssinhviendk_ibfk_1` FOREIGN KEY (`id_lophoc`) REFERENCES `lophoc` (`id`),
  ADD CONSTRAINT `dssinhviendk_ibfk_2` FOREIGN KEY (`ma_sv`) REFERENCES `sinhvien` (`masv`);

--
-- Constraints for table `lophoc`
--
ALTER TABLE `lophoc`
  ADD CONSTRAINT `lophoc_ibfk_1` FOREIGN KEY (`ma_mh`) REFERENCES `monhoc` (`ma_mh`),
  ADD CONSTRAINT `lophoc_ibfk_2` FOREIGN KEY (`nam_hoc`) REFERENCES `namhoc` (`id`);

--
-- Constraints for table `lophoc_lichhoc`
--
ALTER TABLE `lophoc_lichhoc`
  ADD CONSTRAINT `lophoc_lichhoc_ibfk_1` FOREIGN KEY (`id_lichhoc`) REFERENCES `lichhoc` (`id`),
  ADD CONSTRAINT `lophoc_lichhoc_ibfk_2` FOREIGN KEY (`id_lophoc`) REFERENCES `lophoc` (`id`);

--
-- Constraints for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD CONSTRAINT `sinhvien_ibfk_1` FOREIGN KEY (`chuyennganh`) REFERENCES `chuyennganh` (`ma_nganh`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
