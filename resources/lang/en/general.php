<?php
$lop = array() ;
for($i=1;$i<=10;$i++){
	if($i<10){
		$lop['0'.$i] = '0'.$i ;
	}else{
		$lop[''.$i] = ''.$i ;
	}
}
return array(	'nganh' => ['1' => 'Môn toàn trường'] ,
				'hocky'	=> array(	'Học kỳ 1',
									'Học kỳ 2'
								),
				'thu'	=> array(	'T2' 	=>'Thứ 2',
									'T3'	=>'Thứ 3',
									'T4'	=>'Thứ 4',
									'T5'	=>'Thứ 5',
									'T6'	=>'Thứ 6',
									'T7'	=>'Thứ 7',
								),
				'lop'	=>	$lop,
				'diem_hechu' => array(	'A+',
										'A',
										'B+',
										'B',
										'C+',
										'C',
										'D+',
										'D',
										'F'
				),
				'gioitinh'=> array(	'Nam'=>'Nam',
									'Nữ'=>'Nữ'
						),
				'langTable' => [
							    "sProcessing"	=>  "Đang xử lý...",
							    "sLengthMenu"	=>  "Xem _MENU_ mục",
							    "sZeroRecords"	=>  "Không tìm thấy dòng nào phù hợp",
							    "sInfo"			=>   "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
							    "sInfoEmpty"	=>   "Đang xem 0 đến 0 trong tổng số 0 mục",
							    "sInfoFiltered"	=> "(được lọc từ _MAX_ mục)",
							    "sInfoPostFix"	=>  "",
							    "sSearch"		=>  "Tìm:",
							    "sUrl"			=>  "",
							    "oPaginate"=> [
							        "sFirst"	=>  "Đầu",
							        "sPrevious"	=>  "Trước",
							        "sNext"		=>  "Tiếp",
							        "sLast"		=>  "Cuối"
							    ]
							],



			) ;



?>
