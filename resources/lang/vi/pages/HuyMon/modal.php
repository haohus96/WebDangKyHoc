<?php

/**
* Language file for user delete modal
*
*/
return array(

    'body'			=> 'Bạn có thực sự muốn hủy môn học này không ?',
    'cancel'		=> 'Thoát',
    'confirm'		=> 'Xóa',
    'title'         => 'Hủy môn học',

);
