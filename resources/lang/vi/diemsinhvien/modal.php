<?php

/**
* Language file for user delete modal
*
*/
return array(

    'body'			=> 'Bạn có thực sự muốn xóa không ?',
    'cancel'		=> 'Thoát',
    'confirm'		=> 'Xóa',
    'title'         => 'Xóa điểm sinh viên',

);
