
<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Trang chủ</a>
                        </li>
                        <li>
                            <a href="{{route('dangkymonhoc')}}"><i class="fa fa-tasks fa-fw"></i> Đăng ký học<span class="fa arrow"></span></a>

                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{route('dkmh_lichhoc')}}"><i class="fa fa-tasks fa-fw"></i> Lịch học<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="{{route('dkmh_xemdiem')}}"><i class="fa fa-tasks fa-fw"></i> Kết quả học tập<span class="fa arrow"></span></a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>