@extends('layouts/index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 align="center">Lịch học
                            <small>{{$namhoc['hoc_ky'] .' Năm học '.$namhoc['nam']}}</small>
                        </h3>
                    </div>
                    <!-- /.col-lg-12 --> 
                    
                    <div class="row form-group">
                        <div class="col-md-4">
                        <a data-toggle = "modal"  class="btn btn-primary" type="button" id="xacnhan" href="{{route('exportPDF',$namhoc['id'])}}">
                        In lịch học
                        </a>
                        </div>
                    </div> 
                     @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <!-- data-target="#confirm_selected" -->
                    <table class="table table-bordered table-hover" id="dataTables">
                        <thead>
                            <tr align="center">
                               
                                <th>Mã môn học</th>
                                <th>Tên môn học</th>
                               
                                <th>Số tín chỉ</th>
                                <th>Trạng thái</th>
                                <th>Học phí</th>
                                <th>Lớp học</th>
                                <th>Thứ</th>
                                <th>Tiết</th>
                                <th>Giảng đường</th>
                               <!--  <th>Hủy môn</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data_lophocsvdk as $data_lhsvdk)
                            <tr>
                                <td> {{$data_lhsvdk['ma_mh']}} </td>
                                <td>{{$data_lhsvdk['ten_mh']}}</td>
                                <td>{{$data_lhsvdk['so_tinchi']}}</td>
                                <td>{{$data_lhsvdk['trang_thai']}}</td>
                                <td>{{$data_lhsvdk['hoc_phi']}}</td>
                                <td>{{$data_lhsvdk['ma_mh'].' '.$data_lhsvdk['lop']}}</td>
                                <td>{{$data_lhsvdk['ngay_hoc']}}</td>
                                <td>{{$data_lhsvdk['tiet_hoc']}}</td>
                                <td>{{$data_lhsvdk['giang_duong']}}</td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="col-lg-12">
                        <h3 align="center">Thời khóa biểu
                            <small></small>
                        </h3>
                    </div>


                   
                    <table class="table table-bordered table-hover" id="data">
                        <thead>
                            <tr align="center">
                               
                                <th></th>
                                <th>Thứ 2</th>
                               <th>Thứ 3</th>
                               <th>Thứ 4</th>
                               <th>Thứ 5</th>
                               <th>Thứ 6</th>
                               <th>Thứ 7</th>
                                
                               <!--  <th>Hủy môn</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data_test as $key => $value)
                            <tr  align="center">
                              <td>{{'Tiết ' . $key}}</td>
                                @foreach($value as $data)
                                    <td>{{$data}}</td>
                                @endforeach
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div> 
@endsection('content')
@section('script')
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
       
    })
</script>
@endsection