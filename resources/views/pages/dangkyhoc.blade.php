@extends('layouts/index')
@section('header_style')
<style type="text/css" media="screen">
    input[type=checkbox] {
        transform: scale(1.2);
    }
    #active_0{
        background-color: #f9f3ab;
    }
</style>
@endsection
@section('content')
        <!-- Page Content -->
        <div id="page-wrapper">
            @if(count($data_lophoc) == 0)
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <small></small>
                        </h1>
                    </div>
                </div>
                <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                        </a>
                        Đang khóa đăng ký học
                </div>
                </div>
            @else

                
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <small>Danh sách môn học</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 --> 

                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif


                
                <div class="row form-group">
                        <div class="col-md-4">
                        <a data-toggle = "modal"  class="btn btn-primary" type="button" id="xacnhan">
                        Xác nhận
                        </a>
                        </div>
                </div> 
                    <!-- data-target="#confirm_selected" -->
                    <table class="table table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Chọn môn học</th>
                                <th>Mã môn học</th>
                                <th>Tên môn học</th>
                                <th>Lớp học</th>
                                <th>Số tín chỉ</th>
                                <th>Thứ</th>
                                <th>Tiết</th>
                                <th>Giảng đường</th>
                                <th>Số lượng mở lớp</th>
                                <th>Số lượng đăng ký</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($data_lophoc as $data)
                            <tr align="center" title="{{$data['tinh_trang']}}" id="{{'active_'.$data['active']}}">
                                @if($data['active'] == 0)
                                <td>
                                    <input type="checkbox" value="{{$data['id_lophoc']}}" 
                                    disabled="" >
                                </td>
                                @else
                                <td>
                                    <input type="checkbox" value="{{$data['id_lophoc']}}" 
                                   
                                    >
                                </td>
                                @endif
                                <td>{{$data['ma_mh']}}</td>
                                <td>{{$data['ten_mh']}}</td>
                                <td>{{ $data['ma_mh'].' '.$data['lop']}}</td>
                                <td>{{$data['so_tinchi']}}</td>
                                <td>{{$data['ngay_hoc']}}</td>
                                <td>{{$data['tiet_hoc']}}</td>
                                <td>{{$data['giang_duong']}}</td>
                                <td>{{$data['so_luong']}}</td>
                                <td>{{$data['soluong_dk']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>


            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <small>Danh sách môn học đã đăng ký</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 --> 

                    


                    
                    <!-- data-target="#confirm_selected" -->
                    <table class="table table-bordered table-hover" id="dataTables">
                        <thead>
                            <tr align="center">
                               
                                <th>Mã môn học</th>
                                <th>Tên môn học</th>
                               
                                <th>Số tín chỉ</th>
                                <th>Trạng thái</th>
                                <th>Học phí</th>
                                <th>Lớp học</th>
                                <th>Thứ</th>
                                <th>Tiết</th>
                                <th>Giảng đường</th>
                                <th>Hủy môn</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data_lophocsvdk as $data_lhsvdk)
                            <tr>
                                <td> {{$data_lhsvdk['ma_mh']}} </td>
                                <td>{{$data_lhsvdk['ten_mh']}}</td>
                                <td>{{$data_lhsvdk['so_tinchi']}}</td>
                                <td>{{$data_lhsvdk['trang_thai']}}</td>
                                <td>{{number_format($data_lhsvdk['hoc_phi'],2,',','.')}}</td>
                                <td>{{$data_lhsvdk['ma_mh'].' '.$data_lhsvdk['lop']}}</td>
                                <td>{{$data_lhsvdk['ngay_hoc']}}</td>
                                <td>{{$data_lhsvdk['tiet_hoc']}}</td>
                                <td>{{$data_lhsvdk['giang_duong']}}</td>
                                <td> 
                                    <a data-toggle="modal" data-target="#confirm_delete" title="Hủy" id="btn_huymon" href="{{route('confirmHuyMon',$data_lhsvdk['id_lophoc'])}}">
                                        <i class="fa fa-trash-o  fa-fw"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            @endif
        </div>
        <!-- /#page-wrapper -->
        <!-- Page Content -->
       
        <!-- /#page-wrapper -->
        <div class="modal fade" id="confirm_selected" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="user_delete_confirm_title">Đăng ký môn học</h4>
                    </div>
                    <div class="modal-body">
                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                     
                        <button type="button" class="btn btn-primary hidden" id="btn_dangky">Đăng ký</button>
                             
                    </div>

                </div>
            </div>
        </div> 
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    

                </div>
            </div>
        </div> 
       <br>
       <br>
       <br>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        
        var table = $('#dataTables-example').DataTable();
        var classSelected = [] ;
        
        $('input[type="checkbox"]').on('click',changeDataTable);
        
        table.on( 'draw', function () {
            $('input[type="checkbox"]').on('click',changeDataTable);
        });
        function changeDataTable(){
          
            var value = $(this).val() ;
            if($(this).is(':checked')){
                var inde = classSelected.indexOf(value) ;
                //console.log(inde) ;
                    if(inde == -1){
                        classSelected.push(value) ;
                    }
                //classSelected.push(value) ;
            }else{
                var index = classSelected.indexOf(value) ;
                if(index > -1){
                    classSelected.splice(index,1) ;
                }
            }
            console.log(classSelected) ;
            $.ajax({
                url:"{{route('dkmhAjax')}}",
                data:{'arr_idLopHoc[]':classSelected},
                success :function(data){
                    //console.log("data :") ;
                   // console.log(data) ;
                    //alert(data) ;
                    table.clear().draw() ;
                    $.each(data,function(key,value){
                        var node = table.row.add([
                                    value.checkbox,
                                    value.ma_mh,
                                    value.ten_mh,
                                    value.ma_mh + ' ' + value.lop,
                                    value.so_tinchi,
                                    value.ngay_hoc,
                                    value.tiet_hoc,
                                    value.giang_duong,
                                    value.so_luong,
                                    value.soluong_dk,
                                ]).draw(false).node() ;
                        $(node).attr('title',value.tinh_trang) ;
                        $(node).attr('align','center') ;

                        $(node).attr('id','active_'+value.active) ;
                        $(node).find('input[type="checkbox"]').on('click',changeDataTable) ;
                       
                    });
                },
            });
        }
        $('#xacnhan').on('click',function(){
            // var test_page = table.page.info() ;
            // console.log(test_page) ;
            var soluong = classSelected.length ;
            
            if(soluong == 0){
                var html = 'Bạn chưa chọn môn học nào vui lòng chọn môn học ! ' ;
                $('#confirm_selected').find('.modal-body').html(html) ;
                
                $('#confirm_selected').modal() ;
            
            }else{
                var html = 'Bạn đã chọn '+ soluong +' môn học. Bạn có thực sự muốn đăng ký không ?' ;
                $('#confirm_selected').find('.modal-body').html(html) ;
                $('#btn_dangky').removeClass('hidden') ;
                $('#confirm_selected').modal() ;
                console.log(classSelected) ;
                $('#btn_dangky').on('click',function(){
                    //alert("vào đây") ;
                    //$('#confirm_selected').modal("hide") ;
                    $.ajax({
                        url: "{{route('dangky_save')}}",
                        headers : {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        data:{'id_lophoc':classSelected},

                        success: function(response){
                            //console.log(response) ;
                            var html = '' ;
                            if(response == 'error'){
                                html = "Lỗi rồi không đăng ký được !" ;
                            }else{
                                html = 'Bạn đã đăng ký thành công '+ response +' môn học' ;
                            }
                            $('#confirm_selected').find('.modal-body').html(html) ;
                            $('#btn_dangky').addClass('hidden') ;
                            $('#confirm_selected').modal() ;
                            $('#confirm_selected').on('hidden.bs.modal',function(){
                                //alert("test") ;
                                window.location.href = "{{route('dangkymonhoc')}}" ;
                                //window.location.reload(true);
                            //alert(response) ;
                           });
                        },
                        error : function(){

                        }
                    }) ;
                });
            }

        }) ;
       
    }) ;
</script>
@endsection
