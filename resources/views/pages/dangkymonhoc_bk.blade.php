@extends('layouts/index')
@section('header_style')
<style type="text/css" media="screen">
    input[type=checkbox] {
  transform: scale(1.2);
}
</style>
@endsection
@section('content')
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <small>Danh sách môn học</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 --> 

                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif



                    <div class="row form-group">
                        <div class="col-md-4">
                        <a data-toggle = "modal"  class="btn btn-primary" type="button" id="xacnhan">
                        Xác nhận
                        </a>
                        </div>
                    </div> 
                    <!-- data-target="#confirm_selected" -->
                    <table class="table table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Chọn môn học</th>
                                <th>Mã môn học</th>
                                <th>Tên môn học</th>
                                <th>Lớp học</th>
                                <th>Số tín chỉ</th>
                                <th>Thứ</th>
                                <th>Tiết</th>
                                <th>Giảng đường</th>
                                <th>Số lượng mở lớp</th>
                                <th>Số lượng đăng ký</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($data_lophoc as $data)
                            <tr align="center" title="{{$data['tinh_trang']}}" id = "{{'id_'.$data['id_lophoc']}}">
                               <!--  if($data['active'] == 0)
                                <td><input type="checkbox" value="{{$data['id_lophoc']}}" disabled=""></td>
                                else -->
                                <td><input type="checkbox" value="{{$data['id_lophoc']}}"></td>
                               <!--  endif -->
                                <td>{{$data['ma_mh']}}</td>
                                <td>{{$data['ten_mh']}}</td>
                                <td>{{ $data['ma_mh'].' '.$data['lop']}}</td>
                                <td>{{$data['so_tinchi']}}</td>
                                <td>{{$data['ngay_hoc']}}</td>
                                <td>{{$data['tiet_hoc']}}</td>
                                <td>{{$data['giang_duong']}}</td>
                                <td>{{$data['so_luong']}}</td>
                                <td>{{$data['soluong_dk']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
        </div>
        <!-- /#page-wrapper -->
        <!-- Page Content -->
       
        <!-- /#page-wrapper -->
         <div class="modal fade" id="confirm_selected" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="user_delete_confirm_title">Đăng ký môn học</h4>
                    </div>
                    <div class="modal-body">
                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                     
                        <a type="button" class="btn btn-danger">Đăng ký</a>
                             
                    </div>

                </div>
            </div>
        </div> 
       
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        var classSelected = [] ;
        var soluong = 0 ;
        $('input[type="checkbox"]').on('click',function(){
            soluong = $('input[type="checkbox"]:checked').length ;
            var value = $(this).val() ;
            if($(this).is(':checked')){
                
                $('#id_'+value).attr('title','test') ;
                //$(this).attr('disabled','');
                classSelected.push(value) ;
            }else{ ;
               // alert("vao day") ;
                var index = arr.indexOf(value) ;
                if(index > -1){
                    //alert("vao day 2") ;
                    classSelected.splice(index,1) ;
                }
            }
            
            
        });
        $('#xacnhan').on('click',function(){
            var html = 'Bạn đã chọn '+ soluong +' môn học. Bạn có thực sự muốn đăng ký không ?' ;
            $('#confirm_selected').find('.modal-body').html(html) ;
            $('#confirm_selected').modal() ;
        }) ;
        // $('#idButton').on('click',function(){
        //     // for(i = 0;i<arr.length;i++){
        //     //     alert(arr[i]) ;
        //     // }
        //   //  $('table tbody tr:first td:nth-child(1)').html("test html") ;
        //     var b = "1111test" ;
        //     var urlhref  = '{{route("testAjax")}}'+'?id='+b+'&name='+b;
        //     //+b +"67890";
        //     //window.location.href =  urlhref ;

        //     //alert(urlhref) ;
        //     // $('input[type="checkbox"]').each(function(){
        //     //    //if($(this).is(''))
        //     //   // $(this).removeAttr('checked') ;
        //     //   $(this).attr('disabled',"") ;
        //     // });
        // });
        // $('table tbody tr').hover(
        //     function(){
        //   //  if($(this).is(':hover')) {
        //         var a = $(this).find('input[type="checkbox"]').val() ;
        //         console.log("test tr hover " + a) ;
        //    // }
                    
        //     },function(){

        //     }
        // ) ;
        // $('table tbody tr').on('mouseover',function(){
        //     alert("test");
        // }) ;

         // var arr_checkbox = document.querySelectorAll('input[type="checkbox"]') ;
            // for(i = 0 ; i < arr_checkbox.length ; i++){
            //     var value = arr_checkbox[i].value ;
            //     if(arr_checkbox[i].checked){
                
            //         var index = classSelected.indexOf(value) ;
            //         if(index == -1){
                       
            //             classSelected.push(value) ;
            //         }
                   
            //     }else{ ;
                   
            //         var index = classSelected.indexOf(value) ;
            //         if(index > -1){
            //             //alert("vao day 2") ;
            //             classSelected.splice(index,1) ;
            //         }
            //     }
            // }
    }) ;
</script>
@endsection
