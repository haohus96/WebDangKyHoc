@extends('layouts/index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper" >
            <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header" align="center">KẾT QUẢ HỌC TẬP
                            <small></small>
                        </h3>
                    </div>
                    <!-- /.col-lg-12 -->

                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
            <div class="container-fluid" style="overflow-y: scroll;max-height: 500px">
        
                    <table class="table  table-bordered table-hover" id="dataTable">
                        <thead>
                            <tr align="center">
                                <th>STT</th>
                                <th align="center">Mã môn học</th>
                                <th>Tên môn học</th>
                                <th>Số tín chỉ</th>
                                <th>Điểm hệ số 10</th>
                                <th>Điểm hệ số 4</th>
                                <th>Điểm hệ chữ</th>
                              
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach($diemsv as $key => $value)
                                <tr  style="font-size: 15px; font-weight: bold;">
                                    <td align="left" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;{{$key}}
                                    </td>
                                    <td colspan="3"></td>
                                   
                                </tr>
                               
                                @foreach($value as $ke =>$diem)
                                <tr align="center">
                                    <td>{{$ke+1}}</td>
                                    <td>{{$diem['ma_mh']}}</td>
                                    <td>{{$diem['ten_mh']}}</td>
                                    <td>{{$diem['so_tinchi']}}</td>
                                    <td>{{$diem['diem_heso1']}}</td>
                                    <td>{{$diem['diem_heso2']}}</td>
                                    <td>{{$diem['diem_chu']}}</td>
                                   
                                </tr>
                                @endforeach
                            @endforeach
                          
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

            <br>
           
            <div class="row">
                <label>Tổng tín chỉ: {{$tong_tinchi}}</label>
                <br>
                <label>Tổng tín chỉ tích lũy: {{$tong_tinchitl}}</label>
                <br>
                <label>Điểm trung bình tích lũy hệ 4: {{$diem_tb}}</label>
            </div>
             <br>
            <br> 
            <br>
            <br>
        </div>
        <!-- /#page-wrapper -->
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div> 
           <br>
            <br>
            <<br>
@endsection('content')
@section('script')
<script type="text/javascript" charset="utf-8" >
    $(document).ready(function(){
         // $('#dataTable').DataTable({
         //    "order" : [] ,
         //    "searching" : false,
         //    //"lengthChange": false,
         //    "scrollY":        "200px",
         //     "scrollCollapse": true,
         //    "paging":         false

         // });
    }) 
</script>
@endsection