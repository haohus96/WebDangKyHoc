
<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li>
                            <a><i class="fa fa-dashboard fa-fw"></i> Trang chủ</a>
                        </li>
                        <li>
                            <a><i class="fa fa-delicious fa-fw"></i> Ngành học<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('chuyennganh_list')}}">Danh sách</a>
                                </li>
                                <li>
                                    <a href="{{route('chuyennganh_create_get')}}">Thêm mới</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a><i class="fa fa-delicious fa-fw"></i> Năm học<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('namhoc_list')}}">Danh sách</a>
                                </li>
                                <li>
                                    <a href="{{route('namhoc_create_get')}}">Thêm mới</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a><i class="fa fa-tasks fa-fw"></i> Sinh viên<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('sinhvien_list')}}">Danh sách</a>
                                </li>
                                <li>
                                    <a href="{{route('sinhvien_create_get')}}">Thêm</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a><i class="fa fa-navicon fa-fw"></i> Môn học<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('monhoc_list')}}">Danh sách</a>
                                </li>
                                <li>
                                    <a href="{{route('monhoc_create')}}">Thêm mới</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a><i class="fa fa-bar-chart-o fa-fw"></i> Lịch học<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('lichhoc_list')}}">Danh sách</a>
                                </li>
                                <li>
                                    <a href="{{route('lichhoc_create_get')}}">Thêm</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a><i class="fa fa-cube fa-fw"></i> Lớp học<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('lophoc_list')}}">Danh sách</a>
                                </li>
                                <li>
                                    <a href="{{route('lophoc_create_get')}}">Thêm mới</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a><i class="fa fa-list-alt fa-fw"></i> Điểm sinh viên<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('diemsv_list')}}">Danh sách</a>
                                </li>
                                <li>
                                    <a href="{{route('diemsv_create_get')}}">Thêm mới</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Tài khoản<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('user_list')}}">Danh sách</a>
                                </li>
                                <li>
                                    <a href="{{route('user_create_get')}}">Thêm tài khoản quản trị</a>
                                </li>
                                <li>
                                    <a href="{{route('user_create_sv_get')}}">Thêm tài khoản cho sinh viên</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>