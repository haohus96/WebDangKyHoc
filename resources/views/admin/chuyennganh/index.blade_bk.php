@extends('admin/layout/index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Chuyên ngành
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="row form-group">
                        <div class="col-md-10">
                            <a href="{{route('chuyennganh_create_get')}}" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-plus"></span> Thêm mới</a>
                        </div>
                    </div>

                    <!-- /.col-lg-12 -->
                    <table class="table table-bordered table-hover" id="idTable">
                        <thead>
                            <tr style="text-align: center;">
                                <th>&nbsp;Mã ngành</th>
                                <th>&nbsp;Tên ngành</th>
                                
                                <th>&nbsp;Hành động</th>
                                <!-- <th>Edit</th> -->
                            </tr>
                        </thead>
                        <tbody>
                           <!--  @foreach($chuyennganh as $key => $cn)
                            <tr >
                                <td>&nbsp;&nbsp; {{$cn->ma_nganh}}</td>
                                <td>&nbsp;&nbsp; {{$cn->ten_nganh}}</td>
                                
                                <td >&nbsp;&nbsp;
                                    <a href="{{route('chuyennganh_confirmDelete',$cn->ma_nganh)}}" title="xóa" data-toggle="modal" data-target="#confirm_delete">
                                        <i class="fa fa-trash-o  fa-fw"></i>
                                    </a>
                                    <a href="{{route('chuyennganh_edit_get',$cn->ma_nganh)}}" title="sửa"><i class="fa fa-pencil fa-fw"></i> </a>
                                </td>
                            </tr>
                            @endforeach -->
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div> 
@endsection('content')
@section('script')
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        var table = $('#idTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('chuyennganh_data') !!}',
            columns: [
                { data: 'ma_nganh', name: 'ma_nganh' },
                { data: 'ten_nganh', name: 'ten_nganh' },
                { data: 'ten_nganh', name: 'ten_nganh' },
              
               // { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ]
        });
    })
</script>
@endsection