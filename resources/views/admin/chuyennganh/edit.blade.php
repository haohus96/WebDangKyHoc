@extends('admin/layout/index')
@section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Chuyên ngành
                            <small>Sửa</small>
                        </h1>
                    </div>

                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{route('chuyennganh_edit_post',$nganhhoc->ma_nganh)}}" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="ma_nganhOld" 
                            value="{{$nganhhoc->ma_nganh}}">
                            <div class="form-group">
                                <label>Mã ngành</label>
                                <input class="form-control" name="ma_nganh" placeholder="" value="{{$nganhhoc->ma_nganh}}" />
                            </div>
                            <div class="form-group">
                                <label>Tên ngành</label>
                                <input class="form-control" name="ten_nganh" placeholder="" value="{{$nganhhoc->ten_nganh}}" />
                            </div>
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection

@section('script')
@endsection