@extends('admin/layout/index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Chuyên ngành
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="row form-group">
                        <div class="col-md-10">
                            <a href="{{route('chuyennganh_create_get')}}" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-plus"></span> Thêm mới</a>
                        </div>
                    </div>

                    <!-- /.col-lg-12 -->
                    <div style="overflow-x: scroll;">
                    <table class="table table-bordered table-hover" id="idTable">
                        <thead>
                            <tr style="text-align: center;">
                                <th>&nbsp;Mã ngành</th>
                                <th>&nbsp;Tên ngành</th>
                                
                                <th>&nbsp;Hành động</th>
                                <!-- <th>Edit</th> -->
                            </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                    </table>
                </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <br>
        <br>
        <!-- /#page-wrapper -->
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div> 
@endsection('content')
@section('script')
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        var table = $('#idTable').DataTable({
            // processing: true,
            // serverSide: true,
            // ajax: '{!! route('chuyennganh_data') !!}',
            // dataSrc: "",
            // columns: [
            //     { 'data': 'ma_nganh'},
            //     { 'data': 'ten_nganh'},
            //     { 'data': 'ten_nganh'},
            //    // { data: 'ma_nganh', name: 'ma_nganh' },
            //     // { data: 'ten_nganh', name: 'ten_nganh' },
            //     // { data: 'ten_nganh', name: 'ten_nganh' },
            //    // { data: 'actions', name: 'actions', orderable: false, searchable: false }
            // ]
            "processing": true,
            "ajax": {
            "url": "{!! route('chuyennganh_data') !!}",
            "dataSrc": ""
            },
            "columns": [
                { 'data': 'ma_nganh'},
                { 'data': 'ten_nganh'},
                { 'data': 'actions'},
            ],
            "createdRow": function( row, data, dataIndex ) {
                $(row).find("td:eq(2)").attr("align","center") ;
            },
            'columnDefs': [
            {
                'targets': 2,
                'createdCell':  function (td, cellData, rowData, row, col) {
                    //$(td).attr('id', 'otherID'); 
                }
            }
            ],
        });
    })
</script>
@endsection