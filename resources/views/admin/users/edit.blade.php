@extends('admin/layout/index')
@section('content')
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tài khoản
                            <small>Sửa</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->

                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{route('user_edit_post',$user->id)}}" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            
                            
                               <!--  class="hidden" -->
                               <div class="form-group">
                                    <label>Họ tên </label>
                                    <input class="form-control" name="name" placeholder="" value="{{$user->name}}" />

                                </div>
                                <div class="form-group">
                                    <label>Tên đăng nhập</label>
                                    <input class="form-control" name="username" placeholder="" value="{{$user->username}}"/>

                                </div>
                               
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" placeholder="" value="{{$user->email}}"/>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" name="changePassword" id="changePassword">
                                    <label>Đổi mật khẩu</label>
                                    <input type="password" class="form-control password" aria-describedby="basic-addon1" disabled="" id="password" name="password">
                                </div>
                                <!-- <br> -->
                                <div class="form-group">
                                    <label>Nhập lại mật khẩu</label>
                                    <input type="password" class="form-control password" name="passwordAgain" aria-describedby="basic-addon1" disabled="">
                                </div>
                            <button type="submit" class="btn btn-default">Sửa </button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection
@section('script')
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $('#changePassword').on('click',function(){
            if($(this).is(':checked')){
                $('.password').removeAttr('disabled') ;
            }else{
                $('.password').attr('disabled','') ;
            }
        })
    })
</script>
@endsection