@extends('admin/layout/index')
@section('content')
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tài khoản
                            <small>Thêm tài khoản cho sinh viên</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->

                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    @if(count($sinhvien) == 0)
                    <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                           Các sinh viên đều đã có tài khoản
                        </div>
                    @else
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{route('user_create_sv_post')}}" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            
                               <!--  class="hidden" -->
                               <div class="form-group">
                                    <label>Tên đăng nhập</label>
                                    <select name="username" class="form-control" id="username">
                                        @foreach($sinhvien as $sv)
                                        <option value="{{$sv->masv}}">{{$sv->masv}}</option>
                                        @endforeach
                                    </select>

                                </div>
                               <div class="form-group">
                                    <label>Họ tên sinh viên</label>
                                    @if(count($sinhvien) >0)
                                    <input class="form-control" name="name" id="name" value="{{$sinhvien[0]->hoten}}" disabled  />
                                    @else
                                    <input class="form-control" name="name" id="name" value="" disabled  />
                                    @endif
                                </div>
                                <!--  -->
                                
                                <div class="form-group">
                                    <label>Email</label>
                                    @if(count($sinhvien) >0)
                                    <input type="email" class="form-control" name="email" id="email" disabled value="{{$sinhvien[0]->email}}" />
                                    @else
                                    <input type="email" class="form-control" name="email" id="email" disabled value="" />
                                    @endif
                                </div>
                            
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                    @endif
                    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection
@section('script')
<script type="text/javascript" charset="utf-8" >
    $(document).ready(function(){
        var sinhvien = <?php echo json_encode($sinhvien) ;?> ;
        $('#username').on('change',function(){
            var username = $(this).val() ;
            $.each(sinhvien,function(key,value){
                if(username == value.masv){
                    $('#name').val(value.hoten) ;
                    $('#email').val(value.email) ;
                    return false ;
                }
            });
        });
    })
</script>
@endsection