@extends('admin/layout/index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tài khoản
                            <small>Danh sách</small>
                        </h1>
                    </div>


                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif


                    <div class="row form-group">
                        <div class="col-md-4">
                        <a data-toggle = "modal"  class="btn btn-primary" type="button" title="Thêm tài khoản cho mọi sinh viên" data-target="#thongbao">
                        Thêm tài khoản
                        </a>
                        </div>
                    </div> 
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tên người dùng</th>
                                <th>Tên đăng nhập</th>
                                <th>email</th>
                                <th>Quyền</th>
                                <th>Hành động</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td align="center">{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->username}}</td>
                                <td>{{$user->email}}</td>
                                @if($user->level ==1)
                                <td>Admin</td>
                                @else
                                <td>Thường</td>
                                @endif
                                <td align="center">
                                    @if($user->level ==0)
                                    <a title="Xóa" data-toggle="modal" data-target="#confirm_delete" href="{{route('user_confirmDelete',$user->id)}}"> 
                                        <i class="fa fa-trash-o  fa-fw"></i>
                                    </a>
                                    @endif
                                    @if($user->username == Auth::user()->username)
                                    <a title="Sửa" href="{{route('user_edit_get',$user->id)}}">
                                        <i class="fa fa-pencil fa-fw"></i>  
                                    </a>
                                    @endif
                                </td>
                                
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div> 
         <div class="modal fade" id="thongbao" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="user_delete_confirm_title">
                            Thêm tài khoản cho tất cả sinh viên
                        </h4>
                    </div>
                    <div class="modal-body">
                       Bạn có thực sự muốn thêm tài khoản cho mọi sinh viên không ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                     
                        <a type="button" class="btn btn-primary" id="btn_update" href="{{route('user_createAllSinhVien')}}">Thực hiện</a>
                             
                    </div>

                </div>
            </div>
        </div> 
@endsection('content')