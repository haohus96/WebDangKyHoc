@extends('admin/layout/index')
@section('content')
	<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Điểm sinh viên
                            <small>Thêm mới</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->

                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{route('diemsv_create_post')}}" method="POST">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Năm học</label>
                                <select name="namhoc" class="form-control">
                                	@foreach($namhoc as $key => $nh)
                                	<option value="{{$key}}">{{$nh}}</option>
                                	@endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Mã sinh viên</label>
                                <select name="ma_sv" class="form-control" id="ma_sv">
                                	@foreach($sinhvien as $sv)
                                	<option value="{{$sv->masv}}">{{$sv->masv}}</option>
                                	@endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tên sinh viên</label>
                                <input class="form-control" name="hoten" placeholder="" disabled="" value="{{$sinhvien[0]->hoten}}" id="ten_sv" />
                            </div>
                            <div class="form-group">
                                <label>Mã môn học</label>
                                <select name="ma_mh" class="form-control" id="ma_mh">
                                	@foreach($monhoc as $mh)
                                	<option value="{{$mh->ma_mh}}">{{$mh->ma_mh}}</option>
                                	@endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tên môn học</label>
                                <input class="form-control" name="ten_mh" placeholder="" disabled="" value="{{$monhoc[0]->ten_mh}}" id="ten_mh"/>
                            </div>
                            <div class="form-group">
                                <label>Điểm hệ số 10</label>
                                <input class="form-control" name="diem_heso1" placeholder="" />
                            </div>
                            
                            <div class="form-group">
                                <label>Điểm hệ số 4</label>
                                <input class="form-control" name="diem_heso2" placeholder="" />
                            </div>
                            <div class="form-group">
                                <label>Điểm hệ chữ</label>
                                <select name="diem_hechu" class="form-control">
                                	@foreach($diem_hechu as $diem)
                                	<option value="{{$diem}}">{{$diem}}</option>
                                	@endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection
@section('script')
<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		var sinhvien = <?php echo json_encode($sinhvien) ; ?> ;
		var monhoc = <?php echo json_encode($monhoc) ; ?> ;
		$('#ma_sv').on('change',function(){
			console.log(sinhvien) ;
			var ma_sv = $(this).val() ;
			$.each(sinhvien,function(key,value){
				//alert(value.masv) ;
				if(ma_sv == value.masv){
					$('#ten_sv').val(value.hoten) ;
					//break ;
					return false ;
				}
				//alert(value.masv) ;
			});
		}) ;
		$('#ma_mh').on('change',function(){
			var ma_mh = $(this).val() ;
			$.each(monhoc,function(key,value){
				if(ma_mh == value.ma_mh){
					$('#ten_mh').val(value.ten_mh) ;
					return false ;
				}
			})
		});
	})
</script>
@endsection