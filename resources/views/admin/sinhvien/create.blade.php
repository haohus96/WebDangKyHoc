@extends('admin/layout/index')
@section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sinh viên
                            <small>Thêm</small>
                        </h1>
                    </div>
                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{route('sinhvien_create_post')}}" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Mã sinh viên</label>
                                <input class="form-control" name="ma_sv" placeholder="" />
                            </div>
                            <div class="form-group">
                                <label>Họ tên</label>
                                <input class="form-control" name="hoten" placeholder="" />
                               <!--  {!! $errors->first('hoten', '<span class="help-block">:message</span>') !!} -->
                            </div>
                            
                            <div class="form-group">
                                <label>Ngày sinh</label>
                                <input class="form-control" name="ngaysinh" placeholder="dd-mm-yyyy" data-date-format="DD-MM-YYYY" id="ngaysinh" type="text" />
                            </div>
                            <div class="form-group">
                                <label>Nơi sinh</label>
                                <input class="form-control" name="noisinh" placeholder="" />
                            </div>
                            <div class="form-group">
                                <label>Giới tính</label>
                              
                                {!! Form::select('gioitinh',$gioitinh,null,
                                ['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label>Ngành học</label>
                                {!! Form::select('chuyennganh',$chuyennganh,null,['class'=>'form-control']) !!}
                            </div>
                            
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="" />
                            </div>
                            <button type="submit" class="btn btn-default">Thêm </button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection

 @section('script')
  <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
  <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

 <script>
    $(document).ready(function(){
       $('#ngaysinh').datetimepicker(); 
    });
</script>
 @endsection