@extends('admin/layout/index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh sách điểm
                            <small>{{$sinhvien->hoten . ' '.$sinhvien->masv}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->

                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif

                     <div class="row form-group">
                        <div class="col-md-10">
                        <a class="btn btn-primary" href="{{route('sinhvien_createDiem',$sinhvien->masv)}}">Thêm mới</a>
                        </div>
                    </div>  
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Năm học</th>
                                <th>Mã môn học</th>
                                <th>Tên môn học</th>
                                <th>Số tín chỉ</th>
                                <th>Điểm hệ số 10</th>
                                <th>Điểm hệ số 4</th>
                                <th>Điểm hệ chữ</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($diemsv as $diem)
                            <tr>
                                <td>{{$diem->hoc_ky .' Năm học '.$diem->nam}}</td>
                                <td>{{$diem->ma_mh}}</td>
                                <td>{{$diem->ten_mh}}</td>
                                <td>{{$diem->so_tinchi}}</td>
                                <td>{{$diem->diem_heso1}}</td>
                                <td>{{$diem->diem_heso2}}</td>
                                <td>{{$diem->diem_chu}}</td>
                               
                                <td align="center">
                                   
                                    <a href="{{route('diemsv_confirmDelete',$diem->id)}}" 
                                        title="Xóa" data-toggle="modal" data-target = "#confirm_delete">
                                        <i class="fa fa-trash-o  fa-fw"></i>
                                    </a>
                                    <a href="{{route('diemsv_edit_get',$diem->id)}}"><i class="fa fa-pencil fa-fw"></i></a>
                                
                                </td>
                                
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div> 
@endsection('content')