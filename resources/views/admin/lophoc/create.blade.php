@extends('admin/layout/index')
@section('header_style')
<link type="text/css" href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" />  
 <link href="{{ asset('assets/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />    
 <!-- <link href="{{ asset('assets/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css" /> -->
 <link rel="stylesheet" type="text/css" href="{{ asset('tagit/css/jquery.tagit.css') }}" />  
@endsection
@section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lớp học
                            <small>Thêm</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->

                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{route('lophoc_create_post')}}" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" id="_token" >
                            <div class="form-group">
                                <label>Học kỳ</label>
                                {!! Form::select('namhoc',$namhoc,null,['class'=>'form-control','id'=>'namhoc']) !!}
                            </div>
                            <div class="form-group">
                                <label>Chọn môn học</label>
                                <select name="ma_mh" class="form-control" id="ma_mh">
                                    @foreach($monhoc as $mh)
                                    <option 
                                    value="{{$mh->ma_mh}}">{{$mh->ma_mh . ' - ' . $mh->ten_mh}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- <div class="form-group">
                                <label>Tên môn học</label>
                                <input class="form-control" name="ten_mh" placeholder="" disabled="" value="monhoc[0]->ten_mh" id="ten_mh" />
                            </div> -->
                            <div class="form-group">
                                <label>Lớp</label>
                                {!! Form::select('lop',$lop,null,['class'=>'form-control','id'=>'lop']) !!}
                            </div>
                            
                            <div class="row form-group">
                                
                                <div class="col-md-4">
                                    <label>Số lượng</label>
                                    <input class="form-control" name="soluong" placeholder="" />  
                                </div>
                                
                            </div>
                            
                            
                            <div class="form-group">
                                    <label>Lịch học</label>
                                    <br>
                                    {!! Form::select('lichhoc[]',$lichhoc,null,['class'=>'form-control','id'=>'lichhoc','multiple'=>'multiple']) !!}
                            </div>
                           <!--  <div class="form-group"> -->
                                
                                <!-- <div class="col-md-4"> -->
                                    <!-- <label>tags</label>
                                    <input class="form-control" name="tags" placeholder="" id="tags" />   -->
                                <!-- </div> -->
                                
                           <!--  </div> -->
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

        <!-- /#page-wrapper -->
@endsection

@section('script')
<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" ></script>
<script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
 
 <!-- //tag-it.min -->
 <script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
<script src="{{ asset('assets/js/tag-it.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        function sortObject(obj) {
            return Object.keys(obj)
            .sort().reduce((a, v) => {
            a[v] = obj[v];
            return a; }, {});
        }
        $('#ma_mh').select2({
            tags : true ,
        }) ;
        $('#lichhoc').multiselect({
            enableFiltering: true,
            includeSelectAllOption: false,
            maxHeight:200,
            dropUp: true,
            buttonWidth: 250,
            nonSelectedText: 'Chưa chọn lịch học',
           // nSelectedText: '',


        });
        $('#ma_mh,#namhoc').on('change',function(){
           // alert("test") ;
            var ma_mh = $('#ma_mh').val() ;
            var id_namhoc = $('#namhoc').val() ;
            $.ajax({
                url:"{{route('ajax_getTenmhByMamh')}}",
                headers : {
                    'X-CSRF-TOKEN': $('#_token').val()
                },
                data:{'ma_mh':ma_mh ,'id_namhoc':id_namhoc},
                success:function(response){
                    //$('#ten_mh').val(response.ten_mh) ;
                    var arr_lop = response.lop ;
                    var html = '' ;
                   // console.log(arr_lop) ;
                    $.each(arr_lop,function(key,value){
                        //alert(key) ;
                        html = html+'<option value="'+ value +'">'+ value + '</option>' ;
                       
                    });
                    $('#lop').html(html) ;
                    //alert(html) ;
                    var arr_lichhoc = response.lichhoc ;
                  //  console.log(arr_lichhoc) ;
                    var option = '' ;
                    $('.multiselect').multiselect('destroy');
                    $.each(arr_lichhoc,function(index,data){
                        option = option + '<option value="'+ index +'">'+ data + '</option>' ;
                    });
                    $('#lichhoc').html(option) ;
                    
                    $('#lichhoc').multiselect('rebuild');
                   // $('select').multiselect('refresh');
                },
            });
        });
        // var a = ['111','2222'] ;
        //  $('#tags').tagit({
        //     autocomplete: {delay: 0, minLength: 1},
        //  });

    });
</script>
@endsection