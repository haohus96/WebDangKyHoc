@extends('admin/layout/index')
@section('content')
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12"> 
                        <h3 class="page-header">{{$lophoc->hoc_ky.' Năm học '.$lophoc->nam}}
                            <small>Lớp học {{$lophoc->ma_mh.' '.$lophoc->lop}}</small>
                            <br>
                            <small align="center">Danh sách sinh viên đã đăng ký học</small>
                        </h3>
                    </div>
                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <!-- /.col-lg-12 -->
                   <!--  <div class="row form-group">
                        <div class="col-md-10">
                        <a class="btn btn-primary">Thêm mới</a>
                        </div>
                    </div> -->  
                    
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã sinh viên</th>
                                <th>Tên</th>
                               
                                <th>Ngày sinh</th>
                                <th>Nơi sinh</th>
                                <th>Ngành học</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sinhvien as $key => $sv)
                            <tr>
                                <td>{{$sv->masv}}</td>
                                <td>{{$sv->hoten}}</td>
                                
                                <td>{{Carbon\Carbon::parse($sv->ngaysinh)->format('d-m-Y')}}
                                </td>
                                <td>{{$sv->noisinh}}</td>
                                <td>{{$sv->getChuyenNganh->ten_nganh}}</td>
                                <td>{{$sv->trang_thai}}</td>
                                <td align="center">
                                    <a data-toggle="modal" data-target="#confirm_delete"
                                     href="{{route('sinhvien_confirmDelete',$sv->masv)}}">
                                        <i class="fa fa-trash-o fa-fw"></i>
                                    </a>
                                    
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            
        </div>

        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div> 
        <!-- /#page-wrapper -->
        <!-- Page Content -->
       
        <!-- /#page-wrapper -->
      
@endsection('content')
@section('script')
<script type="text/javascript">
    
</script>
@endsection