@extends('admin/layout/index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lớp học
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->


                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="row form-group">
                        <div class="col-md-2">
                        <a class="btn btn-primary" href="{{route('lophoc_create_get')}}">Thêm mới</a>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control" id="searchByNam">
                             <option value="">
                                   Chọn năm học
                            </option>
                            @foreach($namhoc2 as $key => $value)
                                <option value="{{$value->id}}">
                                    {!! $value->hoc_ky .' Năm học '. $value->nam !!}
                                </option>
                              
                           @endforeach
                        </select>
                        </div>
                        <div class="col-md-3" ></div>
                        <div class="col-md-2" >

                            <a class="btn btn-default" data-toggle="modal" 
                            data-target="#confirm_dongdk" >
                            Đóng đăng ký học
                            </a>
                        </div>
                         <div class="col-md-2" >
                            <a  class="btn btn-info" data-toggle="modal" 
                            data-target="#confirm_modk">
                            Mở đăng ký học
                            </a>
                        </div>
                    </div> 
                    <br>
                    <table class="table table-striped table-bordered table-hover" id="dataTable">
                        <thead>
                            <tr align="center">
                                <th>Năm học</th>
                                <th>Lớp môn học</th>
                                <th>Mã môn học</th>
                                <th>Môn học</th>
                                <th>Số tín chỉ</th>
                                <th>Số lượng mở lớp</th>
                                <th>Số lượng đăng ký</th>
                                <th>Thứ</th>
                                <th>Tiết</th>
                                <th>Giảng đường</th>
                                <th>Hành động</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($lophoc as $key =>$lh)
                            <?php $nam_hoc = $lh->getNamHoc ?>
                            <tr title="">
                                <td>{{$nam_hoc->hoc_ky.' Năm học '.$nam_hoc->nam}}</td>
                                <td>{{$lh->ma_mh.' '.$lh->lop}}</td>
                                <td>{{$lh->ma_mh}}</td>
                                <td>{{$lh->getMonHoc->ten_mh}}</td>
                                 <td>{{$lh->getMonHoc->so_tinchi}}</td>
                                <td>{{$lh->so_luong}}</td>
                                <td>{{$lh->soluong_dk}}</td>
                                <td>{{$lh->ngay_hoc}}</td>
                                <td>{{$lh->tiet_hoc}}</td>
                                <td>{{$lh->giang_duong}}</td>

                                <td align="center">
                                    <a href="{{route('lophoc_confirmDelete',$lh->id)}}" title="Xóa" data-toggle="modal" data-target="#confirm_delete"> 
                                        <i class="fa fa-trash-o  fa-fw"></i>
                                    </a>
                                    <a title="Sửa" href="{{route('lophoc_edit_get',$lh->id)}}"> <i class="fa fa-pencil fa-fw"></i> </a>
                                    <a title="Xem danh dách sinh viên đã đăng ký học" href="{{route('lophoc_ds_sinhvienDK',$lh->id)}}"> 
                                        <i class="fa fa-delicious fa-fw"> 
                                        </i>
                                    </a>
                                    <a title="Xuất danh dách sinh viên đã đăng ký học" href="{{route('lophoc_export_dsSinhVien',$lh->id)}}"> 
                                        <i class="fa fa fa-file-pdf-o fa-fw">
                                            
                                        </i> 
                                    </a>
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div> 
        <div class="modal fade" id="confirm_dongdk" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="user_delete_confirm_title">Đăng ký môn học</h4>
                    </div>
                    <div class="modal-body">
                       Bạn có thực sự muốn đóng đăng ký môn học ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                     
                        <a type="button" class="btn btn-primary" id="btn_dangky" href="{{route('lophoc_dongdkHoc')}}">Thực hiện</a>
                             
                    </div>

                </div>
            </div>
        </div> 
        <div class="modal fade" id="confirm_modk" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title" id="user_delete_confirm_title">Mở đăng ký môn học</h4>
                    </div>
                    <div class="modal-body">
                       <div class="form-group">
                        <select class="form-control" id="id_nam">
                             <option value="">
                                   Chọn năm học
                            </option>
                            @foreach($namhoc as $key => $value)
                                <option value="{{$value->id}}">
                                    {!! $value->hoc_ky .' Năm học '. $value->nam !!}
                                </option>
                              
                           @endforeach
                        </select>
                       </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                     
                        <a type="button" class="btn btn-primary" id="btn_modk">Thực hiện</a>
                             
                    </div>

                </div>
            </div>
        </div> 
@endsection('content')
@section('script')
<script type="text/javascript" >
    $(document).ready(function(){
        
        var lang = <?php echo json_encode(__('langTable')) ; ?> ;
        var idTable = $('#dataTable').DataTable({
            //"searching" : false,
            //"lengthChange": false,
            // "scrollY":        "200px",
            // "scrollCollapse": true,
            // "paging":         false
            "language" : lang
        }) ;
        $('#btn_modk').on('click',function(){
            var id_nam = $('#id_nam').val() ;
          //  console.log(id_nam) ;
            $.ajax({
                url:"{{route('lophoc_modkHoc')}}",
                headers : {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                data:{'id_nam':id_nam},
                success:function(){
                    $('#confirm_modk').modal('hide') ;
                },
            })
        }) ;
        // searchLopHocByNamHoc
        $('#searchByNam').on('change',function(){
            var nam_hoc = $(this).val() ;
            $.ajax({
                url:"{{route('lophoc_searchLopHocByNamHoc')}}",
                headers : {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                data:{'nam_hoc':nam_hoc},
                success:function(response){
                    //$('#confirm_modk').modal('hide') ;
                   //if(response != 1){
                        var a = "111";
                        //alert(response) ;
                       
                        idTable.clear().draw() ;
                        $.each(response,function(key,value){
                            idTable.row.add([
                                value.hoc_ky +' Năm học '+value.nam,
                                value.ma_mh + ' '+value.lop,
                                value.ma_mh,
                                value.ten_mh,
                                value.so_tinchi,
                                value.so_luong,
                                value.soluong_dk,
                                value.ngay_hoc,
                                value.tiet_hoc,
                                value.giang_duong,
                                value.option
                            ]).draw(false) ;

                        }) ;
                   //}
                },
            }) ;
        }) ;
    }) ;
</script>
@endsection