@extends('admin/layout/index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lịch học
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <!-- /.col-lg-12 -->
                    <div class="row form-group">
                        <div class="col-md-10">
                        <a class="btn btn-primary" href="{{route('lichhoc_create_get')}}">Thêm mới</a>
                        </div>
                    </div> 

                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Id</th>
                                <th>Thứ</th>
                                <th>Tiết</th>
                                <th>Phòng học</th>
                                <th align="center">Hành động</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($lichhoc as $lh)
                            <tr>
                                <td>{{$lh->id}}</td>
                                <td>{{$lh->ngay_hoc}}</td>
                                <td>{{$lh->tiet_hoc}}</td>
                                <td>{{$lh->giang_duong}}</td>
                                <td align="center">
                                    <a href="{{route('lichhoc_confirmDelete',$lh->id)}}" title="Xóa" data-toggle="modal" data-target="#confirm_delete">
                                        <i class="fa fa-trash-o  fa-fw"></i>
                                    </a>
                                    <a title="Sửa" href="{{route('lichhoc_edit_get',$lh->id)}}"> 
                                        <i class="fa fa-pencil fa-fw"></i> 
                                    </a>
                                </td>
                                
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div> 
@endsection('content')