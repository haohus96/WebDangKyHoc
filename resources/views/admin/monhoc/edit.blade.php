@extends('admin/layout/index')
@section('header_style')
<link type="text/css" href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet" />
    
@endsection
@section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Môn học
                            <small>Sửa</small>
                        </h1>
                    </div>

                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif

                    
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{route('monhoc_edit_post',$monhoc->ma_mh)}}" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Mã môn học</label>
                                <input class="form-control" name="ma_mh" placeholder="" 
                                value="{{$monhoc->ma_mh}}"/>
                            </div>
                            <div class="form-group">
                                <label>Tên môn học</label>
                                <input class="form-control" name="ten_mh" placeholder="" 
                                value="{{$monhoc->ten_mh}}"/>
                            </div>
                            <div class="form-group">
                                <label>Số tín chỉ</label>
                                <input class="form-control" name="sotinchi" placeholder="" value="{{$monhoc->so_tinchi}}"/>
                            </div>
                            <div class="form-group">
                                <label>Lệ phí cơ bản</label>
                                <input type="text" class="form-control" name="lephi" placeholder="" value="{{$monhoc->hoc_phi}}" />
                            </div>
                            <div class="form-group">
                                <label>Ngành học</label>
                                <br>
                                {!! Form::select('chuyennganh[]',$chuyennganh,$data_ma_nganh,['class'=>'form-control','multiple'=>'multiple','id'=>'nganhhoc']) !!}
                            </div>
                            
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection

@section('script')

<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" ></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#nganhhoc').multiselect({
            enableFiltering: true,
            includeSelectAllOption: false,
            maxHeight:200,
            dropUp: true,
            buttonWidth: 250,
            nonSelectedText: 'Chưa chọn ngành học',
           // nSelectedText: '',


        });
    });
</script>
@endsection