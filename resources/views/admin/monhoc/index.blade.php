@extends('admin/layout/index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Môn học
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="row form-group">
                        <div class="col-md-2">
                             <a class="btn btn-primary" href="{{route('monhoc_create')}}">Thêm mới</a>
                       
                        </div>
                         <div class="col-md-2">
                             <a class="btn btn-info" data-toggle="modal" data-target="#update_hocphi">Cập nhập học phí</a>
                       
                        </div>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Mã môn học</th>
                                <th>Tên </th>
                                <th>Số tín chỉ</th>
                                <th>Học phí</th>
                                <th>Ngành học</th>
                                <th>Hành động</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($monhoc as $key => $mh)
                            <tr>
                                <td>{{$mh->ma_mh}}</td>
                                <td>{{$mh->ten_mh}}</td>
                                <td>{{$mh->so_tinchi}}</td>
                                <td>{{$mh->hoc_phi.' VNĐ'}}</td>
                                <td></td>
                                <td align="center">
                                    <a href="{{route('monhoc_confirmDelete',$mh->ma_mh)}}" data-toggle="modal" data-target="#confirm_delete" title="Xóa">
                                        <i class="fa fa-trash-o  fa-fw"></i>
                                    </a>
                                    <a href="{{route('monhoc_edit',$mh->ma_mh)}}" title="Sửa">
                                        <i class="fa fa-pencil fa-fw"></i> 
                                    </a>
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div> 
        <div class="modal fade" id="update_hocphi" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="user_delete_confirm_title">
                              Cập nhật học phí
                        </h4>
                    </div>
                    <div class="modal-body">
                       <div class="form-group">
                            <label>Nhập học phí cơ bản</label>
                            <input type="text" name="hoc_phi" class="form-control" id="hoc_phi">
                       </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                     
                        <a type="button" class="btn btn-primary" id="btn_update">Thực hiện</a>
                             
                    </div>

                </div>
            </div>
        </div> 
        <div class="modal fade" id="thongbao" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="user_delete_confirm_title">
                              Cập nhật học phí
                        </h4>
                    </div>
                    <div class="modal-body">
                       <!-- <div class="form-group">
                            <label>Nhập học phí cơ bản</label>
                            <input type="text" name="hoc_phi" class="form-control" id="hoc_phi">
                       </div> -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                     
                       <!--  <a type="button" class="btn btn-primary" id="btn_update">Thực hiện</a> -->
                             
                    </div>

                </div>
            </div>
        </div> 
@endsection('content')
@section('script')
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $('#btn_update').on('click',function(){
            var hoc_phi = $('#hoc_phi').val() ;
            console.log(hoc_phi) ;
            $.ajax({
                url:"{{route('monhoc_updateHocPhi')}}",
                headers : {
                    'X-CSRF-TOKEN': $('#_token').val()
                },
                data:{'hoc_phi':hoc_phi},
                success : function(response){
                    console.log(response) ;
                    $('#update_hocphi').modal('hide') ;
                    $('#thongbao').find('.modal-body').html(response) ;
                    $('#thongbao').modal() ;
                },
            })
        })
    })
</script>
@endsection