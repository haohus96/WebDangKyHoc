@extends('admin/layout/index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper"  >
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12"> 
                        <h1 class="page-header">Năm học
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="row form-group">
                        <div class="col-md-2">
                             <a class="btn btn-primary" href="{{route('namhoc_create_get')}}">Thêm mới</a>
                       
                        </div>
                        <!-- <div class="col-md-2">
                             <a class="btn btn-info" data-toggle="modal" data-target="#update_hocphi">Cập nhập học phí</a>
                       
                        </div> -->
                    </div>
                    <br>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                 <th>id</th>
                                <th>Năm học</th>
                                <th>Học kỳ</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($namhoc as $nh)
                            <tr align="center"> 
                                <td>{{$nh->id}}</td>
                                <td>{{$nh->nam}}</td>
                                <td >
                                   
                                   {{$nh->hoc_ky}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div> 
@endsection('content')