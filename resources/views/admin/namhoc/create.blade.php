@extends('admin/layout/index')
@section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Năm học
                            <small>Thêm</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
                            </a>
                            {{session('thongbao')}}
                        </div>
                    @endif

                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{route('namhoc_create_post')}}" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Năm học</label>
                                {!! Form::select('namhoc',$namhoc,null,['class'=>'form-control','id'=>'namhoc']) !!}
                            </div>
                           
                            <div class="form-group">
                                <label>Học kỳ</label>
                                {!! Form::select('hocky',$hocky,null,['class'=>'form-control','id'=>'hocky']) !!}
                            </div>
                            
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection

@section('script')
<script type="text/javascript" >
    $(document).ready(function(){
        $('#namhoc').on('change',function(){
            var namhoc = $(this).val() ;
            $.ajax({
                url:'{{route('ajax_getHockyByNamhoc')}}',
                data:{'namhoc':namhoc},
                success:function(response){
                    //alert(response) ;
                    $('#hocky').html(response) ;
                }
            });
        });
    });
</script>
@endsection