<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::route('login_sinhvien') ;
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('test/{str1}/{str2}',['uses'=>'testController@compareString'])->name('test');
Route::get('testPDF',['as'=>'test','uses'=>'FpdfController@pdfView']) ;
Route::get('test_view',function(){
	return view('filePDF.test') ;
}) ;
Route::get('admin/login',['as'=>'login_admin_get','uses'=>'UsersController@getLoginAdmin']) ;
Route::post('admin/login',['as'=>'login_admin_post','uses'=>'UsersController@postLoginAdmin']) ;
Route::get('admin/logout',['as'=>'logout_admin','uses'=>'UsersController@logoutAdmin']) ;
Route::group(['prefix'=>'admin','middleware'=>'AdminLogin'],function(){


	Route::group(['prefix'=>'user'],function(){
		Route::get('/',['as'=>'user_list','uses'=>'UsersController@index']) ;
		Route::get('create',['as'=>'user_create_get','uses'=>'UsersController@create']) ;
		Route::post('create',['as'=>'user_create_post','uses'=>'UsersController@store']) ;
		Route::get('create_sinhvien',['as'=>'user_create_sv_get','uses'=>'UsersController@createSinhVien']) ;
		Route::post('create_sinhvien',['as'=>'user_create_sv_post','uses'=>'UsersController@storeSinhVien']) ;
		Route::get('create_allsinhvien',['as'=>'user_createAllSinhVien','uses'=>'UsersController@createAllUserStudent']) ;
		Route::get('confirm_delete/{id}',['as'=>'user_confirmDelete','uses'=>'UsersController@getModal']);
		Route::get('delete/{id}',['as'=>'user_delete','uses'=>'UsersController@delete']);
		Route::get('edit/{id}',['as'=>'user_edit_get','uses'=>'UsersController@edit']);
		Route::post('edit/{id}',['as'=>'user_edit_post','uses'=>'UsersController@update']);
	}) ;
	Route::group(['prefix'=>'sinhvien'],function(){
		Route::get('/',['as'=>'sinhvien_list','uses'=>'SinhVienController@index']) ;
		Route::get('create',['as'=>'sinhvien_create_get','uses'=>'SinhVienController@create']) ;
		Route::post('create',['as'=>'sinhvien_create_post','uses'=>'SinhVienController@store']) ;
		Route::get('edit/{ma_sv}',['as'=>'sinhvien_edit_get','uses'=>'SinhVienController@edit']) ;
		Route::post('edit/{ma_sv}',['as'=>'sinhvien_edit_post','uses'=>'SinhVienController@update']) ;
		Route::get('confirm_delete/{ma_sv}',['as'=>'sinhvien_confirmDelete','uses'=>'SinhVienController@getModal']);
		Route::get('delete/{ma_sv}',['as'=>'sinhvien_delete','uses'=>'SinhVienController@delete']);
		//getDiemBySinhVien
		Route::get('getDiemBySinhVien/{ma_sv}',['as'=>'sinhvien_getDiemBySinhVien','uses'=>'SinhVienController@getDiemBySinhVien']);
		Route::get('createDiemBySinhVien/{ma_sv}',['as'=>'sinhvien_createDiem','uses'=>'SinhVienController@createDiem']);
		Route::post('createDiemBySinhVien/{ma_sv}',['as'=>'sinhvien_createDiem_post','uses'=>'SinhVienController@storeDiem']);
	});
	Route::group(['prefix'=>'monhoc'],function(){
		Route::get('/',['as'=>'monhoc_list','uses'=>'MonHocController@index']) ;
		Route::get('create',['as'=>'monhoc_create','uses'=>'MonHocController@create']) ;
		Route::post('create',['as'=>'monhoc_create_post','uses'=>'MonHocController@store']) ;
		Route::get('edit/{ma_mh}',['as'=>'monhoc_edit','uses'=>'MonHocController@edit']) ;
		Route::post('edit/{ma_mh}',['as'=>'monhoc_edit_post','uses'=>'MonHocController@update']) ;
		Route::get('confirm_delete/{ma_mh}',['as'=>'monhoc_confirmDelete','uses'=>'MonHocController@getModal']);
		Route::get('delete/{ma_mh}',['as'=>'monhoc_delete','uses'=>'MonHocController@delete']);
		Route::get('cap-nhat-hoc-phi',['as'=>'monhoc_updateHocPhi','uses'=>'MonHocController@updateHocPhi']);
	});
	Route::group(['prefix'=>'lichhoc'],function(){
		Route::get('/',['as'=>'lichhoc_list','uses'=>'LichHocController@index']) ;
		Route::get('create',['as'=>'lichhoc_create_get','uses'=>'LichHocController@create']) ;
		Route::post('create',['as'=>'lichhoc_create_post','uses'=>'LichHocController@store']) ;
		Route::get('edit/{id}',['as'=>'lichhoc_edit_get','uses'=>'LichHocController@edit']) ;
		Route::post('edit/{id}',['as'=>'lichhoc_edit_post','uses'=>'LichHocController@update']) ;
		Route::get('confirm_delete/{id}',['as'=>'lichhoc_confirmDelete','uses'=>'LichHocController@getModal']) ;
		Route::get('delete/{id}',['as'=>'lichhoc_delete','uses'=>'LichHocController@delete']) ;
	});
	Route::group(['prefix'=>'lophoc'],function(){
		Route::get('/',['as'=>'lophoc_list','uses'=>'LopHocController@index']) ;
		Route::get('create',['as'=>'lophoc_create_get','uses'=>'LopHocController@create']) ;
		Route::post('create',['as'=>'lophoc_create_post','uses'=>'LopHocController@store']) ;
		Route::get('edit/{id}',['as'=>'lophoc_edit_get','uses'=>'LopHocController@edit']) ;
		Route::post('edit/{id}',['as'=>'lophoc_edit_post','uses'=>'LopHocController@update']) ;
		Route::get('confirm_delete/{id}',['as'=>'lophoc_confirmDelete','uses'=>'LopHocController@getModal']) ;
		Route::get('delete/{id}',['as'=>'lophoc_delete','uses'=>'LopHocController@delete']) ;
		//dongdkHoc
		Route::get('dongdkHoc',['as'=>'lophoc_dongdkHoc','uses'=>'LopHocController@dongdkHoc']) ;
		Route::get('modkHoc',['as'=>'lophoc_modkHoc','uses'=>'LopHocController@modkHoc']) ;
		//searchLopHocByNamHoc
		Route::get('searchLopHocByNamHoc',['as'=>'lophoc_searchLopHocByNamHoc','uses'=>'LopHocController@searchLopHocByNamHoc']) ;
		Route::get('danh-sach-sinh-vien-dang-ky-{id}',['as'=>'lophoc_ds_sinhvienDK','uses'=>'LopHocController@ds_sinhvienDK']) ;
		Route::get('danh-sach-sinh-vien-{id_lophoc}',['uses'=>'FpdfController@exportPDF_LopHoc'])->name('lophoc_export_dsSinhVien');

	});
	Route::group(['prefix'=>'chuyennganh'],function(){
		Route::get('/',['as'=>'chuyennganh_list','uses'=>'NganhHocController@index']) ;
		Route::get('data',['as'=>'chuyennganh_data','uses'=>'NganhHocController@data']) ;
		Route::get('create',['as'=>'chuyennganh_create_get','uses'=>'NganhHocController@create']) ;
		Route::post('create',['as'=>'chuyennganh_create_post','uses'=>'NganhHocController@store']) ;
		Route::get('edit/{manganh_edit}',['as'=>'chuyennganh_edit_get','uses'=>'NganhHocController@edit']) ;
		Route::post('edit/{ma_nganh}',['as'=>'chuyennganh_edit_post','uses'=>'NganhHocController@update']) ;
		Route::get('confirm_delete/{ma_nganh}',['as'=>'chuyennganh_confirmDelete','uses'=>'NganhHocController@getModal']);
		Route::get('delete/{ma_nganh}',['as'=>'chuyennganh_delete','uses'=>'NganhHocController@delete']);
	});
	Route::group(['prefix'=>'namhoc'],function(){
		// Route::get('/',function(){
		// 	return view('admin.namhoc.index') ;

		// })->name('namhoc_list') ;
		// Route::get('create',function(){
		// 	return view('admin.namhoc.create') ;
			
		// })->name('namhoc_create') ;
		Route::get('',['as'=>'namhoc_list','uses'=>'NamHocController@index']) ;
		Route::get('create',['as'=>'namhoc_create_get','uses'=>'NamHocController@create']) ;
		Route::post('create',['as'=>'namhoc_create_post','uses'=>'NamHocController@store']) ;
		Route::get('edit',['as'=>'namhoc_edit_get','uses'=>'NamHocController@edit']) ;

	});
	Route::group(['prefix'=>'ajax'],function(){
		Route::get('getHockyByNamhoc',['as'=>'ajax_getHockyByNamhoc','uses'=>'AjaxController@getHockyByNamhoc']) ;
		Route::get('getTenmhByMamh',['as'=>'ajax_getTenmhByMamh','uses'=>'AjaxController@getTenmhByMamh']) ;
	});
	Route::group(['prefix'=>'diemsinhvien'],function(){
		Route::get('/',['as'=>'diemsv_list','uses'=>'DiemsvController@index']) ;
		Route::get('create',['as'=>'diemsv_create_get','uses'=>'DiemsvController@create']) ;
		Route::post('create',['as'=>'diemsv_create_post','uses'=>'DiemsvController@store']) ;
		Route::get('edit/{id}',['as'=>'diemsv_edit_get','uses'=>'DiemsvController@edit']) ;
		Route::post('edit/{id}',['as'=>'diemsv_edit_post','uses'=>'DiemsvController@update']) ;
		Route::get('confirm_delete/{id}',['as'=>'diemsv_confirmDelete','uses'=>'DiemsvController@getModal']) ;
		Route::get('delete/{id}',['as'=>'diemsv_delete','uses'=>'DiemsvController@delete']) ;
	});

}) ;
Route::get('dkmh/login',['as'=>'login_sinhvien','uses'=>'PagesController@getLoginSinhVien']) ;
Route::post('dkmh/login',['as'=>'login_sinhvien_post','uses'=>'PagesController@postLoginSinhVien']) ;
Route::get('dkmh/logout',['as'=>'logout_sinhvien','uses'=>'PagesController@getDangXuat']) ;
Route::group(['prefix'=>'dkmh','middleware'=>'SinhVienLogin'],function(){
	//Route::get('lichhoc')
	
	Route::get('dangkyhoc',['as'=>'dangkymonhoc','uses'=>'PagesController@getLopHoc']) ;
	Route::get('storeDanhSachDangKy',['as'=>'dangky_save','uses'=>'PagesController@storeDanhSachDangKy']) ;
	Route::get('confirm_huymon/{id}',['as'=>'confirmHuyMon','uses'=>'PagesController@getModalHuyMon']) ;
	Route::get('huymonhoc/{id}',['as'=>'pages_huymon','uses'=>'PagesController@huyMonHoc']) ;
	Route::get('dkmhAjax',['as'=>'dkmhAjax','uses'=>'PagesController@getLopHoc2']) ;
	//getLichHocBySinhVien
	Route::get('lichhoc',['as'=>'dkmh_lichhoc','uses'=>'PagesController@getLichHocBySinhVien']) ;
	Route::get('exportPDF/{id_namhoc}',['uses'=>'FpdfController@exportPDF'])->name('exportPDF');
	Route::get('changePassword',['uses'=>'PagesController@get_changePassword'])->name('dkmh_changePassword_get');
	Route::post('changePassword',['uses'=>'PagesController@post_changePassword'])->name('dkmh_changePassword_post');
	Route::get('ket-qua-hoc-tap',['as'=>'dkmh_xemdiem','uses'=>'PagesController@DanhSachDiem']) ;

});